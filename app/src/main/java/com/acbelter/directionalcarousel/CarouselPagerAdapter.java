/*
 * Copyright 2014 acbelter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.acbelter.directionalcarousel;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.acbelter.directionalcarousel.page.OnPageClickListener;
import com.adapter.oishi.R;
import com.adapter.oishi.otification.utility.pager.ActivityPageItem;
import com.adapter.oishi.otification.utility.pager.ActorLinearLayout;
import com.github.siyamed.shapeimageview.CircularImageView;

import java.util.ArrayList;

public class CarouselPagerAdapter<T extends Parcelable> extends PagerAdapter implements OnPageChangeListener {
    private static final String TAG = CarouselViewPager.class.getSimpleName();
    private CarouselConfig mConfig;
    private int mPagesCount;
    private int mFirstPosition;

    private OnPageClickListener<T> mCallback;
    private ArrayList<ActivityPageItem> items;
    private int mCurrentPosition;

    private Class mPageFragmentClass;
    private int mPageLayoutId;

    private Context context;

    public CarouselPagerAdapter(Context context, ArrayList<ActivityPageItem> items) {
//        super(fragmentManager);
        mConfig = CarouselConfig.getInstance();
        this.context = context;
        this.items = items;

        mPagesCount = items.size();
        if (mConfig.infinite) {
            mFirstPosition = mPagesCount * CarouselConfig.LOOPS / 2;
        }
    }

    public void setOnPageClickListener(OnPageClickListener<T> listener) {
        mCallback = listener;
    }

    public void sendSingleTap(View view, T item) {
        if (mCallback != null) {
            mCallback.onSingleTap(view, item);
        }
    }

    public void sendDoubleTap(View view, T item) {
        if (mCallback != null) {
            mCallback.onDoubleTap(view, item);
        }
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        int realPosition = position;

        if (mConfig.infinite) {
            position = position % mPagesCount;
        }



        LayoutInflater inflater = LayoutInflater.from(context);
        LinearLayout l = null;


        if (items.get(position).getActiveRid() != -1) {
            l = (LinearLayout) inflater.inflate(R.layout.otification_pager_cell, collection, false);

            ImageView imageView = (ImageView) l.findViewById(R.id.imageView);
            imageView.setImageResource(items.get(position).getNormalRid());

            TextView textView = (TextView) l.findViewById(R.id.textView);
            textView.setText(items.get(position).getText());

        }
        else {
            l = (LinearLayout) inflater.inflate(R.layout.otification_pager_cell_circle, collection, false);

            CircularImageView imageViewColorBackground = (CircularImageView) l.findViewById(R.id.imageViewColorBackground);

            ImageView imageViewChar = (ImageView) l.findViewById(R.id.imageViewChar);
            imageViewChar.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
            imageViewChar.setImageResource(items.get(position).getNormalRid());

        }






        ActorLinearLayout root = (ActorLinearLayout) l.findViewById(R.id.root);
        root.setTag(""+realPosition);
        collection.addView(l);


        return root;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);

        System.runFinalization();
        Runtime.getRuntime().gc();
        System.gc();
    }

    @Override
    public int getCount() {
        if (mConfig.infinite) {
            return mPagesCount * CarouselConfig.LOOPS;
        }
        return mPagesCount;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }



//    @Override
//    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//        switch (mConfig.scrollScalingMode) {
//            case CarouselConfig.SCROLL_MODE_BIG_CURRENT: {
//                PageLayout current = getPageView(position);
//                PageLayout next = getPageView(position + 1);
//
//                if (current != null) {
//                    current.setScaleBoth(mConfig.bigScale
//                            - mConfig.getDiffScale() * positionOffset);
//                }
//
//                if (next != null) {
//                    next.setScaleBoth(mConfig.smallScale
//                            + mConfig.getDiffScale() * positionOffset);
//                }
//                break;
//            }
//            case CarouselConfig.SCROLL_MODE_BIG_ALL: {
//                PageLayout current = getPageView(position);
//                if (current != null) {
//                    current.setScaleBoth(mConfig.bigScale);
//                }
//
//                if (positionOffset > 0.0f) {
//                    scaleAdjacentPages(position, mConfig.pageLimit, mConfig.bigScale);
//                }
//                break;
//            }
//            case CarouselConfig.SCROLL_MODE_NONE: {
//                break;
//            }
//        }
//    }
//
    @Override
    public void onPageSelected(int position) {
        mCurrentPosition = position;

        if (mCallback != null) {
            mCallback.onSelected(position);
        }
    }
//
//    /**
//     * @param position     Position of the current page.
//     * @param scalingPages The number of pages on both sides of the current page,
//     *                     which must be scaled.
//     * @param scale        Scale value.
//     */
//    private void scaleAdjacentPages(int position, int scalingPages, float scale) {
//        if (scalingPages == 0) {
//            return;
//        }
//
//        for (int i = 0; i < scalingPages / 2; i++) {
//            PageLayout prevSidePage = getPageView(position - (i + 1));
//            if (prevSidePage != null) {
//                prevSidePage.setScaleBoth(scale);
//            }
//            PageLayout nextSidePage = getPageView(position + (i + 1));
//            if (nextSidePage != null) {
//                nextSidePage.setScaleBoth(scale);
//            }
//        }
//    }
//
//    @Override
//    public void onPageScrollStateChanged(int state) {
//        if (state == CarouselViewPager.SCROLL_STATE_IDLE) {
//            int scalingPages = CarouselConfig.getInstance().pageLimit;
//            if (scalingPages == 0) {
//                return;
//            }
//
//            if (mConfig.scrollScalingMode == CarouselConfig.SCROLL_MODE_BIG_ALL) {
//                scaleAdjacentPages(mCurrentPosition, scalingPages, mConfig.smallScale);
//            }
//        }
//    }

    public int getFirstPosition() {
        return mFirstPosition;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
