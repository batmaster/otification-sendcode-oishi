package com.adapter.oishi;

import android.content.Context;
import android.content.SharedPreferences;

import com.adapter.oishi.otification.utility.PreferenceService;
import com.adapter.oishi.otification.utility.SharePref;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by batmaster on 9/11/2016 AD.
 */

public class Config {

    public static final String oti_show_at_home = "oti_show_at_home"; // 0 [1]
    public static final String sendcode_show_at_home = "sendcode_show_at_home"; // 0 [1]

    public static final String oti_noti_final_date = "oti_noti_final_date"; // [1/10/2016 0:00]

    public static final String oti_active = "oti_active"; // 0 [1] 2
    public static final String sendcode_active = "sendcode_active"; // 0 [1] 2

    public static final String switch_artwork_date = "switch_artwork_date"; // [1/10/2016 0:00]

    public static final String sendcode_poll_start_date = "sendcode_poll_start_date"; // [1/10/2016 0:00]

    public static final String sendcode_page_prize = "sendcode_page_prize";
    public static final String sendcode_page_rule = "sendcode_page_rule";
    public static final String sendcode_page_winner = "sendcode_page_winner";
    public static final String sendcode_page_howto = "sendcode_page_howto";
    public static final String sendcode_page_poll = "sendcode_page_poll";

    private static final String[] pages = {
            "http://www.oishidrink.com/promotion/app/web/prize.html",
            "http://www.oishidrink.com/promotion/app/web/rule.html",
            "http://www.oishidrink.com/promotion/app/winner.aspx",
            "http://www.oishidrink.com/promotion/app/web/howto.html",
            "http://www.oishidrink.com/promotion/app/poll.aspx",
            "http://www.oishidrink.com/promotion/app_install.php",
            "โออิชิจัดให้! ทริปสุดโอ โกเจแปน",
            "iC_BoffCna4",
            "http://www.oishidrink.com/promotion/api/mobile/applicationstatlog.aspx"
    };


    public static final String sendcode_share_url = "sendcode_share_url"; // http://www.oishidrink.com/promotion/app_install.php
    public static final String sendcode_share_title = "sendcode_share_title"; // โออิชิจัดให้! ทริปสุดโอ โกเจแปน

    public static final String youtube_sendcode = "youtube_sendcode";
    public static final String sendcode_api_stat = "sendcode_api_stat";



    private static final SimpleDateFormat sdf = new SimpleDateFormat("d/MM/yyyy h:mm");

    public static int getInt(Context context, String key) {
        SharedPreferences sp = context.getSharedPreferences("oishi", Context.MODE_PRIVATE);
        return sp.getInt(key, 1);
    }

    public static void setInt(Context context, String key, int value) {
        SharedPreferences sp = context.getSharedPreferences("oishi", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    private static Date getStringDate(Context context, String key) {
        SharedPreferences sp = context.getSharedPreferences("oishi", Context.MODE_PRIVATE);
        String date = sp.getString(key, "1/10/2016 0:00");
        try {
            return sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean isPassed(Context context) {
        Date now = Calendar.getInstance().getTime();
        Date then = Config.getStringDate(context, Config.oti_noti_final_date);
        return then.compareTo(now) < 1;
    }

    public static boolean isOpenVote(Context context) {
        Date now = Calendar.getInstance().getTime();
        Date then = Config.getStringDate(context, Config.sendcode_poll_start_date);
        return then.compareTo(now) < 1;
    }

    public static void setStringDate(Context context, String key, String value) {
        SharedPreferences sp = context.getSharedPreferences("oishi", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getString(Context context, String key) {
        SharedPreferences sp = context.getSharedPreferences("oishi", Context.MODE_PRIVATE);
        int index = -1;
        if (key.equals(sendcode_page_prize)) {
            index = 0;
        }
        else if (key.equals(sendcode_page_rule)) {
            index = 1;
        }
        else if (key.equals(sendcode_page_winner)) {
            index = 2;
        }
        else if (key.equals(sendcode_page_howto)) {
            index = 3;
        }
        else if (key.equals(sendcode_page_poll)) {
            index = 4;
        }

        else if (key.equals(sendcode_share_url)) {
            index = 5;
        }
        else if (key.equals(sendcode_share_title)) {
            index = 6;
        }
        else if (key.equals(youtube_sendcode)) {
            index = 7;
        }
        else if (key.equals(sendcode_api_stat)) {
            index = 8;
        }

        return sp.getString(key, pages[index]);
    }

    public static void setString(Context context, String key, String value) {
        SharedPreferences sp = context.getSharedPreferences("oishi", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(key, value);
        editor.commit();
    }


}
