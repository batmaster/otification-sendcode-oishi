package com.adapter.oishi.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.adapter.oishi.Config;
import com.adapter.oishi.R;
import com.adapter.oishi.otification.activity.HomeActivity;
import com.adapter.oishi.sendcode.activities.custom.LandingActivity;

public class CommemorationActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_commemoration);

        new CountDownTimer(5000, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                if (Config.getInt(getApplicationContext(), Config.oti_show_at_home) == 1 && Config.getInt(getApplicationContext(), Config.sendcode_show_at_home) == 0) {
                    startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                    finish();
                }
                else if (Config.getInt(getApplicationContext(), Config.oti_show_at_home) == 1 && Config.getInt(getApplicationContext(), Config.sendcode_show_at_home) == 1) {
                    startActivity(new Intent(getApplicationContext(), NewHomeActivity.class));
                    finish();
                }
                else {
                    startActivity(new Intent(getApplicationContext(), LandingActivity.class));
                    finish();
                }
            }
        }.start();
    }

    @Override
    public void onBackPressed() {

    }
}
