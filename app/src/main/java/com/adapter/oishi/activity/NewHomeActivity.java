package com.adapter.oishi.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.adapter.oishi.Config;
import com.adapter.oishi.OishiApplication;
import com.adapter.oishi.R;
import com.adapter.oishi.otification.activity.EmptyDialog;
import com.adapter.oishi.otification.activity.HomeActivity;
import com.adapter.oishi.otification.utility.service.HTTPService;
import com.adapter.oishi.sendcode.activities.custom.LandingActivity;
import com.adapter.oishi.sendcode.activities.custom.MainActivity;

import java.util.Calendar;
import java.util.Date;

public class NewHomeActivity extends Activity {

    private OishiApplication application;

    private ImageView imageViewGotoSendcode;
    private ImageView imageViewGotoOti;

    private RelativeLayout rloLeftspace;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_home);

        application = (OishiApplication) getApplication();

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        final int widthPixels = metrics.widthPixels;
        int heightPixels = metrics.heightPixels;

        float scaleFactor = metrics.density;

        float widthDpi = metrics.xdpi;
        float heightDpi = metrics.ydpi;

        float widthInches = widthPixels / widthDpi;
        float heightInches = heightPixels / heightDpi;

        final double diagonalInches = Math.sqrt((widthInches * widthInches) + (heightInches * heightInches));

        imageViewGotoSendcode = (ImageView) findViewById(R.id.imageViewGotoSendcode);
        imageViewGotoSendcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                application.sendButtonStat(OishiApplication.OTI_UA, "Home_Sendcode");
                application.getHttpService().sendOpenAppStat(HTTPService.STAT_SENDCODE);

                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        imageViewGotoOti = (ImageView) findViewById(R.id.imageViewGotoOti);
        imageViewGotoOti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                application.sendButtonStat(OishiApplication.OTI_UA, "Home_Otification");
                Log.d("scsize", diagonalInches + "");

                if (diagonalInches >= 7) {
                    Intent intent = new Intent(getApplicationContext(), EmptyDialog.class);
                    intent.putExtra("text", 2);
                    startActivityForResult(intent, 155);
                }
                else {
                    Intent intent = new Intent(getApplicationContext(), com.adapter.oishi.otification.activity.fragment.MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });

        rloLeftspace = (RelativeLayout) findViewById(R.id.rloLeftspace);


        if ((double)heightPixels / widthPixels < 16.0/10) {
            rloLeftspace.post(new Runnable() {
                @Override
                public void run() {
                    LinearLayout.LayoutParams l = (LinearLayout.LayoutParams) rloLeftspace.getLayoutParams();
                    l.weight = 60;
                    rloLeftspace.setLayoutParams(l);
                }
            });
        }
    }
}
