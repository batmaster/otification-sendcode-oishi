package com.adapter.oishi;

import android.app.ActivityManager;
import android.app.KeyguardManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.adapter.oishi.otification.activity.AlarmActivity;
import com.adapter.oishi.otification.activity.fragment.MainActivity;
import com.adapter.oishi.otification.model.AlarmModel;
import com.adapter.oishi.otification.model.PlayListAlarmModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by batmaster on 8/26/16 AD.
 */
public class Receiver extends WakefulBroadcastReceiver {

    private OishiApplication app;

    @Override
    public void onReceive(Context context, Intent intent) {

        app = (OishiApplication) context.getApplicationContext();

        Log.d("bcc", "get " + intent.getAction());

        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            app.getAlarmDB().addAlarmsAfterBoot();
        } else if (intent.getAction().equals("com.adapter.oishi.ALARM")) {

            AlarmModel alarm = (AlarmModel) intent.getSerializableExtra("alarm");
            ArrayList<AlarmModel> aa = app.getAlarmDB().getAlarms();

            for (int i = aa.size() - 1; 0 <= i; i--) {
                if (aa.get(i).getId() == alarm.getId()) {
                    alarm = aa.get(i);
                    break;
                }
            }

            AlarmModel lastestA = null;
            for (int i = 0; i < aa.size(); i++) {
                if (aa.get(i).getHour() == alarm.getHour() && aa.get(i).getMinute() == alarm.getMinute()) {
                    lastestA = aa.get(i);
                    break;
                }
            }


            int day = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
            if (alarm.getDaysSet()[day - 1] == 1) {
                app.getAlarmDB().addRepeatAlarm(alarm, day - 1);
            }


//            ArrayList<AlarmModel> alarmModels = app.getAlarmDB().getAlarms();
//            for (int i = 0; i < alarmModels.size(); i++) {
//                if (alarmModels.get(i).getHour() == alarm.getHour() && alarmModels.get(i).getMinute() == alarm.getMinute()) {
//                    AlarmModel a = alarmModels.get(i);
//                    if (a.getSu() == 0 && a.getMo() == 0 && a.getTu() == 0 && a.getWe() == 0 && a.getTh() == 0 && a.getFr() == 0 && a.getSa() == 0) {
//                        app.getAlarmDB().setState(alarm, false);
//                        break;
//                    }
//                }
//            }

            Log.d("bcc", "----" + alarm.toString());

            try {
                if (alarm.getId() == lastestA.getId()) {
                    // NOTI
                    PlayListAlarmModel media = app.getPlayListAlarmDB().getApiModel(alarm.getActivity(), alarm.getActor());

                    NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);
                    mBuilder.setSmallIcon(R.mipmap.ic_logo);
                    mBuilder.setContentTitle(media.getNoti_title());
                    mBuilder.setContentText(media.getNoti_message());
                    mBuilder.setAutoCancel(true);

                    Intent resultIntent = new Intent(context, AlarmActivity.class);
                    resultIntent.putExtra("alarm", alarm);
                    TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                    stackBuilder.addParentStack(MainActivity.class);
                    stackBuilder.addNextIntent(resultIntent);

                    PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    mBuilder.setContentIntent(resultPendingIntent);

                    Notification noti = mBuilder.build();

                    if (alarm.getActivity() != -1) {
                        if (alarm.getSound() == 1) {
                            Uri sound = Uri.parse(context.getFileStreamPath(media.getAudioFileName()).toString());
//                        noti.sound = sound;
                            noti.sound = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.silent);

                            MediaPlayer mp = MediaPlayer.create(context, sound);
                            mp.start();
                        } else {
                            noti.sound = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.silent);
                        }

                    } else {
                        noti.sound = Uri.parse(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + "/oishi/" + media.getAudioFileName());
                    }


                    long[] vibrate = {0, 0};
                    if (alarm.getVibration() == 1) {
                        vibrate = new long[]{200, 200, 200, 200, 200, 200, 200, 200};
                    }

                    noti.vibrate = vibrate;

                    NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                    mNotificationManager.notify(31909, noti);


                    try {
                        Thread.sleep(5000);
                        // play clip
                        Intent intent1 = new Intent(context, AlarmActivity.class);
                        intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent1.putExtra("alarm", alarm);
                        context.startActivity(intent1);

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }

    }

    private boolean isRunningInForeground(Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = manager.getRunningTasks(1);
        if (tasks.isEmpty()) {
            return false;
        }
        String topActivityName = tasks.get(0).topActivity.getPackageName();

        KeyguardManager myKM = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
        return topActivityName.equalsIgnoreCase(context.getPackageName()) && !myKM.inKeyguardRestrictedInputMode();
    }
}
