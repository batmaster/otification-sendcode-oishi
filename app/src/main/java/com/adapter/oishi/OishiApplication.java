package com.adapter.oishi;

import android.app.Application;
import android.util.Log;

import com.adapter.oishi.otification.utility.database.AlarmDBHelper;
import com.adapter.oishi.otification.utility.database.PlayListAlarmDBHelper;
import com.adapter.oishi.otification.utility.database.PlayListFriendDBHelper;
import com.adapter.oishi.otification.utility.database.PlayListGalleryDBHelper;
import com.adapter.oishi.otification.utility.database.ShareDBHelper;
import com.adapter.oishi.otification.utility.service.HTTPService;
import com.adapter.oishi.sendcode.http.OishiHTTPService;
import com.adapter.oishi.sendcode.managers.SystemManager;
import com.appsflyer.AppsFlyerLib;
import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.parse.Parse;
import com.parse.ParseInstallation;

import java.util.HashMap;

import io.fabric.sdk.android.Fabric;

/**
 * Created by batmaster on 8/14/16 AD.
 */
public class OishiApplication extends Application {

    private Tracker mTracker;

    private HTTPService httpService;

    private PlayListAlarmDBHelper playListAlarmDB;
    private PlayListFriendDBHelper playListFriendDB;
    private PlayListGalleryDBHelper playListGalleryDB;
    private AlarmDBHelper alarmDB;
    private ShareDBHelper shareDB;

    private SystemManager systemManager;

    private OishiHTTPService oishiHttpService;

    @Override
    public void onCreate() {
        super.onCreate();
        final Fabric fabric = new Fabric.Builder(this)
                .kits(new Crashlytics())
                .debuggable(true)
                .build();
        Fabric.with(fabric);


        AppsFlyerLib.getInstance().startTracking(this, "HGETasn6yh8FhMC2LQgEWP");
        AppsFlyerLib.getInstance().setAppId("com.adapter.oishi");

        Parse.initialize(this, "hcQPULoCz11pZmdmxDP5ZiDciS3ZmP5nXdcxjQzG", "ZkIBK8C8AY0UAv6DbPtNupqfYbUoajEHh7maLRsN");
        ParseInstallation.getCurrentInstallation().saveInBackground();

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        httpService = new HTTPService(this);
        playListAlarmDB = new PlayListAlarmDBHelper(this);
        playListFriendDB = new PlayListFriendDBHelper(this);
        playListGalleryDB = new PlayListGalleryDBHelper(this);
        alarmDB = new AlarmDBHelper(this);
        shareDB = new ShareDBHelper(this);

        systemManager = new SystemManager(this);
        oishiHttpService = new OishiHTTPService(this);
    }

    public HTTPService getHttpService() {
        return httpService;
    }


    public PlayListAlarmDBHelper getPlayListAlarmDB() {
        return playListAlarmDB;
    }

    public PlayListFriendDBHelper getPlayListFriendDB() {
        return playListFriendDB;
    }

    public PlayListGalleryDBHelper getPlayListGalleryDB() {
        return playListGalleryDB;
    }

    public AlarmDBHelper getAlarmDB() {
        return alarmDB;
    }

    public ShareDBHelper getShareDB() {
        return shareDB;
    }

    public SystemManager getSystemManager() {
        return systemManager;
    }

    public OishiHTTPService getOishiHttpService() {
        return oishiHttpService;
    }

    public static final String OTI_UA = "UA-48812367-17";
    public static final String SENDCODE_UA = "UA-48812367-19";

    private static HashMap<String, Tracker> mTrackers = new HashMap<String, Tracker>();

    synchronized Tracker getTracker(String ua) {
        if (!mTrackers.containsKey(ua)) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            mTrackers.put(ua, analytics.newTracker(ua));

        }
        return mTrackers.get(ua);

    }

    public void sendButtonStat(String label) {
        sendButtonStat(OTI_UA, label);
    }

    public void sendButtonStat(String ua, String label) {
        Log.d("sendButtonStat", label);

        getTracker(ua).send(new HitBuilders.EventBuilder()
                .setCategory("Button")
                .setAction("clicked")
                .setLabel(label)
                .build());
    }

    public void sendPageStat(String label) {
        sendPageStat(OTI_UA, label);
    }

    public void sendPageStat(String ua, String label) {
        Log.d("sendPageStat", label);

        getTracker(ua).send(new HitBuilders.EventBuilder()
                .setCategory("Page")
                .setAction("opened")
                .setLabel(label)
                .build());
    }

}
