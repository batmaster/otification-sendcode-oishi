package com.adapter.oishi.otification.utility.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.adapter.oishi.otification.model.ShareModel;

import java.util.ArrayList;

public class ShareDBHelper extends SQLiteOpenHelper {

    private Context context;

    public ShareDBHelper(Context context) {
        super(context, "share.db", null, 1);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE shares (id INTEGER PRIMARY KEY AUTOINCREMENT, type INTEGER, activity INTEGER, actor INTEGER, hour INTEGER, minute INTEGER)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS shares");
        onCreate(db);
    }

    public void addAlarm(int type, int activity, int actor, int hour, int minute) {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        sqLiteDatabase.execSQL(String.format("INSERT INTO shares (type, activity, actor, hour, minute) VALUES (%d, %d, %d, %d, %d)", type, activity, actor, hour, minute));
        sqLiteDatabase.close();

    }

    public void deleteAlarm(ShareModel alarm) {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        sqLiteDatabase.execSQL(String.format("DELETE FROM shares WHERE id = %d", alarm.getId()));
        sqLiteDatabase.close();
    }

    public ArrayList<ShareModel> getAlarms() {
        ArrayList<ShareModel> alarms = new ArrayList<ShareModel>();

        SQLiteDatabase sqLiteDatabase = getReadableDatabase();

        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM shares ORDER BY id DESC", null);

        if (cursor != null) {
            cursor.moveToFirst();
        }

        while (!cursor.isAfterLast()) {

            alarms.add(new ShareModel(
                    cursor.getInt(0),
                    cursor.getInt(1),
                    cursor.getInt(2),
                    cursor.getInt(3),
                    cursor.getInt(4),
                    cursor.getInt(5)
            ));

            cursor.moveToNext();
        }

        sqLiteDatabase.close();

        return alarms;
    }
}
