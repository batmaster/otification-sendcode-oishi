package com.adapter.oishi.otification.utility.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.adapter.oishi.otification.model.PlayListFriendModel;

import java.io.File;
import java.util.ArrayList;

public class PlayListFriendDBHelper extends SQLiteOpenHelper {

    private Context context;

    public PlayListFriendDBHelper(Context context) {
        super(context, "plfriend.db", null, 1);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE medias (id INTEGER PRIMARY KEY AUTOINCREMENT, activity INTEGER, actor INTEGER, active INTEGER, name TEXT, video TEXT, galleryImage TEXT, url TEXT, title TEXT, description TEXT, shareImage TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS medias");
        onCreate(db);
    }

    public void addMedia(PlayListFriendModel model) {
        removeMedia(model);

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        sqLiteDatabase.execSQL(String.format("INSERT INTO medias (activity, actor, active, name, video, galleryImage, url, title, description, shareImage) VALUES (%d, %d, %d, '%s', '%s', '%s', '%s', '%s', '%s', '%s')",
                model.getActivity(), model.getActor(), model.getActive(), model.getName(), model.getVideo(),
                model.getGalleryImage(), model.getUrl(), model.getTitle(), model.getDescription(), model.getShareImage()));
        sqLiteDatabase.close();
    }

    private void removeMedia(PlayListFriendModel model) {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        sqLiteDatabase.execSQL("DELETE FROM medias WHERE activity = " + model.getActivity() + " AND actor = " + model.getActor());
        sqLiteDatabase.close();
    }

    private ArrayList<PlayListFriendModel> getApiModels() {
        ArrayList<PlayListFriendModel> playListFriendModels = new ArrayList<PlayListFriendModel>();

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(String.format("SELECT activity, actor, active, name, video, galleryImage, url, title, description, shareImage FROM medias"), null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    playListFriendModels.add(new PlayListFriendModel(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getString(7), cursor.getString(8), cursor.getString(9)));
                } while (cursor.moveToNext());
            }
        }

        cursor.close();
        sqLiteDatabase.close();

        return playListFriendModels;
    }

    public PlayListFriendModel getApiModel(int activity, int actor) {
        ArrayList<PlayListFriendModel> playListFriendModels = getApiModels();
        for (int i = 0; i < playListFriendModels.size(); i++) {
            if (activity == playListFriendModels.get(i).getActivity() && actor == playListFriendModels.get(i).getActor()) {
                return playListFriendModels.get(i);
            }
        }
        return null;
    }

    public boolean checkHasToDownload(PlayListFriendModel model) {
        int activity = model.getActivity();
        int actor = model.getActor();

        PlayListFriendModel d = getApiModel(activity, actor);


        if (d == null) {
            // download
            return true;
        }
        else if (!d.getVideoFileName().equals(model.getVideoFileName())) {
            removeFile(context, d.getVideoFileName());
            return true;
        }
        else if (!fileExistance(context, d.getVideoFileName())) {
            removeFile(context, d.getVideoFileName());
            return true;
        }
        return false;
    }

    public boolean fileExistance(Context context, String fname){
        File file = context.getFileStreamPath(fname);
        return file.exists();
    }

    public boolean removeFile(Context context, String fname){
        File file = context.getFileStreamPath(fname);
        return file.delete();
    }

}
