package com.adapter.oishi.otification.activity.fragment;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.acbelter.directionalcarousel.CarouselPagerAdapter;
import com.acbelter.directionalcarousel.CarouselViewPager;
import com.acbelter.directionalcarousel.page.OnPageClickListener;
import com.adapter.oishi.OishiApplication;
import com.adapter.oishi.R;
import com.adapter.oishi.otification.activity.EmptyDialog;
import com.adapter.oishi.otification.activity.LoadingActivity;
import com.adapter.oishi.otification.activity.ThankYouDialog;
import com.adapter.oishi.otification.model.AlarmModel;
import com.adapter.oishi.otification.model.PlayListAlarmModel;
import com.adapter.oishi.otification.utility.CustomViewPager;
import com.adapter.oishi.otification.utility.PreferenceService;
import com.adapter.oishi.otification.utility.pager.ActivityPageItem;
import com.adapter.oishi.otification.utility.pager.ActorPagerAdapter;
import com.adapter.oishi.otification.utility.service.HTTPService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

import cn.carbswang.android.numberpickerview.library.NumberPickerView;

public class CreateAlarmFragment extends Fragment {

    private OishiApplication app;

    private int activitySelected = 0;
    private CarouselViewPager carouselActivity;
    private ArrayList<ActivityPageItem> itemsActivity;
    private CarouselPagerAdapter<ActivityPageItem> adapterActivity;

    private static final int[] resActivity = {
            R.drawable.btn_normal_4,
            R.drawable.btn_normal_2,
            R.drawable.btn_normal_3,
            R.drawable.btn_normal_1,
            R.drawable.btn_normal_5
    };

    private static final int[] res2Activity = {
            R.drawable.btn_active_4,
            R.drawable.btn_active_2,
            R.drawable.btn_active_3,
            R.drawable.btn_active_1,
            R.drawable.btn_active_5
    };

    private int actorSelected = 0;
    private static final String[] textActivity = {
            "ตื่นนอน",
            "ออกกำลังกาย",
            "อ่านหนังสือ",
            "ฝันดี",
            "นัดนู่นนี่นั่น"
    };

    private static final String[] names = {
            "พุฒ พุฒิชัย",
            "ต่อ ธนภพ",
            "มาร์ช จุฑาวุฒิ",
            "กั้ง วรกร",
            "มุก วรนิษฐ์",
            "ออฟ จุมพล",
    };

    private CustomViewPager viewPagerActor;
    private GestureDetector gestureDetector;
    public final static int PAGES = 6;
    public final static int LOOPS = 1000;
    public final static int FIRST_PAGE = PAGES * LOOPS / 2;

    private TextView textViewName;

    private ImageView imageViewWhite;


    private ImageView imageViewLeft;
    private RelativeLayout pressedArea;
    private ImageView imageViewRight;

    private ImageView imageViewCustom;
    private ImageView imageViewSave;

    private ImageView imageViewShare;

    public CreateAlarmFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.otification_fragment_create_alarm, container, false);

        app = (OishiApplication) getActivity().getApplication();
        app.sendPageStat("page_create");

        Calendar c = Calendar.getInstance();
        c.add(Calendar.MINUTE, 1);

        final NumberPickerView numberPickerHours = (NumberPickerView) rootView.findViewById(R.id.numberPickerHours);
        ArrayList<String> hours = new ArrayList<String>();
        for (int i = 0; i < 24; i++) {
            hours.add(String.valueOf(i));
        }
        numberPickerHours.refreshByNewDisplayedValues(hours.toArray(new String[hours.size()]));
        numberPickerHours.setValue(c.get(Calendar.HOUR_OF_DAY));

        final NumberPickerView numberPickerMinutes = (NumberPickerView) rootView.findViewById(R.id.numberPickerMinutes);
        ArrayList<String> minutes = new ArrayList<String>();
        for (int i = 0; i < 60; i++) {
            minutes.add(String.format("%02d", i));
        }
        numberPickerMinutes.refreshByNewDisplayedValues(minutes.toArray(new String[minutes.size()]));
        numberPickerMinutes.setValue(c.get(Calendar.MINUTE));

        imageViewWhite = (ImageView) rootView.findViewById(R.id.imageViewWhite);

        carouselActivity = (CarouselViewPager) rootView.findViewById(R.id.carouselActivity);
        carouselActivity.setOffscreenPageLimit(6);
        carouselActivity.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (state == 0) {
                    imageViewWhite.setVisibility(View.VISIBLE);
                }
                else {
                    imageViewWhite.setVisibility(View.INVISIBLE);
                }

                switch (state) {
                    case 2:
                        carouselActivity.setPagingEnabled(false);
                        break;
                    case 0:
                        carouselActivity.setPagingEnabled(true);
                        break;
                }
            }
        });

        itemsActivity = new ArrayList<ActivityPageItem>(resActivity.length);
        for (int i = 0; i < resActivity.length; i++) {
            itemsActivity.add(new ActivityPageItem(resActivity[i], res2Activity[i], textActivity[i]));
        }

        final String[] activityGA = {
                "my_wakeup",
                "my_EX",
                "my_read",
                "my_GN",
                "my_appoiment"
        };

        adapterActivity = new CarouselPagerAdapter<ActivityPageItem>(getContext(), itemsActivity);
        adapterActivity.setOnPageClickListener(new OnPageClickListener<ActivityPageItem>() {
            @Override
            public void onSingleTap(View view, ActivityPageItem item) {

            }

            @Override
            public void onDoubleTap(View view, ActivityPageItem item) {

            }

            @Override
            public void onSelected(int position) {

                int i = position % resActivity.length;
                activitySelected = i;
                //ActorLinearLayout linearLayout = (ActorLinearLayout) carouselActivity.findViewWithTag(""+position);
                //linearLayout.setBackgroundColor(Color.BLACK);

                app.sendButtonStat(activityGA[activitySelected]);

                (new PreferenceService(getContext())).putString("activitySelected", String.valueOf(activitySelected));
            }
        });
        carouselActivity.setAdapter(adapterActivity);

        final int[] actors = new int[] {0, 1, 2, 3, 4, 5};
        final int[] actorsActive = new int[] {1, 1, 1, 1, 1, 1};
        actorSelected = actors[0];

        textViewName = (TextView) rootView.findViewById(R.id.textViewName);
        textViewName.setText(names[actorSelected]);

        imageViewLeft = (ImageView) rootView.findViewById(R.id.imageViewLeft);
        imageViewLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPagerActor.setCurrentItem(viewPagerActor.getCurrentItem() -1,true);
            }
        });

        imageViewRight = (ImageView) rootView.findViewById(R.id.imageViewRight);
        imageViewRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPagerActor.setCurrentItem(viewPagerActor.getCurrentItem() + 1,true);
            }
        });


        Display display = getActivity().getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);
        int width = size.x;

//        int margin = 1000 - (600/1080) * width;

        viewPagerActor = (CustomViewPager) rootView.findViewById(R.id.viewPagerActor);
        final ActorPagerAdapter adapter = new ActorPagerAdapter(getActivity(), getContext(), CreateAlarmFragment.this, getFragmentManager(), actors, actorsActive, true);
        viewPagerActor.setAdapter(adapter);
        viewPagerActor.setPageTransformer(false, adapter);
        viewPagerActor.setCurrentItem(FIRST_PAGE);
        viewPagerActor.setOffscreenPageLimit(6);

        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int widthPixels = metrics.widthPixels;

        int margin = -600;
        if (widthPixels < 600) {
            margin = -300;
        }

        gestureDetector = new GestureDetector(getContext(), new SingleTapConfirm());

        pressedArea = (RelativeLayout) rootView.findViewById(R.id.pressedArea);
        pressedArea.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (gestureDetector.onTouchEvent(event)) {
                    // single tab
                    Log.d("gest", "tab");
                    adapter.getCharImageCollection().get(0).callOnClick();

                }
                else {
                    // swipe
                    Log.d("gest", "s");
                    viewPagerActor.onTouchEvent(event);

                }
                return true;
            }
        });
        pressedArea.bringToFront();

        viewPagerActor.setPageMargin(margin);
        viewPagerActor.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                position %= actors.length;

                actorSelected = actors[position];
                textViewName.setText(names[actorSelected]);

                final int finalPosition = position;
                pressedArea.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if (gestureDetector.onTouchEvent(event)) {
                            // single tab
                            Log.d("gest", "tab");
                            adapter.getCharImageCollection().get(finalPosition).callOnClick();

                        }
                        else {
                            // swipe
                            Log.d("gest", "s");
                            viewPagerActor.onTouchEvent(event);

                        }
                        return true;
                    }
                });

                System.runFinalization();
                Runtime.getRuntime().gc();
                System.gc();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
//                Log.d("stateeee", state + "");
//                switch (state) {
//                    case 2:
//                        viewPagerActor.setPagingEnabled(false);
//                        break;
//                    case 0:
//                        viewPagerActor.setPagingEnabled(true);
//                        break;
//                }
            }
        });

        imageViewCustom = (ImageView) rootView.findViewById(R.id.imageViewCustom);
        imageViewCustom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                app.sendButtonStat("my_bnt_custom");


                ArrayList<AlarmModel> alarms = app.getAlarmDB().getAlarms();
                if (alarms.size() > 7) {
                    Intent intent = new Intent(getActivity().getApplicationContext(), EmptyDialog.class);
                    intent.putExtra("text", 1);
                    startActivity(intent);
                }
                else {
                    Bundle args = new Bundle();
                    args.putInt("activity", activitySelected);
                    args.putInt("hour", numberPickerHours.getValue());
                    args.putInt("minute", numberPickerMinutes.getValue());

                    ((MainActivity) getActivity()).setFragment(5, args);
                }
            }
        });

        imageViewSave = (ImageView) rootView.findViewById(R.id.imageViewSave);
        imageViewSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                app.sendButtonStat("my_bnt_save");

                ArrayList<AlarmModel> alarms = app.getAlarmDB().getAlarms();
                if (alarms.size() > 7) {
                    Intent intent = new Intent(getActivity().getApplicationContext(), EmptyDialog.class);
                    intent.putExtra("text", 1);
                    startActivity(intent);
                }
                else {

                    app.getHttpService().getPlaylistAlarm(new HTTPService.OnResponseCallback<JSONObject>() {
                        @Override
                        public void onResponse(boolean success, Throwable error, JSONObject data) {
                            if (!(data == null)) {
                                try {

                                    final ArrayList<PlayListAlarmModel> playListAlarmModels = new ArrayList<PlayListAlarmModel>();

                                    JSONArray group = data.getJSONObject("playlist").getJSONArray("group");
                                    for (int i = 0; i < group.length(); i++) {

                                        JSONObject g = group.getJSONObject(i);

                                        int activity = g.getInt("no") - 1;

                                        JSONArray list = g.getJSONArray("list");
                                        for (int j = 0; j < list.length(); j++) {
                                            JSONObject l = list.getJSONObject(j);

                                            int actor = l.getInt("actor") - 1;

                                            if (activity == activitySelected && actor == actorSelected) {
                                                final PlayListAlarmModel media = new PlayListAlarmModel(activity, actor, l.getString("name"), l.getString("noti_title"), l.getString("noti_message"), l.getString("video"), l.getString("audio_android"), l.getString("share_url"), l.getString("share_title"), l.getString("share_description"), l.getString("share_image"));

                                                String vid = media.getVideo().split("/")[media.getVideo().split("/").length - 1].replace(".mp4", "");
                                                app.sendButtonStat("save_" + vid);

                                                if (app.getPlayListAlarmDB().checkHasToDownload(media)) {
                                                    Intent intent = new Intent(getContext(), LoadingActivity.class);
                                                    intent.putExtra("media", media);
                                                    intent.putExtra("shouldPreview", false);
                                                    intent.putExtra("isFromAlarm", true);
                                                    intent.putExtra("hour", numberPickerHours.getValue());
                                                    intent.putExtra("minute", numberPickerMinutes.getValue());
                                                    getActivity().startActivityForResult(intent, 1909);
                                                } else {
                                                    Log.d("savee", numberPickerHours.getValue() + " " + numberPickerMinutes.getValue() + " " + activitySelected + " " + actorSelected);
                                                    app.getAlarmDB().addAlarm(activitySelected, actorSelected, numberPickerHours.getValue(), numberPickerMinutes.getValue());

                                                    int videoId = Integer.parseInt(media.getVideo().split("/")[media.getVideo().split("/").length - 1].replace(".mp4", ""));
                                                    app.getHttpService().saveGame(true, numberPickerHours.getValue(), numberPickerMinutes.getValue(), videoId, -1, new HTTPService.OnResponseCallback<JSONObject>() {
                                                        @Override
                                                        public void onResponse(boolean success, Throwable error, JSONObject data) {

                                                        }
                                                    });

                                                    Intent intent = new Intent(getActivity().getApplicationContext(), ThankYouDialog.class);
                                                    intent.putExtra("media", media);
                                                    startActivity(intent);

                                                    ((MainActivity) getActivity()).setFragment(1, null);
                                                }

                                            }


                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            } else {
                                Toast.makeText(getContext(), "เชื่อมต่ออินเตอร์เน็ตไม่ได้", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                }


            }
        });

        imageViewShare = (ImageView) rootView.findViewById(R.id.imageViewShare);
        imageViewShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                app.sendButtonStat("bnt_send_f");

                ((MainActivity) getActivity()).setFragment(6, null);
            }
        });

        return rootView;
    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Log.d("BEST","111");
        return super.onContextItemSelected(item);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d("BEST","222");
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    private class SingleTapConfirm extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onSingleTapUp(MotionEvent event) {
            return true;
        }
    }


}
