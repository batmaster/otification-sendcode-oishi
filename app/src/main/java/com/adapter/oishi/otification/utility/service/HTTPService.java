package com.adapter.oishi.otification.utility.service;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.adapter.oishi.Config;
import com.adapter.oishi.otification.utility.PreferenceService;
import com.adapter.oishi.otification.utility.SharePref;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by batmaster on 2/26/16 AD.
 */
public class HTTPService {

    private Context context;
    private RequestQueue queue;
    private String BASE_URL = "http://www.oishidrink.com/otification/api/mobile/";

    public HTTPService(Context context) {
        this.context = context;
        queue = Volley.newRequestQueue(context);
    }

    /**
     * API 1 Check Active App
     *
     * @param responseCallback
     */
    public void checkActiveApp(final OnResponseCallback<JSONObject> responseCallback) {
        Log.d("httpapi", "API 1 Check Active App " + BASE_URL + "getDataInfoMix.aspx?rnd=" + Calendar.getInstance().getTimeInMillis());
        // getDataInfoMix.aspx?rnd=765
        StringRequest request = new StringRequest(Request.Method.POST, BASE_URL + "getDataInfoMix.aspx?rnd=" + Calendar.getInstance().getTimeInMillis(), new Response.Listener<String>() {

            @Override
            public void onResponse(String s) {
                Log.d("httpapi", "API 1 onResponse: " + s);
                try {
                    JSONObject json = new JSONObject(s);

                    responseCallback.onResponse(true, null, json);

                } catch (JSONException e) {
                    e.printStackTrace();
                    responseCallback.onResponse(true, null, null);
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("httpapi", "API 1 onErrorResponse: " + volleyError);
                responseCallback.onResponse(false, volleyError, null);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("User-agent", _UA);
                return headers;
            }
        };

        queue.add(request);
    }

    /**
     * API 2 Get Playlist Alarm
     *
     * @param responseCallback
     */
    public void getPlaylistAlarm(final OnResponseCallback<JSONObject> responseCallback) {
        Log.d("httpapi", "API 2 Get Playlist Alarm");
        StringRequest request = new StringRequest(Request.Method.GET, BASE_URL + "getPlaylistAlarm.aspx", new Response.Listener<String>() {

            @Override
            public void onResponse(String s) {
                Log.d("httpapi", "API 2 onResponse: " + s);

                try {
                    JSONObject json = new JSONObject(s);

                    responseCallback.onResponse(true, null, json);

                } catch (JSONException e) {
                    e.printStackTrace();
                    responseCallback.onResponse(true, null, null);
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("httpapi", "API 2 onErrorResponse: " + volleyError);
                responseCallback.onResponse(false, volleyError, null);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("User-agent", _UA);
                return headers;
            }
        };

        queue.add(request);
    }

    /**
     * API 3 Get Playlist Friend
     *
     * @param responseCallback
     */
    public void getPlaylistFriend(final OnResponseCallback<JSONObject> responseCallback) {
        Log.d("httpapi", "API 3 Get Playlist Friend");
        StringRequest request = new StringRequest(Request.Method.GET, BASE_URL + "getPlaylistFriend.aspx", new Response.Listener<String>() {

            @Override
            public void onResponse(String s) {
                Log.d("httpapi", "API 3 onResponse: " + s);
                try {
                    JSONObject json = new JSONObject(s);

                    responseCallback.onResponse(true, null, json);

                } catch (JSONException e) {
                    e.printStackTrace();
                    responseCallback.onResponse(true, null, null);
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("httpapi", "API 3 onErrorResponse: " + volleyError);
                responseCallback.onResponse(false, volleyError, null);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("User-agent", _UA);
                return headers;
            }
        };

        queue.add(request);
    }

    /**
     * API 4 Get Playlist Gallery
     *
     * @param responseCallback
     */
    public void getPlaylistGallery(final OnResponseCallback<JSONObject> responseCallback) {
        Log.d("httpapi", "API 4 Get Playlist Gallery");
        StringRequest request = new StringRequest(Request.Method.GET, BASE_URL + "getPlaylistGallery.aspx", new Response.Listener<String>() {

            @Override
            public void onResponse(String s) {
                Log.d("httpapi", "API 4 onResponse: " + s);
                try {
                    JSONObject json = new JSONObject(s);

                    responseCallback.onResponse(true, null, json);

                } catch (JSONException e) {
                    e.printStackTrace();
                    responseCallback.onResponse(true, null, null);
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("httpapi", "API 4 onErrorResponse: " + volleyError);
                responseCallback.onResponse(false, volleyError, null);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("User-agent", _UA);
                return headers;
            }
        };

        queue.add(request);
    }

    /**
     * API 5 Save game
     *
     * @param responseCallback
     */
    public void saveGame(boolean self, int hour, int minute, int videoId, int category, final OnResponseCallback<JSONObject> responseCallback) {
        Log.d("httpapi", "API 5 Save game");

        final PreferenceService pref = new PreferenceService(context);
        if (AccessToken.getCurrentAccessToken() == null && (pref.getBoolean(PreferenceService.KEY_BOOLEAN_HAS_UPDATE_FB_INFO) == true)) {

            updateToken(new OnResponseCallback<JSONObject>() {
                @Override
                public void onResponse(boolean success, Throwable error, JSONObject data) {
                    pref.putBoolean(PreferenceService.KEY_BOOLEAN_HAS_UPDATE_FB_INFO, true);

                    Log.d("httpapi", "API 5 Save FB Info OK!!");
                }
            });
        }
        final StringRequest request = new StringRequest(Request.Method.POST, BASE_URL + "submitGameNonToken.aspx", new Response.Listener<String>() {

            @Override
            public void onResponse(String s) {
                Log.d("httpapi", "API 5 onResponse: " + s);
                try {
                    JSONObject json = new JSONObject(s);

                    responseCallback.onResponse(true, null, json);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("httpapi", "API 5 onErrorResponse: " + volleyError);
                responseCallback.onResponse(false, volleyError, null);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("User-agent", _UA);
                return headers;
            }
        };

        final HashMap<String, String> params = new HashMap<String, String>();
        params.put("param1", "android");

        String param2 = "";
        if (self) {
            param2 = "1|" + hour + ":" + minute + "|";
            if (videoId == -1) {
                param2 += "custom|" + category;
            } else {
                param2 += videoId;
            }
        } else {
            param2 = "2|" + hour + ":" + minute + "|" + videoId;
        }
        params.put("param2", param2);
        params.put("param3", "");
        params.put("access", "mobileapp");
        params.put("caller", "json");

        if (AccessToken.getCurrentAccessToken() == null) {
            params.put("fbuid", SharePref.getStringRid(context));
        } else {
            GraphRequest graph = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                @Override
                public void onCompleted(JSONObject object, GraphResponse response) {
                    if (response.getJSONObject() != null) {
                        Log.d("httpapi", "API 5 != null GraphResponse response: " + response.toString());

                        try {
                            String first_name = response.getJSONObject().getString("first_name");
                            String last_name = response.getJSONObject().getString("last_name");
                            String name = "";//response.getJSONObject().getString("name");
                            String email = "";//response.getJSONObject().getString("email");
                            String gender = response.getJSONObject().getString("gender");
                            String link = response.getJSONObject().getString("link");

                            params.put("fbuid", AccessToken.getCurrentAccessToken().getUserId());
                            params.put("firstname", first_name);
                            params.put("lastname", last_name);
                            params.put("username", name);
                            params.put("email", email);
                            params.put("gender", gender);
                            params.put("profilelink", link);

                            Log.d("httpapi", "API 5 " + params);

                            request.setParams(params);
                            queue.add(request);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,first_name,last_name,email,gender,link");
            graph.setParameters(parameters);
            graph.executeAsync();
        }
    }

    /**
     * API 6 NEW Update Token
     *
     * @param responseCallback
     */
    private void updateToken(final OnResponseCallback<JSONObject> responseCallback) {
        Log.d("httpapi", "API 6 Update Token with: " + AccessToken.getCurrentAccessToken());
        GraphRequest graph = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                Log.d("httpapi", "API 6 GraphResponse response: " + response);

                try {
                    String first_name = response.getJSONObject().getString("first_name");
                    String last_name = response.getJSONObject().getString("last_name");
                    String name = "";//response.getJSONObject().getString("name");
                    String email = "";//response.getJSONObject().getString("email");
                    String gender = response.getJSONObject().getString("gender");
                    String link = response.getJSONObject().getString("link");

                    StringRequest request = new StringRequest(Request.Method.POST, BASE_URL + "getinfoV3NonToken.aspx", new Response.Listener<String>() {

                        @Override
                        public void onResponse(String s) {
                            Log.d("httpapi", "API 6 onResponse: " + s);
                            try {
                                JSONObject json = new JSONObject(s);

                                responseCallback.onResponse(true, null, json);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Log.d("httpapi", "API 6 onErrorResponse: " + volleyError);
                            responseCallback.onResponse(false, volleyError, null);

                        }
                    }) {
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> headers = new HashMap<String, String>();
                            headers.put("User-agent", _UA);
                            return headers;
                        }
                    };

                    HashMap<String, String> params = new HashMap<String, String>();
                    params.put("fakefbuid", SharePref.getStringRid(context));
                    params.put("fbuid", AccessToken.getCurrentAccessToken().getUserId());
                    params.put("firstname", first_name);
                    params.put("lastname", last_name);
                    params.put("username", name);
                    params.put("email", email);
                    params.put("gender", gender);
                    params.put("profilelink", link);
                    params.put("access", "mobileapp");
                    params.put("caller", "json");
                    request.setParams(params);

                    Log.d("httpapi", "API 6 HashMap: " + params.toString());

                    queue.add(request);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,first_name,last_name,email,gender,link");
        graph.setParameters(parameters);
        graph.executeAsync();
    }

    /**
     * API 7 Save facebook share
     *
     * @param responseCallback
     */
    public void saveShareToWall(int clip_id, String postId, final OnResponseCallback<JSONObject> responseCallback) {
        Log.d("httpapi", "API 7 Save facebook share");
        StringRequest request = new StringRequest(Request.Method.POST, BASE_URL + "saveShareToWall.aspx", new Response.Listener<String>() {

            @Override
            public void onResponse(String s) {
                Log.d("httpapi", "API 7 onResponse: " + s);
                try {
                    JSONObject json = new JSONObject(s);

                    responseCallback.onResponse(true, null, json);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("httpapi", "API 7 onErrorResponse: " + volleyError);
                responseCallback.onResponse(false, volleyError, null);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("User-agent", _UA);
                return headers;
            }
        };

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("gid", String.valueOf(clip_id));
        params.put("type", "postshare");
        params.put("postid", postId);
        params.put("access", "mobileapp");
        params.put("code", AccessToken.getCurrentAccessToken().getToken());
        params.put("caller", "json");
        request.setParams(params);

        queue.add(request);
    }

    // STAT

    public static final String STAT_MAIN = "main2016";
    public static final String STAT_OTI = "otification";
    public static final String STAT_SENDCODE = "oishipromo";

    public void sendOpenAppStat(final String STAT) {
        String url = STAT == STAT_MAIN || STAT == STAT_OTI ? "http://www.oishidrink.com/otification/api/mobile/applicationstatlog.aspx"
                : "http://www.oishidrink.com/promotion/api/mobile/applicationstatlog.aspx";

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String s) {
                Log.d("httpapi", "API 0: " + STAT + " onResponse: " + s);

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("httpapi", "API 0: " + STAT + " onErrorResponse: " + volleyError);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("User-agent", _UA);
                return headers;
            }
        };

        Log.d("httpapi", "API 0: " + STAT + " " + url);

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("stat", STAT);
        params.put("param1", "android");
        params.put("param2", "openapp");
        request.setParams(params);

        queue.add(request);
    }

    public static final String START_SENDCODE = "startsendcode";
    public static final String VOICE_START = "voicestart";
    public static final String VOICE_END = "voiceend";
    public static final String VOICE_CANCEL = "voicecancel";

    public void sendStat(final String STAT) {
        Log.d("httpapi", "API 00: " + STAT + " " + Config.getString(context, Config.sendcode_api_stat));

        StringRequest request = new StringRequest(Request.Method.GET, Config.getString(context, Config.sendcode_api_stat), new Response.Listener<String>() {

            @Override
            public void onResponse(String s) {
                Log.d("httpapi", "API 00: " + STAT + " onResponse: " + s);

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("httpapi", "API 00: " + STAT + " onErrorResponse: " + volleyError);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("User-agent", _UA);
                return headers;
            }


        };

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("stat", "oishipromo");
        params.put("param1", "android");
        params.put("param2", STAT);
        request.setParams(params);

        queue.add(request);
    }

    public interface OnResponseCallback<T> {
        void onResponse(boolean success, Throwable error, T data);
    }

    public static final String _UA = Build.BRAND + " " + Build.MODEL + " (" + Build.BOOTLOADER + "); API: " + Build.VERSION.SDK_INT;

}
