package com.adapter.oishi.otification.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.adapter.oishi.OishiApplication;
import com.adapter.oishi.R;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class TutorialActivity extends FragmentActivity {

    private OishiApplication app;

    private ImageView imageViewClose;
    private ViewPager pager;

    private TextView textViewName;

    private LinearLayout linearLayout;
    private ImageView imageViewLeft;
    private ImageView imageViewRight;

    private static final String[] pageName = {
            "หน้าตั้งเตือนตัวเอง",
            "หน้าสร้าง VDO เอง",
            "หน้าส่งให้เพื่อน",
            "หน้าจัดการ list",
            "หน้าตั้งค่าการเตือน"
    };

    private static final int[] dotId = {
            R.id.imageViewDot1,
            R.id.imageViewDot2,
            R.id.imageViewDot3,
            R.id.imageViewDot4,
            R.id.imageViewDot5,
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otification_activity_tutorial);

        app = (OishiApplication) getApplication();
        app.sendPageStat("page_howto");

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/DB HelvethaicaMon X.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());


        final DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int widthPixels = metrics.widthPixels;


        imageViewClose = (ImageView) findViewById(R.id.imageViewClose);
        imageViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                app.sendButtonStat("tutorial_close");

                finish();
            }
        });

        textViewName = (TextView) findViewById(R.id.textViewName);
        if (widthPixels < 600) {
            textViewName.setTextSize(TypedValue.COMPLEX_UNIT_SP, 25);
        }

        pager = (ViewPager) findViewById(R.id.pager);
        TutorialPagerAdapter adapter = new TutorialPagerAdapter(getApplicationContext());
        pager.setAdapter(adapter);


        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                textViewName.setText(pageName[position]);

                for (int i = 0; i < dotId.length; i++) {
                    if (i == position) {
                        ((ImageView) findViewById(dotId[i])).setImageResource(R.drawable.img_tutorial_dot_active);
                    }
                    else {
                        ((ImageView) findViewById(dotId[i])).setImageResource(R.drawable.img_tutorial_dot);
                    }
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
        linearLayout.post(new Runnable() {
            @Override
            public void run() {
                RelativeLayout.LayoutParams p = (RelativeLayout.LayoutParams) linearLayout.getLayoutParams();
                p.setMargins(p.leftMargin, p.topMargin, p.rightMargin, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, metrics));
                linearLayout.setLayoutParams(p);
            }
        });

        imageViewLeft = (ImageView) findViewById(R.id.imageViewLeft);
        imageViewLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                app.sendButtonStat("tutorial_back");

                if (pager.getCurrentItem() > 0) {
                    pager.setCurrentItem(pager.getCurrentItem() - 1, true);
                }
            }
        });

        imageViewRight = (ImageView) findViewById(R.id.imageViewRight);
        imageViewRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                app.sendButtonStat("tutorial_next");

                if (pager.getCurrentItem() < dotId.length) {
                    pager.setCurrentItem(pager.getCurrentItem() + 1, true);
                }
            }
        });

        textViewName.setText(pageName[0]);
        ((ImageView) findViewById(dotId[0])).setImageResource(R.drawable.img_tutorial_dot_active);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(base));
    }
}
