package com.adapter.oishi.otification.model;

import java.io.Serializable;
import java.util.ArrayList;

public class PlayListGalleryModel implements Serializable {

    private int type;
    private int activity;
    private int actor;

    private int active;

    private String name;
    private String video;
    private String galleryImage;
    private String url;
    private String title;
    private String description;
    private String shareImage;

    public PlayListGalleryModel(int type, int activity, int actor, int active, String name, String video, String galleryImage, String url, String title, String description, String shareImage) {
        this.type = type;
        this.activity = activity;
        this.actor = actor;
        this.active = active;
        this.name = name;
        this.video = video;
        this.galleryImage = galleryImage;
        this.url = url;
        this.title = title;
        this.description = description;
        this.shareImage = shareImage;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getActivity() {
        return activity;
    }

    public void setActivity(int activity) {
        this.activity = activity;
    }

    public int getActor() {
        return actor;
    }

    public void setActor(int actor) {
        this.actor = actor;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getGalleryImage() {
        return galleryImage;
    }

    public void setGalleryImage(String galleryImage) {
        this.galleryImage = galleryImage;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getShareImage() {
        return shareImage;
    }

    public void setShareImage(String shareImage) {
        this.shareImage = shareImage;
    }

    public String getVideoFileName() {
        String[] s = video.split("/");
        return s[s.length - 1];
    }

    @Override
    public String toString() {
        return "PlayListGalleryModel{" +
                "activity=" + activity +
                ", actor=" + actor +
                ", active=" + active +
                ", name='" + name + '\'' +
                ", video='" + video + '\'' +
                ", galleryImage='" + galleryImage + '\'' +
                ", url='" + url + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", shareImage='" + shareImage + '\'' +
                '}';
    }

    public static ArrayList<PlayListGalleryModel> searchByActivity(ArrayList<PlayListGalleryModel> models, int activity) {
        ArrayList<PlayListGalleryModel> m = new ArrayList<PlayListGalleryModel>();

        for (int i = 0; i < models.size(); i++) {
            if (models.get(i).getActivity() == activity) {
                m.add(models.get(i));
            }
        }

        return m;
    }
}
