package com.adapter.oishi.otification.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.adapter.oishi.otification.model.PlayListFriendModel;
import com.adapter.oishi.otification.model.PlayListGalleryModel;
import com.adapter.oishi.otification.utility.InputStreamVolleyRequest;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import com.adapter.oishi.OishiApplication;
import com.adapter.oishi.R;
import com.adapter.oishi.otification.model.PlayListAlarmModel;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LoadingActivity extends Activity {

    private OishiApplication app;

    private TextView textViewPercent;

    private double videoFileLength = 0;
    private long videoFileDownloaded = 0;
    private double audioFileLength = 0;
    private long audioFileDownloaded = 0;

    private boolean shouldPreview;
    private boolean isFromAlarm;

    private RequestQueue requestQueue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otification_activity_loading);

        app = (OishiApplication) getApplication();

        shouldPreview = getIntent().getBooleanExtra("shouldPreview", false);
        isFromAlarm = getIntent().getBooleanExtra("isFromAlarm", false);
        boolean isFromGallery = getIntent().getBooleanExtra("isFromGallery", false);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/DB HelvethaicaMon X.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());

        textViewPercent = (TextView) findViewById(R.id.textViewPercent);


        if (isFromAlarm) {
            final PlayListAlarmModel media = (PlayListAlarmModel) getIntent().getSerializableExtra("media");

            InputStreamVolleyRequest downloadVideo = new InputStreamVolleyRequest(Request.Method.GET, media.getVideo(), new Response.Listener<byte[]>() {
                @Override
                public void onResponse(byte[] response) {
                    try {
                        if (response != null) {

                            videoFileLength = response.length;

                            InputStream input = new ByteArrayInputStream(response);
                            File file = getApplicationContext().getFileStreamPath(media.getVideoFileName());
                            file.createNewFile();
                            BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(file));
                            byte data[] = new byte[1024];

                            int count = 0;
                            while ((count = input.read(data)) != -1) {
                                videoFileDownloaded += count;
                                output.write(data, 0, count);

                                // pregress here
                                // progress = (int)total*100/file_length;
                                // progressDialog.setProgress(progress);

                                if (videoFileLength != 0 && audioFileLength != 0) {
                                    int perc = (int) ((videoFileDownloaded + audioFileDownloaded) / (videoFileLength + audioFileLength) * 100);
                                    setPercent(perc);
                                }
                            }

                            if (videoFileDownloaded == videoFileLength && audioFileDownloaded == audioFileLength && videoFileLength != 0 && audioFileLength != 0) {
                                app.getPlayListAlarmDB().addMedia(media);

                                if (shouldPreview) {
                                    Intent intent = new Intent(getApplicationContext(), PreviewVDOActivity.class);
                                    intent.putExtra("activity", media.getActivity());
                                    intent.putExtra("actor", media.getActor());
                                    intent.putExtra("isFromAlarm", isFromAlarm);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    Intent intent = new Intent();
                                    intent.putExtra("result", 1);
                                    intent.putExtra("activity", media.getActivity());
                                    intent.putExtra("actor", media.getActor());
                                    intent.putExtra("hour", getIntent().getIntExtra("hour", -1));
                                    intent.putExtra("minute", getIntent().getIntExtra("minute", -1));
                                    intent.putExtra("media", getIntent().getSerializableExtra("media"));
                                    setResult(Activity.RESULT_OK, intent);
                                    finish();
                                }
                            }

                            output.flush();

                            output.close();
                            input.close();
                        } else {
                            Toast.makeText(getApplicationContext(), "ดาวน์โหลดวิดีโอไม่สำเร็จ", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent();
                            intent.putExtra("result", 0);
                            setResult(Activity.RESULT_OK, intent);
                            finish();
                        }
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), "ดาวน์โหลดวิดีโอไม่สำเร็จ", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();

                        Intent intent = new Intent();
                        intent.putExtra("result", 0);
                        setResult(Activity.RESULT_OK, intent);
                        finish();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }, null);

            InputStreamVolleyRequest downloadAudio = new InputStreamVolleyRequest(Request.Method.GET, media.getAudio(), new Response.Listener<byte[]>() {
                @Override
                public void onResponse(byte[] response) {
                    try {
                        if (response != null) {

                            audioFileLength = response.length;

                            InputStream input = new ByteArrayInputStream(response);
                            File file = getApplicationContext().getFileStreamPath(media.getAudioFileName());
                            Log.d("aud", file.getAbsolutePath());
                            file.createNewFile();
                            BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(file));
                            byte data[] = new byte[1024];

                            int count = 0;
                            while ((count = input.read(data)) != -1) {
                                audioFileDownloaded += count;
                                output.write(data, 0, count);

                                // pregress here
                                // progress = (int)total*100/file_length;
                                // progressDialog.setProgress(progress);

                                if (videoFileLength != 0 && audioFileLength != 0) {
                                    int perc = (int) ((videoFileDownloaded + audioFileDownloaded) / (videoFileLength + audioFileLength) * 100);
                                    setPercent(perc);
                                }
                            }

                            if (videoFileDownloaded == videoFileLength && audioFileDownloaded == audioFileLength && videoFileLength != 0 && audioFileLength != 0) {
                                app.getPlayListAlarmDB().addMedia(media);

                                if (shouldPreview) {
                                    Intent intent = new Intent(getApplicationContext(), PreviewVDOActivity.class);
                                    intent.putExtra("activity", media.getActivity());
                                    intent.putExtra("actor", media.getActor());
                                    startActivity(intent);
                                    finish();
                                } else {
                                    Intent intent = new Intent();
                                    intent.putExtra("result", 1);
                                    intent.putExtra("activity", media.getActivity());
                                    intent.putExtra("actor", media.getActor());
                                    intent.putExtra("hour", getIntent().getIntExtra("hour", -1));
                                    intent.putExtra("minute", getIntent().getIntExtra("minute", -1));
                                    intent.putExtra("media", getIntent().getSerializableExtra("media"));
                                    setResult(Activity.RESULT_OK, intent);
                                    finish();
                                }
                            }

                            output.flush();

                            output.close();
                            input.close();
                        } else {
                            Toast.makeText(getApplicationContext(), "ดาวน์โหลดออดิโอไม่สำเร็จ", Toast.LENGTH_SHORT).show();

                            Intent intent = new Intent();
                            intent.putExtra("result", 0);
                            setResult(Activity.RESULT_OK, intent);
                            finish();
                        }
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), "ดาวน์โหลดออดิโอไม่สำเร็จ", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();

                        Intent intent = new Intent();
                        intent.putExtra("result", 0);
                        setResult(Activity.RESULT_OK, intent);
                        finish();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }, null);


            requestQueue = Volley.newRequestQueue(getApplicationContext(), new HurlStack());
            requestQueue.add(downloadVideo);
            requestQueue.add(downloadAudio);
        }
        else if (isFromGallery) {
            final PlayListGalleryModel media = (PlayListGalleryModel) getIntent().getSerializableExtra("media");

            InputStreamVolleyRequest downloadVideo = new InputStreamVolleyRequest(Request.Method.GET, media.getVideo(), new Response.Listener<byte[]>() {
                @Override
                public void onResponse(byte[] response) {
                    try {
                        if (response != null) {

                            videoFileLength = response.length;

                            InputStream input = new ByteArrayInputStream(response);
                            File file = getApplicationContext().getFileStreamPath(media.getVideoFileName());
                            file.createNewFile();
                            BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(file));
                            byte data[] = new byte[1024];

                            int count = 0;
                            while ((count = input.read(data)) != -1) {
                                videoFileDownloaded += count;
                                output.write(data, 0, count);

                                // pregress here
                                // progress = (int)total*100/file_length;
                                // progressDialog.setProgress(progress);

                                if (videoFileLength != 0) {
                                    int perc = (int) ((videoFileDownloaded) / (videoFileLength) * 100);
                                    setPercent(perc);
                                }
                            }

                            if (videoFileDownloaded == videoFileLength && videoFileLength != 0) {
                                app.getPlayListGalleryDB().addMedia(media);

                                if (shouldPreview) {
                                    Intent intent = new Intent(getApplicationContext(), PreviewVDOActivity.class);
                                    intent.putExtra("activity", media.getActivity());
                                    intent.putExtra("activity", media.getActivity());
                                    intent.putExtra("actor", media.getActor());
                                    intent.putExtra("isFromAlarm", isFromAlarm);
                                    intent.putExtra("isFromGallery", true);
                                    intent.putExtra("media", media);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    Intent intent = new Intent();
                                    intent.putExtra("result", 1);
                                    intent.putExtra("activity", media.getActivity());
                                    intent.putExtra("actor", media.getActor());
                                    intent.putExtra("hour", getIntent().getIntExtra("hour", -1));
                                    intent.putExtra("minute", getIntent().getIntExtra("minute", -1));
                                    intent.putExtra("media", media);
                                    setResult(Activity.RESULT_OK, intent);
                                    finish();
                                }
                            }

                            output.flush();

                            output.close();
                            input.close();
                        } else {
                            Toast.makeText(getApplicationContext(), "ดาวน์โหลดวิดีโอไม่สำเร็จ", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent();
                            intent.putExtra("result", 0);
                            setResult(Activity.RESULT_OK, intent);
                            finish();
                        }
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), "ดาวน์โหลดวิดีโอไม่สำเร็จ", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();

                        Intent intent = new Intent();
                        intent.putExtra("result", 0);
                        setResult(Activity.RESULT_OK, intent);
                        finish();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }, null);

            requestQueue = Volley.newRequestQueue(getApplicationContext(), new HurlStack());
            requestQueue.add(downloadVideo);
        }
        else {
            final PlayListFriendModel media = (PlayListFriendModel) getIntent().getSerializableExtra("media");

            InputStreamVolleyRequest downloadVideo = new InputStreamVolleyRequest(Request.Method.GET, media.getVideo(), new Response.Listener<byte[]>() {
                @Override
                public void onResponse(byte[] response) {
                    try {
                        if (response != null) {

                            videoFileLength = response.length;

                            InputStream input = new ByteArrayInputStream(response);
                            File file = getApplicationContext().getFileStreamPath(media.getVideoFileName());
                            file.createNewFile();
                            BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(file));
                            byte data[] = new byte[1024];

                            int count = 0;
                            while ((count = input.read(data)) != -1) {
                                videoFileDownloaded += count;
                                output.write(data, 0, count);

                                // pregress here
                                // progress = (int)total*100/file_length;
                                // progressDialog.setProgress(progress);

                                if (videoFileLength != 0) {
                                    int perc = (int) ((videoFileDownloaded) / (videoFileLength) * 100);
                                    setPercent(perc);
                                }
                            }

                            if (videoFileDownloaded == videoFileLength && videoFileLength != 0) {
                                app.getPlayListFriendDB().addMedia(media);

                                if (shouldPreview) {
                                    Intent intent = new Intent(getApplicationContext(), PreviewVDOActivity.class);
                                    intent.putExtra("activity", media.getActivity());
                                    intent.putExtra("actor", media.getActor());
                                    intent.putExtra("isFromAlarm", isFromAlarm);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    Intent intent = new Intent();
                                    intent.putExtra("result", 1);
                                    intent.putExtra("activity", media.getActivity());
                                    intent.putExtra("actor", media.getActor());
                                    intent.putExtra("hour", getIntent().getIntExtra("hour", -1));
                                    intent.putExtra("minute", getIntent().getIntExtra("minute", -1));
                                    intent.putExtra("media", media);
                                    setResult(Activity.RESULT_OK, intent);
                                    finish();
                                }
                            }

                            output.flush();

                            output.close();
                            input.close();
                        } else {
                            Toast.makeText(getApplicationContext(), "ดาวน์โหลดวิดีโอไม่สำเร็จ", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent();
                            intent.putExtra("result", 0);
                            setResult(Activity.RESULT_OK, intent);
                            finish();
                        }
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), "ดาวน์โหลดวิดีโอไม่สำเร็จ", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();

                        Intent intent = new Intent();
                        intent.putExtra("result", 0);
                        setResult(Activity.RESULT_OK, intent);
                        finish();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }, null);

            requestQueue = Volley.newRequestQueue(getApplicationContext(), new HurlStack());
            requestQueue.add(downloadVideo);
        }
    }

    private void setPercent(int perc) {
        Log.d("precc", perc + "%");
        if (perc % 4 == 0) {
            textViewPercent.setText(perc + "%");
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(base));
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        requestQueue.stop();
        System.runFinalization();
        Runtime.getRuntime().gc();
        System.gc();
    }

}
