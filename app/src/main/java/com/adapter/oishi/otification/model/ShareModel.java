package com.adapter.oishi.otification.model;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Created by batmaster on 8/19/16 AD.
 */
public class ShareModel implements Serializable {

    private int id;

    private int type;
    private int activity;
    private int actor;
    private int hour;
    private int minute;

    private String[] activity_th = {
            "ตื่นนอน",
            "ออกกำลังกาย",
            "อ่านหนังสือ",
            "ฝันดี",
            "นัดนู่นนี่นั่น"
    };

    private String[] activity_th2 = {
            "HBD",
            "คิดถึง",
            "ฝันดี",
            "ให้กำลังใจ",
            "ชวนเที่ยว",
            "ง้อ",
            "ขอบคุณ",
            "Take Care",
    };

    public ShareModel(int id, int type, int activity, int actor, int hour, int minute) {
        this.type = type;
        this.id = id;
        this.activity = activity;
        this.actor = actor;
        this.hour = hour;
        this.minute = minute;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getActivityTH() {
        if (type == 1) {
            return activity_th[activity];
        }
        else {
            return activity_th2[activity];
        }
    }

    public int getActivity() {
        return activity;
    }

    public void setActivity(int activity) {
        this.activity = activity;
    }

    public int getActor() {
        return actor;
    }

    public void setActor(int actor) {
        this.actor = actor;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public String getTime() {
        return String.format("%02d:%02d", hour, minute);
    }
}
