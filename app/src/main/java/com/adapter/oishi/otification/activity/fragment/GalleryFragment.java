package com.adapter.oishi.otification.activity.fragment;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import com.acbelter.directionalcarousel.CarouselPagerAdapter;
import com.acbelter.directionalcarousel.CarouselViewPager;
import com.acbelter.directionalcarousel.page.OnPageClickListener;
import com.adapter.oishi.OishiApplication;
import com.adapter.oishi.R;
import com.adapter.oishi.otification.model.PlayListGalleryModel;
import com.adapter.oishi.otification.utility.PreferenceService;
import com.adapter.oishi.otification.utility.pager.ActivityPageItem;
import com.adapter.oishi.otification.utility.service.HTTPService;
import com.facebook.CallbackManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GalleryFragment extends Fragment {

    private OishiApplication app;

    private int actorSelected = 0;
    private CarouselViewPager carouselActor;
    private ArrayList<ActivityPageItem> itemsActor;
    private CarouselPagerAdapter<ActivityPageItem> adapterActor;

    private TextView textViewName;
    private LinearLayout verticalLinearLayout;

    private ImageView imageViewWhite;

    private CallbackManager callbackManager;

    private String[] actor = {
            "พุฒ พุฒิชัย",
            "ต่อ ธนภพ",
            "มาร์ช จุฑาวุฒิ",
            "กั้ง วรกร",
            "มุก วรนิษฐ์",
            "ออฟ จุมพล",
    };

    private static final int[] resActor = {
            R.drawable.img_char0,
            R.drawable.img_char1,
            R.drawable.img_char4,
            R.drawable.img_char3,
            R.drawable.img_char5,
            R.drawable.img_char2
    };

    public GalleryFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.otification_fragment_gallery, container, false);

        app = (OishiApplication) getActivity().getApplication();
        app.sendPageStat("page_gallery");

        textViewName = (TextView) rootView.findViewById(R.id.textViewName);
        textViewName.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/DB HelvethaicaMon X Bd.ttf"));
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);

        int widthPixels = metrics.widthPixels;
        if (widthPixels < 600) {
            textViewName.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
        }

        carouselActor = (CarouselViewPager) rootView.findViewById(R.id.carouselActor);
        carouselActor.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                switch (state) {
                    case 2:
                        carouselActor.setPagingEnabled(false);
                        break;
                    case 0:
                        carouselActor.setPagingEnabled(true);
                        break;
                }
            }
        });

        itemsActor = new ArrayList<ActivityPageItem>(resActor.length);
        for (int i = 0; i < resActor.length; i++) {
            itemsActor.add(new ActivityPageItem(resActor[i], -1, null));
        }

        adapterActor = new CarouselPagerAdapter<ActivityPageItem>(getContext(), itemsActor);
        adapterActor.setOnPageClickListener(new OnPageClickListener<ActivityPageItem>() {
            @Override
            public void onSingleTap(View view, ActivityPageItem item) {

            }

            @Override
            public void onDoubleTap(View view, ActivityPageItem item) {

            }

            @Override
            public void onSelected(final int position) {
//                new Thread() {
//                    @Override
//                    public void run() {
//                        getActivity().runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
                                int i = position % resActor.length;
                                actorSelected = i;

                                (new PreferenceService(getContext())).putString("actorSelectedGallery", String.valueOf(actorSelected));

                                if (actorActivity != null) {
                                    setActivity();
                                }
//                            }
//                        });
//                    }
//                }.start();

            }
        });

        imageViewWhite = (ImageView) rootView.findViewById(R.id.imageViewWhite);

        carouselActor.setAdapter(adapterActor);
        carouselActor.setOffscreenPageLimit(6);
        carouselActor.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (state == 0) {
                    imageViewWhite.setVisibility(View.VISIBLE);
                }
                else {
                    imageViewWhite.setVisibility(View.INVISIBLE);
                }

            }
        });

        verticalLinearLayout = (LinearLayout) rootView.findViewById(R.id.verticalLinearLayout);

        app.getHttpService().getPlaylistGallery(new HTTPService.OnResponseCallback<JSONObject>() {
            @Override
            public void onResponse(boolean success, Throwable error, JSONObject data) {
                if (!(data == null)) {
                    try {

                        playListGalleryModels = new ArrayList<PlayListGalleryModel>();

                        JSONArray group = data.getJSONObject("playlist").getJSONArray("actor");
                        for (int i = 0; i < group.length(); i++) {

                            JSONObject g = group.getJSONObject(i);

                            int actor = g.getInt("no") - 1;

                            JSONArray list = g.getJSONArray("list");
                            for (int j = 0; j < list.length(); j++) {
                                JSONObject l = list.getJSONObject(j);

                                int[][] typeNoToActivity = {
                                        {-1, 0, 1, 2, 3, 4, -1, -1, -1},
                                        {-1, 0, 1, 2, 3, 4, 5, 6, 7}
                                };

                                int type = l.getInt("type") - 1;
                                int no = l.getInt("no");
                                int activity = typeNoToActivity[type][no];


                                playListGalleryModels.add(new PlayListGalleryModel(type, activity, actor, l.getInt("active"), l.getString("name"), l.getString("video"), l.getString("gallery_image"), l.getString("share_url"), l.getString("share_title"), l.getString("share_description"), l.getString("share_image")));
                            }
                        }

                        actorActivity = (ArrayList<PlayListGalleryModel>[]) new ArrayList[6];
                        for (int i = 0; i < 6; i++) {
                            actorActivity[i] = new ArrayList<PlayListGalleryModel>();
                        }
                        for (int i = 0; i < playListGalleryModels.size(); i++) {
                            actorActivity[playListGalleryModels.get(i).getActor()].add(playListGalleryModels.get(i));
                        }

                        setActivity();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getContext(), "เชื่อมต่ออินเตอร์เน็ตไม่ได้", Toast.LENGTH_SHORT).show();
                }
            }
        });

        callbackManager = ((MainActivity) getActivity()).setFacebookResponseWaitingFragmentToShare(GalleryFragment.this);

        return rootView;
    }


    private ArrayList<PlayListGalleryModel> playListGalleryModels;
    private ArrayList<PlayListGalleryModel>[] actorActivity;
    private GalleryGridAdapter adapter;

    public void setActivity() {
        textViewName.setText(actor[actorSelected]);

        adapter = new GalleryGridAdapter(getActivity().getApplicationContext(), getActivity(), this, verticalLinearLayout, actorActivity[actorSelected]);
        adapter.load();
    }
}
