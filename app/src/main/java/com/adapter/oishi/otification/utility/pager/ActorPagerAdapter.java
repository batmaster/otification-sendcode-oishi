package com.adapter.oishi.otification.utility.pager;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.SparseArray;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.adapter.oishi.OishiApplication;
import com.adapter.oishi.R;
import com.adapter.oishi.otification.activity.LoadingActivity;
import com.adapter.oishi.otification.activity.PreviewVDOActivity;
import com.adapter.oishi.otification.model.PlayListAlarmModel;
import com.adapter.oishi.otification.model.PlayListFriendModel;
import com.adapter.oishi.otification.utility.PreferenceService;
import com.adapter.oishi.otification.utility.service.HTTPService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ActorPagerAdapter extends PagerAdapter implements ViewPager.PageTransformer {

    private SparseArray<View> charImageCollection = new SparseArray<View>();

    public final static float BIG_SCALE = 1.0f;
    public final static float SMALL_SCALE = 0.7f;
    public final static float DIFF_SCALE = BIG_SCALE - SMALL_SCALE;

    private ActorLinearLayout cur = null;
    private ActorLinearLayout next = null;
    private Context context;
    private FragmentManager fm;
    private Fragment fragment;
    private float scale;

    private int[] actors;
    private int[] actorsActive;
    private boolean forCreateAlarm;
    private Activity activity;

    private OishiApplication app;

    public ActorPagerAdapter(Activity activity, Context context, Fragment fragment, FragmentManager fm, int[] actors, int[] actorsActive, boolean forCreateAlarm) {
//        super(fm);
        this.fm = fm;
        this.activity = activity;
        this.context = context;
        this.fragment = fragment;
        this.actors = actors;
        this.actorsActive = actorsActive;
        this.forCreateAlarm = forCreateAlarm;

        app = (OishiApplication) context.getApplicationContext();

    }

    public void setActors(int[] actors, int[] actorsActive) {
        this.actors = actors;
        this.actorsActive = actorsActive;
    }

    private ArrayList<PlayListFriendModel> models;
    public void setModelsForFriend(ArrayList<PlayListFriendModel> models) {
        this.models = models;
    }

//    @Override
//    public Fragment getItem(int position) {
//        // make the first pager bigger than others
//        if (position == actors.length)
//            scale = BIG_SCALE;
//        else
//            scale = SMALL_SCALE;
//
//        position = position % actors.length;
//
//        return ActorFragment.newInstance(context, actors[position], actors.length, scale, forCreateAlarm);
////        return new ActorFragment(context, position, scale);
//    }

    private int[] res = {
            R.drawable.img_1,
            R.drawable.img_2,
            R.drawable.img_5,
            R.drawable.img_4,
            R.drawable.img_6,
            R.drawable.img_3,
    };

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {

//        LayoutInflater inflater = LayoutInflater.from(context);
//        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.tt, collection, false);
//        collection.addView(layout);
//        return layout;


        LayoutInflater inflater = LayoutInflater.from(context);
        LinearLayout l = (LinearLayout) inflater.inflate(R.layout.otification_carousel_cell, collection, false);

//        if (position == actors.length)
//            scale = BIG_SCALE;
//        else
//            scale = SMALL_SCALE;

        position = position % actors.length;


        final int actor = position;

        Log.d("actor", actor + "");

        ImageView im = (ImageView) l.findViewById(R.id.im);
        im.setImageResource(res[actors[actor]]);
        im.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                int p = actor;
//                int p = actor - 1;
//                if (p == -1) {
//                    p = actors.length - 1;
//                }

                final int finalP = p;
                final int a = Integer.parseInt((new PreferenceService(context)).getString("activitySelected"));
                if (forCreateAlarm) {
                    app.getHttpService().getPlaylistAlarm(new HTTPService.OnResponseCallback<JSONObject>() {
                        @Override
                        public void onResponse(boolean success, Throwable error, JSONObject data) {
                            if (!(data == null)) {
                                try {
                                    JSONArray group = data.getJSONObject("playlist").getJSONArray("group");
                                    for (int i = 0; i < group.length(); i++) {

                                        JSONObject g = group.getJSONObject(i);

                                        int activity = g.getInt("no") - 1;

                                        JSONArray list = g.getJSONArray("list");
                                        for (int j = 0; j < list.length(); j++) {
                                            JSONObject l = list.getJSONObject(j);

                                            int actor = l.getInt("actor") - 1;

                                            if (activity == a && actor == actors[finalP]) {
                                                final PlayListAlarmModel media = new PlayListAlarmModel(activity, actor, l.getString("name"), l.getString("noti_title"), l.getString("noti_message"), l.getString("video"), l.getString("audio_android"), l.getString("share_url"), l.getString("share_title"), l.getString("share_description"), l.getString("share_image"));

                                                String vid = media.getVideo().split("/")[media.getVideo().split("/").length - 1].replace(".mp4", "");
                                                app.sendButtonStat("preview_" + vid);

                                                if (app.getPlayListAlarmDB().checkHasToDownload(media)) {
                                                    Intent intent = new Intent(context, LoadingActivity.class);
                                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                    intent.putExtra("media", media);
                                                    intent.putExtra("shouldPreview", true);
                                                    intent.putExtra("isFromAlarm", true);
                                                    context.startActivity(intent);
                                                } else {
                                                    Intent intent = new Intent(context, PreviewVDOActivity.class);
                                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                    intent.putExtra("activity", a);
                                                    intent.putExtra("actor", actors[finalP]);
                                                    intent.putExtra("isFromAlarm", true);
                                                    context.startActivity(intent);
                                                }

                                            }


                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            } else {
                                Toast.makeText(context, "เชื่อมต่ออินเตอร์เน็ตไม่ได้", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                } else {

                    Log.d("actadap", "activity == " + a + " && actor == " + actors[finalP]);
                    app.getHttpService().getPlaylistFriend(new HTTPService.OnResponseCallback<JSONObject>() {
                        @Override
                        public void onResponse(boolean success, Throwable error, JSONObject data) {
                            if (!(data == null)) {
                                try {
                                    JSONArray group = data.getJSONObject("playlist").getJSONArray("group");
                                    for (int i = 0; i < group.length(); i++) {

                                        JSONObject g = group.getJSONObject(i);

                                        int activity = g.getInt("no") - 1;

                                        JSONArray list = g.getJSONArray("list");
                                        for (int j = 0; j < list.length(); j++) {
                                            JSONObject l = list.getJSONObject(j);

                                            int actor = l.getInt("actor") - 1;

                                            Log.d("actadap", activity + " " + actor);

                                            if (activity == a && actor == actors[finalP]) {
                                                PlayListFriendModel media = new PlayListFriendModel(activity, actor, l.getInt("active"), l.getString("name"), l.getString("video"), l.getString("gallery_image"), l.getString("share_url"), l.getString("share_title"), l.getString("share_description"), l.getString("share_image"));

                                                String vid = media.getVideo().split("/")[media.getVideo().split("/").length - 1].replace(".mp4", "");
                                                app.sendButtonStat("preview_" + vid);

                                                if (app.getPlayListFriendDB().checkHasToDownload(media)) {
                                                    Intent intent = new Intent(context, LoadingActivity.class);
                                                    intent.putExtra("media", media);
                                                    intent.putExtra("shouldPreview", true);
                                                    intent.putExtra("isFromAlarm", false);
                                                    context.startActivity(intent);
                                                } else {
                                                    Intent intent = new Intent(context, PreviewVDOActivity.class);
                                                    intent.putExtra("activity", a);
                                                    intent.putExtra("actor", actors[finalP]);
                                                    context.startActivity(intent);
                                                }
                                            }
                                        }
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            } else {
                                Toast.makeText(context, "เชื่อมต่ออินเตอร์เน็ตไม่ได้", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                }


            }
        });

        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;

//        if (width > 1500) {
        im.setDrawingCacheEnabled(true);
        im.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Bitmap bmp = Bitmap.createBitmap(v.getDrawingCache());
                try {
                    int color = bmp.getPixel((int) event.getX(), (int) event.getY());
                    if (color == Color.TRANSPARENT)
                        return true;
                    else {
                        return false;
                    }
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                    return true;
                }
            }
        });
//        }

        if (actorsActive[actor] == 0) {
            im.setAlpha(0.5f);
            im.setOnClickListener(null);
        }

        ActorLinearLayout root = (ActorLinearLayout) l.findViewById(R.id.root);
        root.setScaleBoth(1.0f);

        charImageCollection.put(actor, im);

        collection.addView(l);

        return root;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);

        System.runFinalization();
        Runtime.getRuntime().gc();
        System.gc();
    }

    @Override
    public int getCount() {
        return 6 * 1000;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void transformPage(View page, float position) {
//        ActorLinearLayout actorLinearLayout = (ActorLinearLayout) page.findViewById(R.id.root);
//        float scale = BIG_SCALE;
//        if (position > 0) {
//            scale = scale - position * DIFF_SCALE;
//        } else {
//            scale = scale + position * DIFF_SCALE;
//        }
//        if (scale < 0) scale = 0;
//        actorLinearLayout.setScaleBoth(scale);
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    public SparseArray<View> getCharImageCollection() {
        return charImageCollection;
    }


}
