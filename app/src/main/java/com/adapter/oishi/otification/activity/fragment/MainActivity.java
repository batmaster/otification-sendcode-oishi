package com.adapter.oishi.otification.activity.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;

import com.adapter.oishi.Config;
import com.adapter.oishi.OishiApplication;
import com.adapter.oishi.R;
import com.adapter.oishi.activity.NewHomeActivity;
import com.adapter.oishi.otification.activity.LogoActivity;
import com.adapter.oishi.otification.activity.ThankYouDialog;
import com.adapter.oishi.otification.activity.TutorialActivity;
import com.adapter.oishi.otification.model.PlayListAlarmModel;
import com.adapter.oishi.otification.utility.service.HTTPService;
import com.adapter.oishi.sendcode.activities.custom.LandingActivity;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

import org.json.JSONObject;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends FragmentActivity {

    private OishiApplication app;

    private DrawerLayout drawerLayout;
    private ImageView imageViewMenu;

    private ImageView blankMenuBackground;
    private ImageView imageViewClose;
    private ImageView imageViewMenu0;
    private ImageView imageViewMenu1;
    private ImageView imageViewMenu2;
    private ImageView imageViewMenu3;
    private ImageView imageViewMenu4;
    private ImageView imageViewMenu5;
    private ImageView imageViewMenuFacebook;
    private ImageView imageViewMenuLine;

    private CallbackManager callbackManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otification_activity_main);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/DB HelvethaicaMon X.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());

        app = (OishiApplication) getApplication();
        if (Config.isPassed(getApplicationContext())) {
            startActivity(new Intent(getApplicationContext(), LogoActivity.class));
            finish();
        }

        app.getHttpService().sendOpenAppStat(HTTPService.STAT_OTI);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        View drawer = (View) findViewById(R.id.drawer);
        drawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        imageViewMenu = (ImageView) findViewById(R.id.imageViewMenu);
        imageViewMenu.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                app.sendButtonStat("bnt_menu");

                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawers();
                } else {
                    drawerLayout.openDrawer(Gravity.RIGHT);
                }

            }
        });

        imageViewClose = (ImageView) drawer.findViewById(R.id.imageViewClose);
        imageViewClose.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawers();
            }
        });

        imageViewMenu0 = (ImageView) drawer.findViewById(R.id.imageViewMenu0);
        imageViewMenu0.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (Config.getInt(getApplicationContext(), Config.sendcode_show_at_home) == 0) {
                    setFragment(1, null);
                }
                else {
                    startActivity(new Intent(getApplicationContext(), NewHomeActivity.class));
                    finish();
                }
            }
        });

        imageViewMenu1 = (ImageView) drawer.findViewById(R.id.imageViewMenu1);
        imageViewMenu1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                app.sendButtonStat("bnt_create");

                setFragment(0, null);
            }
        });

        imageViewMenu2 = (ImageView) drawer.findViewById(R.id.imageViewMenu2);
        imageViewMenu2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                app.sendButtonStat("bnt_list");

                setFragment(1, null);
            }
        });

        imageViewMenu3 = (ImageView) drawer.findViewById(R.id.imageViewMenu3);
        imageViewMenu3.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                app.sendButtonStat("bnt_howto");

                Intent intent = new Intent(getApplicationContext(), TutorialActivity.class);
                startActivity(intent);

                drawerLayout.closeDrawers();
            }
        });

        imageViewMenu4 = (ImageView) drawer.findViewById(R.id.imageViewMenu4);
        imageViewMenu4.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                app.sendButtonStat("bnt_gallery");

                setFragment(2, null);
            }
        });

        imageViewMenu5 = (ImageView) drawer.findViewById(R.id.imageViewMenu5);
        imageViewMenu5.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
//                setFragment(3, null);
            }
        });

        imageViewMenuFacebook = (ImageView) drawer.findViewById(R.id.imageViewMenuFacebook);
        imageViewMenuFacebook.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                app.sendButtonStat("bnt_main-share-FB");

                app.getHttpService().checkActiveApp(new HTTPService.OnResponseCallback<JSONObject>() {
                    @Override
                    public void onResponse(boolean success, Throwable error, JSONObject data) {
                        if (!(data == null)) {
                            try {
                                drawerLayout.closeDrawers();

                                JSONObject otification = data.getJSONArray("appdata").getJSONObject(0);
                                String share_url = otification.getString("share_url");
                                String share_title = otification.getString("share_title");
                                String share_description = otification.getString("share_description");
                                String share_image = otification.getString("share_image");


                                ShareLinkContent content = new ShareLinkContent.Builder()
                                        .setImageUrl(Uri.parse(share_image))
                                        .setContentUrl(Uri.parse(share_url))
                                        .setContentTitle(share_title)
                                        .setContentDescription(share_description)
                                        .build();
                                Log.d("facee", "dialog ok");

                                ShareDialog dialog = new ShareDialog(MainActivity.this);
                                dialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                                    @Override
                                    public void onSuccess(Sharer.Result result) {
                                        Log.d("facee", "onSuccess");

                                    }

                                    @Override
                                    public void onCancel() {
                                        Log.d("facee", "onCancel()");
                                    }

                                    @Override
                                    public void onError(FacebookException error) {
                                        Log.d("facee", "onError");
                                        Log.d("share 2", "" + error.getMessage());
                                    }
                                });
                                if (dialog.canShow(ShareLinkContent.class)) {
                                    dialog.show(content);
                                } else {
                                    Log.d("facee", "you cannot share :(");
                                }



                            } catch (Exception e) {

                            }
                        }
                    }
                });



            }
        });

        imageViewMenuLine = (ImageView) drawer.findViewById(R.id.imageViewMenuLine);
        imageViewMenuLine.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                app.sendButtonStat("bnt_main-share-line");

                app.getHttpService().checkActiveApp(new HTTPService.OnResponseCallback<JSONObject>() {
                    @Override
                    public void onResponse(boolean success, Throwable error, JSONObject data) {
                        if (!(data == null)) {
                            try {
                                JSONObject otification = data.getJSONArray("appdata").getJSONObject(0);
                                String share_url = otification.getString("share_url");

                                startActivity(new Intent(getApplicationContext(), ThankYouDialog.class));

                                try {
                                    String shareBody = "line://msg/text/" + share_url;
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(shareBody)));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                drawerLayout.closeDrawers();
                            } catch (Exception e) {

                            }
                        }
                    }
                });
            }
        });

        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                if (tmpFragment instanceof  ShareAlarmFragment) {
                    ((ShareAlarmFragment) tmpFragment).share();
                }

            }

            @Override
            public void onCancel() {
                int a = 0;
            }

            @Override
            public void onError(FacebookException error) {
                int a = 0;
            }
        });

        if (getIntent().getIntExtra("index", -1) == 0) {
            setFragment(0, null);
        } else {
            setFragment(1, null);
        }

        boolean fromAlarmActivity = getIntent().getBooleanExtra("fromAlarmActivity", false);
        Log.d("fromAlarmActivity", fromAlarmActivity ? "yes" : "no");
        if (fromAlarmActivity) {
            app.getHttpService().checkActiveApp(new HTTPService.OnResponseCallback<JSONObject>() {
                @Override
                public void onResponse(boolean success, Throwable error, JSONObject data) {

                }
            });
        }
    }


    private int currentFragmentIndex = -1;

    public void setFragment(int index, Bundle bundle) {
        customClipFragment = null;
//        if (index != currentFragmentIndex) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        Fragment f = null;
        if (index == 0) {
            f = new CreateAlarmFragment();

            transaction.replace(R.id.container, f);
            transaction.commit();
        } else if (index == 1) {
            f = new MyListFragment();

            transaction.replace(R.id.container, f);
            transaction.commit();
        } else if (index == 2) {
            f = new GalleryFragment();

            transaction.replace(R.id.container, f);
            transaction.commit();
        } else if (index == 3) {
            f = new ShareAlarmFragment();

            transaction.replace(R.id.container, f);
            transaction.commit();
        } else if (index == 4) {
            f = new EditAlarmFragment();
            f.setArguments(bundle);

            transaction.replace(R.id.container, f);
            transaction.commit();
        } else if (index == 5) {
            f = new CustomClipFragment();
            f.setArguments(bundle);
            customClipFragment = f;

            transaction.add(R.id.container, f, "CustomClipFragment");
            transaction.commit();
        } else if (index == 6) {
            f = new ShareAlarmFragment();

            transaction.replace(R.id.container, f);
            transaction.commit();
        } else if (index == 7) {
            f = new MyListFriendFragment();

            transaction.replace(R.id.container, f);
            transaction.commit();
        }

        currentFragmentIndex = index;
//        }

        drawerLayout.closeDrawers();
    }

    private Fragment customClipFragment;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        callbackManager.onActivityResult(requestCode, resultCode, data);
//        Log.d("requestCodee", requestCode + "");

//        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 1909 && data.getIntExtra("result", -1) == 1) {
                int activitySelected = data.getIntExtra("activity", -1);
                int actorSelected = data.getIntExtra("actor", -1);
                int hour = data.getIntExtra("hour", -1);
                int minute = data.getIntExtra("minute", -1);

                Log.d("savee", hour + " " + minute + " " + activitySelected + " " + actorSelected);
                app.getAlarmDB().addAlarm(activitySelected, actorSelected, hour, minute);

                PlayListAlarmModel media = (PlayListAlarmModel) data.getSerializableExtra("media");
                int videoId = Integer.parseInt(media.getVideo().split("/")[media.getVideo().split("/").length - 1].replace(".mp4", ""));
                app.getHttpService().saveGame(true, hour, minute, videoId, -1, new HTTPService.OnResponseCallback<JSONObject>() {
                    @Override
                    public void onResponse(boolean success, Throwable error, JSONObject data) {

                    }
                });

                Intent intent = new Intent(getApplicationContext(), ThankYouDialog.class);
                intent.putExtra("media", media);
                startActivity(intent);

                setFragment(1, null);
            }
            else if (requestCode == 1908) {
                ((CustomClipFragment) customClipFragment).setResultRecordVideo(data != null && data.getBooleanExtra("ok", false));
            }
            else if (requestCode == 1907) {
                ((ShareAlarmFragment) tmpFragment).share();
            }
            else if (requestCode == 1678) {
                setFragment(7, null);
            }
//        }
    }

    private Fragment tmpFragment;
    public CallbackManager setFacebookResponseWaitingFragmentToShare(Fragment fragment) {
        tmpFragment = fragment;
        return callbackManager;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(base));
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawers();
        }
        else {
            if (currentFragmentIndex == 5) {
                setFragment(0, null);
            } else if (currentFragmentIndex == 1) {
                if (Config.getInt(getApplicationContext(), Config.sendcode_show_at_home) == 0) {
                    finish();
                }
                else {
                    startActivity(new Intent(getApplicationContext(), NewHomeActivity.class));
                    finish();
                }
            } else {
                setFragment(1, null);
            }
        }
    }
}
