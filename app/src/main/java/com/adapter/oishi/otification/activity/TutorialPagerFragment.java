package com.adapter.oishi.otification.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.adapter.oishi.R;

/**
 * Created by batmaster on 8/25/16 AD.
 */
public class TutorialPagerFragment extends Fragment {


    private int index = 0;
    private int[] res = {
            R.drawable.bg_tutorial1,
            R.drawable.bg_tutorial2,
            R.drawable.bg_tutorial3,
            R.drawable.bg_tutorial4,
            R.drawable.bg_tutorial5
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.otification_fragment_tutorial, container, false);

        final ImageView imageView = (ImageView) rootView.findViewById(R.id.imageView);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (index < res.length - 1) {
                    imageView.setImageResource(res[++index]);
                }
            }
        });

        return rootView;
    }
}
