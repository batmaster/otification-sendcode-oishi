package com.adapter.oishi.otification.utility;

/**
 * Created by batmaster on 9/1/2016 AD.
 */

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.adapter.oishi.R;

/**
 * Created by Bestiize on 8/12/2558.
 */
public class StrokeTextView extends TextView {
    private boolean stroke = false;
    private float strokeWidth = 0.0f;
    private int strokeColor;
    public StrokeTextView(Context context) {
        super(context);

    }

    public StrokeTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context, attrs);
    }

    public StrokeTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);
    }
    private void initView(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.StrokeTextview);
        stroke = a.getBoolean(R.styleable.StrokeTextview_textStroke, false);
        strokeWidth = a.getFloat(R.styleable.StrokeTextview_textStrokeWidth, 0.0f);
        strokeColor = a.getColor(R.styleable.StrokeTextview_textStrokeColor, 0xffffffff);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        if (stroke) {
            ColorStateList states = getTextColors();
            getPaint().setStyle(Paint.Style.STROKE);
            getPaint().setStrokeWidth(strokeWidth);
            setTextColor(strokeColor);
            super.onDraw(canvas);

            getPaint().setStyle(Paint.Style.FILL);
            setTextColor(states);
        }

        super.onDraw(canvas);
    }
}
