package com.adapter.oishi.otification.activity;

import android.app.Activity;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.ImageView;
import android.widget.MediaController;

import com.adapter.oishi.OishiApplication;
import com.adapter.oishi.R;
import com.adapter.oishi.otification.model.PlayListAlarmModel;
import com.adapter.oishi.otification.model.PlayListFriendModel;
import com.adapter.oishi.otification.model.PlayListGalleryModel;
import com.adapter.oishi.otification.utility.FullScreenVideoView;

public class PreviewVDOActivity extends Activity {

    private OishiApplication app;

    private FullScreenVideoView videoView;
    private ImageView imageViewClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otification_activity_preview_vdo);

        app = (OishiApplication) getApplication();

        int activity = getIntent().getIntExtra("activity", -1);
        int actor = getIntent().getIntExtra("actor", -1);
        boolean isFromAlarm = getIntent().getBooleanExtra("isFromAlarm", false);
        boolean isFromGallery = getIntent().getBooleanExtra("isFromGallery", false);

        String previewCustom_filename = getIntent().getStringExtra("previewCustom_filename");

        imageViewClose = (ImageView) findViewById(R.id.imageViewClose);
        imageViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                app.sendButtonStat("bnt_Preview_close");

                finish();
            }
        });


        Uri video = null;
        if (previewCustom_filename != null) {
            video = Uri.parse(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + "/oishi/" + previewCustom_filename + ".mp4");
        }
        else if (isFromAlarm) {
            PlayListAlarmModel media = app.getPlayListAlarmDB().getApiModel(activity, actor);
            video = Uri.parse(getApplicationContext().getFileStreamPath(media.getVideoFileName()).getAbsolutePath());
        }
        else if (isFromGallery) {
            int type = getIntent().getIntExtra("type", -1);
            PlayListGalleryModel media = (PlayListGalleryModel) getIntent().getSerializableExtra("media");
            video = Uri.parse(getApplicationContext().getFileStreamPath(media.getVideoFileName()).getAbsolutePath());
        }
        else {
            PlayListFriendModel media = app.getPlayListFriendDB().getApiModel(activity, actor);
            video = Uri.parse(getApplicationContext().getFileStreamPath(media.getVideoFileName()).getAbsolutePath());
        }

        videoView = (FullScreenVideoView) findViewById(R.id.videoView);
        MediaController mediaController = new MediaController(this);
        mediaController.setAnchorView(videoView);
        videoView.setMediaController(mediaController);
        videoView.setVideoURI(video);
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                finish();
            }
        });

        videoView.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//
//        System.runFinalization();
//        Runtime.getRuntime().gc();
//        System.gc();
    }
}
