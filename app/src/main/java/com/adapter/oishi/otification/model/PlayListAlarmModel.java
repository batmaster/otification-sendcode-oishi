package com.adapter.oishi.otification.model;

import java.io.Serializable;

public class PlayListAlarmModel implements Serializable {

    private int activity;
    private int actor;

    private String name;
    private String noti_title;
    private String noti_message;

    private String video;
    private String audio;

    private String sUrl;
    private String sTitle;
    private String sDescription;
    private String sImage;

    public PlayListAlarmModel(int activity, int actor, String name, String noti_title, String noti_message, String video, String audio, String sUrl, String sTitle, String sDescription, String sImage) {
        this.activity = activity;
        this.actor = actor;
        this.name = name;
        this.noti_title = noti_title;
        this.noti_message = noti_message;
        this.video = video;
        this.audio = audio;
        this.sUrl = sUrl;
        this.sTitle = sTitle;
        this.sDescription = sDescription;
        this.sImage = sImage;
    }

    public int getActivity() {
        return activity;
    }

    public void setActivity(int activity) {
        this.activity = activity;
    }

    public int getActor() {
        return actor;
    }

    public void setActor(int actor) {
        this.actor = actor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNoti_title() {
        return noti_title;
    }

    public void setNoti_title(String noti_title) {
        this.noti_title = noti_title;
    }

    public String getNoti_message() {
        return noti_message;
    }

    public void setNoti_message(String noti_message) {
        this.noti_message = noti_message;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public String getsUrl() {
        return sUrl;
    }

    public void setsUrl(String sUrl) {
        this.sUrl = sUrl;
    }

    public String getsTitle() {
        return sTitle;
    }

    public void setsTitle(String sTitle) {
        this.sTitle = sTitle;
    }

    public String getsDescription() {
        return sDescription;
    }

    public void setsDescription(String sDescription) {
        this.sDescription = sDescription;
    }

    public String getsImage() {
        return sImage;
    }

    public void setsImage(String sImage) {
        this.sImage = sImage;
    }

    public String getVideoFileName() {
        String[] s = video.split("/");
        return s[s.length - 1];
    }

    public String getAudioFileName() {
        String[] s = audio.split("/");
        return s[s.length - 1];
    }

    @Override
    public String toString() {
        return "PlayListAlarmModel{" +
                "activity=" + activity +
                ", actor=" + actor +
                ", name='" + name + '\'' +
                ", noti_title='" + noti_title + '\'' +
                ", noti_message='" + noti_message + '\'' +
                ", video='" + video + '\'' +
                ", audio='" + audio + '\'' +
                ", sUrl='" + sUrl + '\'' +
                ", sTitle='" + sTitle + '\'' +
                ", sDescription='" + sDescription + '\'' +
                ", sImage='" + sImage + '\'' +
                '}';
    }
}
