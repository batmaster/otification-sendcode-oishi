package com.adapter.oishi.otification;

import android.content.Context;
import android.content.Intent;

import com.adapter.oishi.otification.activity.LogoActivity;
import com.parse.ParsePushBroadcastReceiver;

/**
 * Created by Bestiize on 8/2/2559.
 */
public class OishiPushNotificationReciever extends ParsePushBroadcastReceiver {

    @Override
    protected void onPushOpen(Context context, Intent intent) {
        //super.onPushOpen(context, intent);

        Intent intent1 = new Intent(context, LogoActivity.class);
        intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent1);
    }


}
