package com.adapter.oishi.otification.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.adapter.oishi.Config;
import com.adapter.oishi.OishiApplication;
import com.adapter.oishi.R;
import com.adapter.oishi.activity.CommemorationActivity;
import com.adapter.oishi.activity.NewHomeActivity;
import com.adapter.oishi.otification.utility.SharePref;
import com.adapter.oishi.otification.utility.service.HTTPService;
import com.adapter.oishi.sendcode.activities.custom.LandingActivity;
import com.adapter.oishi.sendcode.views.dialog.NoticeWinnerDialog;
import com.adapter.oishi.sendcode.views.dialog.SorryDialog;
import com.adapter.oishi.sendcode.views.dialog.ThankyouDialog;
import com.facebook.AccessToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

public class LogoActivity extends Activity {

    private OishiApplication app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otification_activity_logo);

        app = (OishiApplication) getApplication();

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        int widthPixels = metrics.widthPixels;
        int heightPixels = metrics.heightPixels;


        if ((double)heightPixels / widthPixels <= 3.0/2) {
            ((ImageView) findViewById(R.id.imageViewBackground)).setImageResource(R.drawable.bg_logo_fat);
        }

        if (AccessToken.getCurrentAccessToken() == null && SharePref.getStringRid(getApplicationContext()) == null) {
            SharePref.setStringRid(getApplicationContext(), "fbid_" + UUID.randomUUID().toString());
        }

        app.getHttpService().checkActiveApp(new HTTPService.OnResponseCallback<JSONObject>() {
            @Override
            public void onResponse(boolean success, Throwable error, JSONObject data) {
                if (!(data == null)) {
                    try {

                        String switch_artwork_date = data.getString("switch_artwork_date");

                        JSONArray appdata = data.getJSONArray("appdata");

                        int oti_show_at_home = appdata.getJSONObject(0).getInt("show_at_home");
                        int oti_active = appdata.getJSONObject(0).getInt("active");
                        String oti_noti_final_date = appdata.getJSONObject(0).getString("noti_final_date");

                        int sendcode_show_at_home = appdata.getJSONObject(1).getInt("show_at_home");
                        int sendcode_active = appdata.getJSONObject(1).getInt("active");
                        String sendcode_poll_start_date = appdata.getJSONObject(1).getString("poll_start_date");

                        String page_prize = appdata.getJSONObject(1).getString("page_prize");
                        String page_rule = appdata.getJSONObject(1).getString("page_rule");
                        String page_winner = appdata.getJSONObject(1).getString("page_winner");
                        String page_howto = appdata.getJSONObject(1).getString("page_howto");
                        String page_poll = appdata.getJSONObject(1).getString("page_poll");

                        String share_url = appdata.getJSONObject(1).getString("share_url");
                        String share_title = appdata.getJSONObject(1).getString("share_title");

                        String youtube_sendcode = appdata.getJSONObject(1).getString("youtube");
                        String sendcode_api_stat = appdata.getJSONObject(1).getString("api_stat");

                        Config.setStringDate(getApplicationContext(), Config.switch_artwork_date, switch_artwork_date);
                        Config.setInt(getApplicationContext(), Config.oti_show_at_home, oti_show_at_home);
                        Config.setInt(getApplicationContext(), Config.oti_active, oti_active);
                        Config.setStringDate(getApplicationContext(), Config.oti_noti_final_date, oti_noti_final_date);
                        Config.setInt(getApplicationContext(), Config.sendcode_show_at_home, sendcode_show_at_home);
                        Config.setInt(getApplicationContext(), Config.sendcode_active, sendcode_active);
                        Config.setStringDate(getApplicationContext(), Config.sendcode_poll_start_date, sendcode_poll_start_date);

                        Config.setString(getApplicationContext(), Config.sendcode_page_prize, page_prize);

                        Config.setString(getApplicationContext(), Config.sendcode_page_rule, page_rule);
                        Config.setString(getApplicationContext(), Config.sendcode_page_winner, page_winner);
                        Config.setString(getApplicationContext(), Config.sendcode_page_howto, page_howto);
                        Config.setString(getApplicationContext(), Config.sendcode_page_poll, page_poll);

                        Config.setString(getApplicationContext(), Config.sendcode_share_url, share_url);
                        Config.setString(getApplicationContext(), Config.sendcode_share_title, share_title);

                        Config.setString(getApplicationContext(), Config.youtube_sendcode, youtube_sendcode);
                        Config.setString(getApplicationContext(), Config.sendcode_api_stat, sendcode_api_stat);

                        if (Config.isPassed(getApplicationContext())) {
                            app.getAlarmDB().deleteAllAlarm();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), "เชื่อมต่ออินเตอร์เน็ตไม่ได้", Toast.LENGTH_SHORT).show();
                }

                new CountDownTimer(1500, 1000) {

                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        startActivity(new Intent(getApplicationContext(), CommemorationActivity.class));
                        finish();

                    }
                }.start();

            }
        });

        app.getHttpService().sendOpenAppStat(HTTPService.STAT_MAIN);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.runFinalization();
        Runtime.getRuntime().gc();
        System.gc();

    }

    @Override
    public void onBackPressed() {

    }
}
