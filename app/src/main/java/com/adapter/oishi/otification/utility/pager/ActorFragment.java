package com.adapter.oishi.otification.utility.pager;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.adapter.oishi.OishiApplication;
import com.adapter.oishi.R;
import com.adapter.oishi.otification.activity.LoadingActivity;
import com.adapter.oishi.otification.activity.PreviewVDOActivity;
import com.adapter.oishi.otification.model.PlayListAlarmModel;
import com.adapter.oishi.otification.utility.PreferenceService;
import com.adapter.oishi.otification.utility.service.HTTPService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ActorFragment extends Fragment {

    private OishiApplication app;

    public static Fragment newInstance(Context context, int actor, int actorLength, float scale, boolean forCreateAlarm) {
        Bundle b = new Bundle();
        b.putInt("actor", actor);
        b.putInt("actorLength", actorLength);
        b.putFloat("scale", scale);
        b.putBoolean("forCreateAlarm", forCreateAlarm);
        return Fragment.instantiate(context, ActorFragment.class.getName(), b);
    }

//    private Context context;
//    private int pos;
//    private float scale;
//
//    public ActorFragment(Context context, int pos, float scale) {
//        this.context = context;
//        this.pos = pos;
//        this.scale = scale;
//    }

    private int[] res = {
            R.drawable.img_1,
            R.drawable.img_2,
            R.drawable.img_5,
            R.drawable.img_4,
            R.drawable.img_6,
            R.drawable.img_3,
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        app = (OishiApplication) getActivity().getApplication();

        if (container == null) {
            return null;
        }

        LinearLayout l = (LinearLayout) inflater.inflate(R.layout.otification_carousel_cell, container, false);

        final int actor = getArguments().getInt("actor");

        ImageView im = (ImageView) l.findViewById(R.id.im);
        im.setImageResource(res[actor]);
        im.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int p = actor - 1;
                if (p == -1) {
                    p = getArguments().getInt("actorLength") - 1;
                }

                final int finalP = p;
                final int a = Integer.parseInt((new PreferenceService(getContext())).getString("activitySelected"));
                if (getArguments().getBoolean("forCreateAlarm")) {
                    app.getHttpService().getPlaylistAlarm(new HTTPService.OnResponseCallback<JSONObject>() {
                        @Override
                        public void onResponse(boolean success, Throwable error, JSONObject data) {
                            if (!(data == null)) {
                                try {

                                    final ArrayList<PlayListAlarmModel> playListAlarmModels = new ArrayList<PlayListAlarmModel>();

                                    JSONArray group = data.getJSONObject("playlist").getJSONArray("group");
                                    for (int i = 0; i < group.length(); i++) {

                                        JSONObject g = group.getJSONObject(i);

                                        int activity = g.getInt("no") - 1;

                                        JSONArray list = g.getJSONArray("list");
                                        for (int j = 0; j < list.length(); j++) {
                                            JSONObject l = list.getJSONObject(j);

                                            int actor = l.getInt("actor") - 1;

                                            if (activity == a && actor == finalP) {
                                                final PlayListAlarmModel media = new PlayListAlarmModel(activity, actor, l.getString("name"), l.getString("noti_title"), l.getString("noti_message"), l.getString("video"), l.getString("audio_android"), l.getString("share_url"), l.getString("share_title"), l.getString("share_description"), l.getString("share_image"));

                                                if (app.getPlayListAlarmDB().checkHasToDownload(media)) {
                                                    Intent intent = new Intent(getContext(), LoadingActivity.class);
                                                    intent.putExtra("media", media);
                                                    intent.putExtra("shouldPreview", true);
                                                    startActivity(intent);
                                                } else {
                                                    Intent intent = new Intent(getContext(), PreviewVDOActivity.class);
                                                    intent.putExtra("activity", a);
                                                    intent.putExtra("actor", finalP);
                                                    startActivity(intent);
                                                }

                                            }


                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            } else {
                                Toast.makeText(getContext(), "เชื่อมต่ออินเตอร์เน็ตไม่ได้", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                } else {
                    Log.d("hwy", "1");

                }


            }
        });

        ActorLinearLayout root = (ActorLinearLayout) l.findViewById(R.id.root);
        float scale = this.getArguments().getFloat("scale");
        root.setScaleBoth(scale);

        return l;
    }
}
