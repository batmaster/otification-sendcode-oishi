package com.adapter.oishi.otification.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.DisplayMetrics;
import android.util.Log;

import com.adapter.oishi.R;
import com.adapter.oishi.otification.activity.EmptyDialog;
import com.adapter.oishi.otification.activity.fragment.MainActivity;
import com.adapter.oishi.otification.utility.PreferenceService;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class HomeActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otification_activity_home);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        int widthPixels = metrics.widthPixels;
        int heightPixels = metrics.heightPixels;

        float scaleFactor = metrics.density;

        float widthDpi = metrics.xdpi;
        float heightDpi = metrics.ydpi;

        float widthInches = widthPixels / widthDpi;
        float heightInches = heightPixels / heightDpi;

        double diagonalInches = Math.sqrt((widthInches * widthInches) + (heightInches * heightInches));

        Log.d("scsize", diagonalInches + "");

        if (diagonalInches >= 7) {
            Intent intent = new Intent(getApplicationContext(), EmptyDialog.class);
            intent.putExtra("text", 2);
            startActivityForResult(intent, 155);
        }
        else {
            new CountDownTimer(1500, 1000) {

                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {
                    if ((new PreferenceService(getApplicationContext())).getBoolean(PreferenceService.KEY_BOOLEAN_HAS_TUTORIAL) == false) {
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        intent.putExtra("index", 0);
                        startActivity(intent);

                        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                                .setDefaultFontPath("fonts/DB HelvethaicaMon X.ttf")
                                .setFontAttrId(R.attr.fontPath)
                                .build());

                        startActivity(new Intent(getApplicationContext(), TutorialActivity.class));

                        (new PreferenceService(getApplicationContext())).putBoolean(PreferenceService.KEY_BOOLEAN_HAS_TUTORIAL, true);
                    }
                    else {
                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                    }

                    finish();
                }
            }.start();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        boolean b = data.getBooleanExtra("close", false);
        if (b) {
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.runFinalization();
        Runtime.getRuntime().gc();
        System.gc();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(base));
    }
}
