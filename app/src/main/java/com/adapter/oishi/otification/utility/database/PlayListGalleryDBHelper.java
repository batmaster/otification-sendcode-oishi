package com.adapter.oishi.otification.utility.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.adapter.oishi.otification.model.PlayListGalleryModel;

import java.io.File;
import java.util.ArrayList;

public class PlayListGalleryDBHelper extends SQLiteOpenHelper {

    private Context context;

    public PlayListGalleryDBHelper(Context context) {
        super(context, "plgallery.db", null, 1);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE medias (id INTEGER PRIMARY KEY AUTOINCREMENT, type INTEGER, activity INTEGER, actor INTEGER, active INTEGER, name TEXT, video TEXT, galleryImage TEXT, url TEXT, title TEXT, description TEXT, shareImage TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS medias");
        onCreate(db);
    }

    public void addMedia(PlayListGalleryModel model) {
        removeMedia(model);

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        sqLiteDatabase.execSQL(String.format("INSERT INTO medias (type, activity, actor, active, name, video, galleryImage, url, title, description, shareImage) VALUES (%d, %d, %d, %d, '%s', '%s', '%s', '%s', '%s', '%s', '%s')",
                model.getType(), model.getActivity(), model.getActor(), model.getActive(), model.getName(), model.getVideo(),
                model.getGalleryImage(), model.getUrl(), model.getTitle(), model.getDescription(), model.getShareImage()));
        sqLiteDatabase.close();
    }

    private void removeMedia(PlayListGalleryModel model) {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        sqLiteDatabase.execSQL("DELETE FROM medias WHERE activity = " + model.getType() + " AND activity = " + model.getActivity() + " AND actor = " + model.getActor());
        sqLiteDatabase.close();
    }

    private ArrayList<PlayListGalleryModel> getApiModels() {
        ArrayList<PlayListGalleryModel> playListGalleryModels = new ArrayList<PlayListGalleryModel>();

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(String.format("SELECT type, activity, actor, active, name, video, galleryImage, url, title, description, shareImage FROM medias"), null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    playListGalleryModels.add(new PlayListGalleryModel(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2), cursor.getInt(3), cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getString(7), cursor.getString(8), cursor.getString(9), cursor.getString(10)));
                } while (cursor.moveToNext());
            }
        }

        cursor.close();
        sqLiteDatabase.close();

        return playListGalleryModels;
    }

    public PlayListGalleryModel getApiModel(int type, int activity, int actor) {
        ArrayList<PlayListGalleryModel> playListGalleryModels = getApiModels();
        for (int i = 0; i < playListGalleryModels.size(); i++) {
            if (activity == playListGalleryModels.get(i).getActivity() && actor == playListGalleryModels.get(i).getActor() && type == playListGalleryModels.get(i).getType()) {
                return playListGalleryModels.get(i);
            }
        }
        return null;
    }

    public boolean checkHasToDownload(PlayListGalleryModel model) {
        int type = model.getType();
        int activity = model.getActivity();
        int actor = model.getActor();

        PlayListGalleryModel d = getApiModel(type, activity, actor);

        if (d == null) {
            // download
            return true;
        }
        else if (!d.getVideoFileName().equals(model.getVideoFileName())) {
            removeFile(context, d.getVideoFileName());
            return true;
        }
        else if (!fileExistance(context, d.getVideoFileName())) {
            removeFile(context, d.getVideoFileName());
            return true;
        }
        return false;
    }

    public boolean fileExistance(Context context, String fname){
        File file = context.getFileStreamPath(fname);
        return file.exists();
    }

    public boolean removeFile(Context context, String fname){
        File file = context.getFileStreamPath(fname);
        return file.delete();
    }

}
