package com.adapter.oishi.otification.activity;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.adapter.oishi.Config;
import com.adapter.oishi.OishiApplication;
import com.adapter.oishi.R;
import com.adapter.oishi.otification.activity.fragment.MainActivity;
import com.adapter.oishi.otification.model.PlayListAlarmModel;
import com.adapter.oishi.otification.model.AlarmModel;
import com.adapter.oishi.otification.utility.FullScreenVideoView;
import com.adapter.oishi.sendcode.activities.custom.LandingActivity;

import java.util.ArrayList;

public class AlarmActivity extends Activity {

    private OishiApplication app;

    private ImageView imageViewClose;
    private FullScreenVideoView videoView;

    private RelativeLayout layoutSnooze;
    private ImageView imageViewSnooze;

    private boolean moveBack = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otification_activity_alarm);

        app = (OishiApplication) getApplication();
        if (Config.isPassed(getApplicationContext())) {
            startActivity(new Intent(getApplicationContext(), LogoActivity.class));
            finish();
        }

        final AlarmModel alarm = (AlarmModel) getIntent().getSerializableExtra("alarm");

        PlayListAlarmModel media = app.getPlayListAlarmDB().getApiModel(alarm.getActivity(), alarm.getActor());

        imageViewSnooze = (ImageView) findViewById(R.id.imageViewSnooze);
        imageViewSnooze.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                return false;
            }
        });

        layoutSnooze = (RelativeLayout) findViewById(R.id.layoutSnooze);
        layoutSnooze.post(new Runnable() {
            @Override
            public void run() {
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) imageViewSnooze.getLayoutParams();
                params.leftMargin = (layoutSnooze.getWidth() / 2) - (imageViewSnooze.getWidth() / 2);
            }
        });

        imageViewSnooze.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) imageViewSnooze.getLayoutParams();
                int w = layoutSnooze.getWidth();

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        break;
                    case MotionEvent.ACTION_UP:
                        if (moveBack) {
                            params.leftMargin = (layoutSnooze.getWidth() / 2) - (imageViewSnooze.getWidth() / 2);
                        }
                        break;
                    case MotionEvent.ACTION_MOVE:

                        int x_cord = (int) event.getRawX();


                        if (x_cord > w - imageViewSnooze.getWidth()/2 - imageViewSnooze.getWidth()/5) {
                            x_cord = w - imageViewSnooze.getWidth()/2 - imageViewSnooze.getWidth()/5;

                            moveBack = false;

                            // close
                            app.sendButtonStat("bnt_close_alert");
                            videoView.stopPlayback();

                            NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                            mNotificationManager.cancel(31909);

                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.putExtra("fromAlarmActivity", true);
                            startActivity(intent);
                            finish();

                            ArrayList<AlarmModel> alarmModels = app.getAlarmDB().getAlarms();
                            for (int i = 0; i < alarmModels.size(); i++) {
                                if (alarmModels.get(i).getId() == alarm.getId()) {
                                    AlarmModel a = alarmModels.get(i);
                                    if (a.getSu() == 0 && a.getMo() == 0 && a.getTu() == 0 && a.getWe() == 0 && a.getTh() == 0 && a.getFr() == 0 && a.getSa() == 0) {
                                        app.getAlarmDB().setState(alarm, false);
                                        break;
                                    }
                                }
                            }
                        }
                        else if (x_cord < (imageViewSnooze.getWidth() / 2)) {
                            x_cord = (imageViewSnooze.getWidth() / 2);

                            moveBack = false;

                            // snooze
                            app.sendButtonStat("bnt_close_snooze");

                            ArrayList<AlarmModel> a = app.getAlarmDB().getAlarms();
                            boolean shouldAdd = false;
                            for (int i = 0; i < a.size(); i++) {
                                Log.d("shouldAdd snooze", a.get(i).getId() + " " + alarm.getId());
                                if (a.get(i).getId() == alarm.getId()) {
                                    shouldAdd = true;
                                    break;
                                }
                            }

                            if (shouldAdd) {
                                app.getAlarmDB().addSnoozeAlarm(alarm);
                            }

                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.putExtra("fromAlarmActivity", true);
                            startActivity(intent);
                            finish();
                        }

                        params.leftMargin = x_cord - (imageViewSnooze.getWidth() / 2);

                        imageViewSnooze.setLayoutParams(params);
                        break;
                    default:
                        break;
                }
                return true;
            }
        });


        videoView = (FullScreenVideoView) findViewById(R.id.videoView);
//        MediaController mediaController = new MediaController(this);
//        mediaController.setAnchorView(videoView);
        Uri video = null;
        if (media.getActivity() == -1) {
            video = Uri.parse(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + "/oishi/" + media.getVideoFileName());
        }
        else {
            video = Uri.parse(getApplicationContext().getFileStreamPath(media.getVideoFileName()).getAbsolutePath());
        }
//        videoView.setMediaController(mediaController);
        videoView.setVideoURI(video);
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });

        videoView.start();
    }

    int pauseTime = 0;
    @Override
    protected void onPause() {
        super.onPause();
        pauseTime = videoView.getCurrentPosition();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (pauseTime != 0) {
            videoView.seekTo(pauseTime);
            videoView.start();
        }
    }
}
