package com.adapter.oishi.otification.activity.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.adapter.oishi.OishiApplication;
import com.adapter.oishi.otification.model.PlayListAlarmModel;
import com.adapter.oishi.otification.model.ShareModel;
import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.chauthai.swipereveallayout.ViewBinderHelper;

import java.util.ArrayList;

import com.adapter.oishi.otification.model.AlarmModel;
import com.adapter.oishi.R;

/**
 * Created by batmaster on 8/19/16 AD.
 */
public class ListViewAdapter extends BaseAdapter {

    private final ViewBinderHelper binderHelper;

    public static final int MODE_SELF = 0;
    public static final int MODE_FRIEND = 1;

    private static final int[] resChar = {
            R.drawable.img_char0,
            R.drawable.img_char1,
            R.drawable.img_char4,
            R.drawable.img_char3,
            R.drawable.img_char5,
            R.drawable.img_char2
    };

    private OishiApplication app;

    private Activity activity;
    private ArrayList<AlarmModel> alarms;
    private ArrayList<ShareModel> shares;
    private int mode;



    public ListViewAdapter(Activity activity, ArrayList<AlarmModel> alarms, ArrayList<ShareModel> shares, int mode) {
        app = (OishiApplication) activity.getApplication();

        this.activity = activity;
        this.alarms = alarms;
        this.shares = shares;
        this.mode = mode;

        binderHelper = new ViewBinderHelper();
        binderHelper.setOpenOnlyOne(true);
    }

    @Override
    public int getCount() {
        if (mode == MODE_SELF) {
            return alarms.size();
        }
        else {
            return shares.size();
        }
    }

    @Override
    public Object getItem(int position) {
        if (mode == MODE_SELF) {
            return alarms.get(position);
        } else {
            return shares.get(position);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setAlarms(ArrayList<AlarmModel> alarms) {
        this.alarms = alarms;
        notifyDataSetChanged();
    }

    public void setShares(ArrayList<ShareModel> shares) {
        this.shares = shares;
        notifyDataSetChanged();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater mInflater = (LayoutInflater) activity.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View row = mInflater.inflate(R.layout.otification_list_row, parent, false);


        final SwipeRevealLayout swipeLayout = (SwipeRevealLayout) row.findViewById(R.id.swipeLayout);
        binderHelper.bind(swipeLayout, swipeLayout.toString());
//        swipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
//            @Override
//            public void onStartOpen(SwipeLayout layout) {
//                Log.d("swipss", "onStartOpen");
//                if (dragged != null) {
//                    dragged.close();
//                }
//                dragged = layout;
//            }
//
//            @Override
//            public void onOpen(SwipeLayout layout) {
//                Log.d("swipss", "onOpen");
//            }
//
//            @Override
//            public void onStartClose(SwipeLayout layout) {
//                Log.d("swipss", "onStartClose");
//            }
//
//            @Override
//            public void onClose(SwipeLayout layout) {
//                Log.d("swipss", "onClose");
//                dragged = null;
//            }
//
//            @Override
//            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {
//
//            }
//
//            @Override
//            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {
//
//            }
//        });

        final LinearLayout linearLayout = (LinearLayout) row.findViewById(R.id.linearLayout);
        if (position % 2 == 1) {
            linearLayout.setBackgroundResource(R.drawable.bg_list_even);
        } else {
            linearLayout.setBackgroundResource(R.drawable.bg_list_odd);
        }

        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (swipeLayout.isClosed()) {
                    Bundle args = new Bundle();
                    if (mode == MODE_SELF) {
                        args.putSerializable("alarm", alarms.get(position));
                        ((MainActivity) activity).setFragment(4, args);
                    }

                }
            }
        });
        linearLayout.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        ImageView imageViewDelete = (ImageView) row.findViewById(R.id.imageViewDelete);
        imageViewDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mode == MODE_SELF) {
                    app.getAlarmDB().deleteAlarm(alarms.get(position));

                    alarms.remove(position);
                    notifyDataSetChanged();
                }
                else {
                    app.getShareDB().deleteAlarm(shares.get(position));

                    shares.remove(position);
                    notifyDataSetChanged();
                }
            }
        });


        final ImageView imageViewChar = (ImageView) row.findViewById(R.id.imageViewChar);
//        imageViewChar.post(new Runnable() {
//            @Override
//            public void run() {
//                int w = imageViewChar.getWidth();
//                int h = imageViewChar.getHeight();
//
//                if (w <= h) {
//                    imageViewChar.setLayoutParams(new ViewGroup.LayoutParams(w, w));
//                }
//                else {
//                    imageViewChar.setLayoutParams(new ViewGroup.LayoutParams(h, h));
//                }
//            }
//        });
        TextView textViewText = (TextView) row.findViewById(R.id.textViewText);
        final TextView textViewTime = (TextView) row.findViewById(R.id.textViewTime);
        TextView textViewDay = (TextView) row.findViewById(R.id.textViewDay);
        ToggleButton toggleButton = (ToggleButton) row.findViewById(R.id.toggleButton);

        if (mode == MODE_SELF) {
            if (alarms.get(position).getActivity() == -1) {
                PlayListAlarmModel media = app.getPlayListAlarmDB().getApiModel(alarms.get(position).getActivity(), alarms.get(position).getActor());
                String fileName = media.getVideoFileName().replace(".mp4", "");

                if ((ActivityCompat.checkSelfPermission(activity.getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                        && (ActivityCompat.checkSelfPermission(activity.getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) {
                    imageViewChar.setImageBitmap(readBitmap(fileName));
                }
            }
            else {
                imageViewChar.setImageResource(resChar[alarms.get(position).getActor()]);
            }

            textViewTime.setText(alarms.get(position).getTime());
            textViewTime.setTextColor(alarms.get(position).getState() == 1 ? Color.RED : Color.BLACK);

            textViewDay.setText(alarms.get(position).getDays());

            if (alarms.get(position).getActivity() == -1) {
                PlayListAlarmModel media = app.getPlayListAlarmDB().getApiModel(alarms.get(position).getActivity(), alarms.get(position).getActor());
                textViewText.setText(media.getNoti_title());
            }
            else {
                textViewText.setText(alarms.get(position).getActivityTH());
            }

            toggleButton.setChecked(alarms.get(position).getState() == 1);

            toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    textViewTime.setTextColor(isChecked ? Color.RED : Color.BLACK);
                    app.getAlarmDB().setState(alarms.get(position), isChecked);
                }
            });
        }
        else {
            textViewTime.setText(shares.get(position).getTime());

            textViewDay.setVisibility(View.INVISIBLE);

            toggleButton.setVisibility(View.INVISIBLE);
            textViewText.setText(shares.get(position).getActivityTH());
            imageViewChar.setImageResource(resChar[shares.get(position).getActor()]);
        }



        return row;
    }

    public Bitmap readBitmap(String filename) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap b = BitmapFactory.decodeFile(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath() + "/oishi/" + filename + ".png", options);
        return b;
    }

}
