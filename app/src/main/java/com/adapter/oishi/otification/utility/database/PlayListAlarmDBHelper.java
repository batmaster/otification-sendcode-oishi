package com.adapter.oishi.otification.utility.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;
import java.util.ArrayList;

import com.adapter.oishi.otification.model.PlayListAlarmModel;

public class PlayListAlarmDBHelper extends SQLiteOpenHelper {

    private Context context;

    public PlayListAlarmDBHelper(Context context) {
        super(context, "plalarm.db", null, 1);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE medias (id INTEGER PRIMARY KEY AUTOINCREMENT, activity INTEGER, actor INTEGER, name TEXT, title TEXT, message TEXT, video TEXT, audio TEXT, sUrl TEXT, sTitle TEXT, sDescription TEXT, sImage TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS medias");
        onCreate(db);
    }

    public void addMedia(PlayListAlarmModel model) {
        removeMedia(model);

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        sqLiteDatabase.execSQL(String.format("INSERT INTO medias (activity, actor, name, title, message, video, audio, sUrl, sTitle, sDescription, sImage) VALUES (%d, %d, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')",
                model.getActivity(), model.getActor(), model.getName(), model.getNoti_title(), model.getNoti_message(),
                model.getVideo(), model.getAudio(), model.getsUrl(), model.getsTitle(), model.getsDescription(), model.getsImage()));
        sqLiteDatabase.close();
    }

    private void removeMedia(PlayListAlarmModel model) {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        sqLiteDatabase.execSQL("DELETE FROM medias WHERE activity = " + model.getActivity() + " AND actor = " + model.getActor());
        sqLiteDatabase.close();
    }

    private ArrayList<PlayListAlarmModel> getApiModels() {
        ArrayList<PlayListAlarmModel> playListAlarmModels = new ArrayList<PlayListAlarmModel>();

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(String.format("SELECT activity, actor, name, title, message, video, audio, sUrl, sTitle, sDescription, sImage FROM medias"), null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    playListAlarmModels.add(new PlayListAlarmModel(cursor.getInt(0), cursor.getInt(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getString(7), cursor.getString(8), cursor.getString(9), cursor.getString(10)));
                } while (cursor.moveToNext());
            }
        }

        cursor.close();
        sqLiteDatabase.close();

        return playListAlarmModels;
    }

    public PlayListAlarmModel getApiModel(int activity, int actor) {
        ArrayList<PlayListAlarmModel> playListAlarmModels = getApiModels();
        for (int i = 0; i < playListAlarmModels.size(); i++) {
            if (activity == playListAlarmModels.get(i).getActivity() && actor == playListAlarmModels.get(i).getActor()) {
                return playListAlarmModels.get(i);
            }
        }
        return null;
    }

    public boolean checkHasToDownload(PlayListAlarmModel model) {
        int activity = model.getActivity();
        int actor = model.getActor();

        PlayListAlarmModel d = getApiModel(activity, actor);


        if (d == null) {
            // download
            return true;
        }
        else if (!d.getVideoFileName().equals(model.getVideoFileName()) || !d.getAudioFileName().equals(model.getAudioFileName())) {
            removeFile(context, d.getVideoFileName());
            removeFile(context, d.getAudioFileName());
            return true;
        }
        else if (!fileExistance(context, d.getVideoFileName()) || !fileExistance(context, d.getAudioFileName())) {
            removeFile(context, d.getVideoFileName());
            removeFile(context, d.getAudioFileName());
            return true;
        }
        return false;
    }

    public boolean fileExistance(Context context, String fname){
        File file = context.getFileStreamPath(fname);
        return file.exists();
    }

    public boolean removeFile(Context context, String fname){
        File file = context.getFileStreamPath(fname);
        return file.delete();
    }

}
