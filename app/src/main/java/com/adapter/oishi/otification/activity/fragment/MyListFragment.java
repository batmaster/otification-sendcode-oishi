package com.adapter.oishi.otification.activity.fragment;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.ArrayList;

import com.adapter.oishi.OishiApplication;
import com.adapter.oishi.otification.model.AlarmModel;
import com.adapter.oishi.R;
import com.adapter.oishi.otification.utility.database.AlarmDBHelper;

public class MyListFragment extends Fragment {

    private OishiApplication app;

    private ImageView imageViewCreate;
    private ListView listView;
    private ImageView imageViewFriend;

    private ListViewAdapter adapter;

    public MyListFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.otification_fragment_my_list, container, false);

        app = (OishiApplication) getActivity().getApplication();
        app.sendPageStat("page_list");

        if ((ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
                || (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED)
                ||(ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                || (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {

            requestPermissions(new String[] {Manifest.permission.RECORD_AUDIO, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 111);
        }

        imageViewCreate = (ImageView) rootView.findViewById(R.id.imageViewCreate);
        imageViewCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                app.sendButtonStat("bnt_list_crebnt_list_createate");

                ((MainActivity) getActivity()).setFragment(0, null);
            }
        });

        listView = (ListView) rootView.findViewById(R.id.listView);

        ArrayList<AlarmModel> alarms = app.getAlarmDB().getAlarms();
        adapter = new ListViewAdapter(getActivity(), alarms, null, ListViewAdapter.MODE_SELF);
        listView.setAdapter(adapter);
//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Toast.makeText(getContext(), "item " + position, Toast.LENGTH_SHORT).show();
//            }
//        });

//        SwipeMenuCreator creator = new SwipeMenuCreator() {
//            @Override
//            public void create(SwipeMenu menu) {
//                SwipeMenuItem deleteItem = new SwipeMenuItem(getContext());
//
//                deleteItem.setWidth(256);
////                deleteItem.setBackground(R.drawable.btn_delete);
//                deleteItem.setIcon(R.drawable.ic_delete);
//
//                menu.addMenuItem(deleteItem);
//            }
//        };
//        listView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
//            @Override
//            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
//                Toast.makeText(getContext(), "menu " + index, Toast.LENGTH_SHORT).show();
//                return false;
//            }
//        });
//        listView.setMenuCreator(creator);
//        listView.setSwipeDirection(SwipeMenuListView.DIRECTION_RIGHT);

        imageViewFriend = (ImageView) rootView.findViewById(R.id.imageViewFriend);
        imageViewFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).setFragment(7, null);
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        ArrayList<AlarmModel> alarms = new AlarmDBHelper(getContext()).getAlarms();
        adapter.setAlarms(alarms);
    }
}
