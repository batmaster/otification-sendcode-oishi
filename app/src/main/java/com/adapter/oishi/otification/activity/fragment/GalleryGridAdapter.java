package com.adapter.oishi.otification.activity.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import com.adapter.oishi.OishiApplication;
import com.adapter.oishi.R;
import com.adapter.oishi.otification.activity.LoadingActivity;
import com.adapter.oishi.otification.activity.PreviewVDOActivity;
import com.adapter.oishi.otification.activity.ThankYouDialog;
import com.adapter.oishi.otification.model.PlayListGalleryModel;
import com.adapter.oishi.otification.utility.service.HTTPService;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

/**
 * Created by batmaster on 8/19/16 AD.
 */
public class GalleryGridAdapter {

    private OishiApplication app;
    private Context context;
    private Activity activity;
    private Fragment fragment;
    private LinearLayout verticalLinearLayout;
    private ArrayList<PlayListGalleryModel> gallery;

    private CallbackManager callbackManager;

    public GalleryGridAdapter(final Context context, Activity activity, Fragment fragment, LinearLayout verticalLinearLayout, ArrayList<PlayListGalleryModel> gallery) {
        app = (OishiApplication) context.getApplicationContext();
        this.context = context;
        this.activity = activity;
        this.fragment = fragment;
        this.verticalLinearLayout = verticalLinearLayout;
        this.gallery = gallery;
    }

    public void load() {
        verticalLinearLayout.removeAllViews();

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        LinearLayout horizontalLinearLayout = null;
        for (int i = 0; i < gallery.size(); i++) {
            if (i % 2 == 0) {
                horizontalLinearLayout = new LinearLayout(context);
                horizontalLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
                horizontalLinearLayout.setWeightSum(100f);
                horizontalLinearLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                verticalLinearLayout.addView(horizontalLinearLayout);
            }

            View cell = inflater.inflate(R.layout.otification_grid_cell, null);

            LinearLayout parent = (LinearLayout) cell.findViewById(R.id.parent);
            parent.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 50f));

            final int finalI = i;
            final ImageView imageViewBackground = (ImageView) cell.findViewById(R.id.imageViewBackground);
            imageViewBackground.post(new Runnable() {
                @Override
                public void run() {
                    int w = imageViewBackground.getWidth();
                    int h = imageViewBackground.getHeight();

                    Picasso.with(context).load(gallery.get(finalI).getGalleryImage()).noPlaceholder().resize(w, h).into(imageViewBackground, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {

                        }
                    });
                }
            });


            imageViewBackground.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PlayListGalleryModel media = gallery.get(finalI);

                    String vid = media.getVideo().split("/")[media.getVideo().split("/").length - 1].replace(".mp4", "");
                    app.sendButtonStat("preview_" + vid);

                    if (app.getPlayListGalleryDB().checkHasToDownload(media)) {
                        Intent intent = new Intent(context, LoadingActivity.class);
                        intent.putExtra("media", media);
                        intent.putExtra("shouldPreview", true);
                        intent.putExtra("isFromAlarm", false);
                        intent.putExtra("isFromGallery", true);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    } else {
                        Intent intent = new Intent(context, PreviewVDOActivity.class);
                        intent.putExtra("type", media.getType());
                        intent.putExtra("activity", media.getActivity());
                        intent.putExtra("actor", media.getActor());
                        intent.putExtra("isFromGallery", true);
                        intent.putExtra("media", media);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    }
                }
            });

            TextView textViewName = (TextView) cell.findViewById(R.id.textViewName);
            textViewName.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/DB HelvethaicaMon X Bd.ttf"));
            textViewName.setText(gallery.get(i).getName());

            ImageView imageViewFacebook = (ImageView) cell.findViewById(R.id.imageViewFacebook);
            imageViewFacebook.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    app.sendButtonStat("bnt_gall_share-fb");

                    callbackManager = ((MainActivity) activity).setFacebookResponseWaitingFragmentToShare(fragment);

                    if (AccessToken.getCurrentAccessToken() == null) {
                        String[] permissions = {"public_profile", "email"};

                        LoginManager.getInstance().logInWithReadPermissions(activity, Arrays.asList(permissions));
                    }
                    else {
                        share(gallery.get(finalI));
                    }


                }
            });

            ImageView imageViewLine = (ImageView) cell.findViewById(R.id.imageViewLine);
            imageViewLine.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    app.sendButtonStat("bnt_gall_share-line");

                    activity.startActivity(new Intent(activity.getApplicationContext(), ThankYouDialog.class));

                    try {
                        String shareBody = "line://msg/text/" + gallery.get(finalI).getUrl();
                        activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(shareBody)));
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });

//
//            RelativeLayout idealParentLayout = (RelativeLayout) cell.findViewById(R.id.idealParentLayout);
//            RelativeLayout.LayoutParams iparams = (RelativeLayout.LayoutParams) idealParentLayout.getLayoutParams();
//
//            LinearLayout buggyParentLayout = (LinearLayout) cell.findViewById(R.id.buggyParentLayout);
//            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) buggyParentLayout.getLayoutParams();
//            params.height = iparams.height;
//            buggyParentLayout.setLayoutParams(params);



            horizontalLinearLayout.addView(cell);
        }
    }

    public void share(final PlayListGalleryModel model) {

        final String url = model.getUrl();
        String title = model.getTitle();
        String description = model.getDescription();
        String image = model.getShareImage();

        ShareLinkContent content = new ShareLinkContent.Builder()
                .setImageUrl(Uri.parse(image))
                .setContentUrl(Uri.parse(url))
                .setContentTitle(title)
                .setContentDescription(description)
                .build();
        Log.d("facee", "dialog ok");

        ShareDialog dialog = new ShareDialog(activity);
        dialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                Log.d("facee", "onSuccess");

                Calendar c = Calendar.getInstance();
                app.getShareDB().addAlarm(model.getType(), model.getActivity(), model.getActor(), c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE));

                int videoId = Integer.parseInt(url.split("/")[url.split("/").length - 1].replace(".html", ""));
                app.getHttpService().saveShareToWall(videoId, result.getPostId(), new HTTPService.OnResponseCallback<JSONObject>() {
                    @Override
                    public void onResponse(boolean success, Throwable error, JSONObject data) {

                    }
                });

                activity.startActivity(new Intent(activity.getApplicationContext(), ThankYouDialog.class));

            }

            @Override
            public void onCancel() {
                Log.d("facee", "onCancel()");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d("facee", "onError");
                Log.d("share 2", "" + error.getMessage());
            }
        });
        if (dialog.canShow(ShareLinkContent.class)) {
            dialog.show(content);
        } else {
            Log.d("facee", "you cannot share :(");
        }

    }
}
