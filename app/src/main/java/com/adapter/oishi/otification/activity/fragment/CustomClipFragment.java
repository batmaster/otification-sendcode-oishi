package com.adapter.oishi.otification.activity.fragment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.adapter.oishi.OishiApplication;
import com.adapter.oishi.R;
import com.adapter.oishi.otification.activity.CameraActivity;
import com.adapter.oishi.otification.activity.EmptyDialog;
import com.adapter.oishi.otification.activity.PreviewVDOActivity;
import com.adapter.oishi.otification.activity.ThankYouDialog;
import com.adapter.oishi.otification.model.AlarmModel;
import com.adapter.oishi.otification.model.PlayListAlarmModel;
import com.adapter.oishi.otification.utility.service.HTTPService;

import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CustomClipFragment extends Fragment {

    private OishiApplication app;

    private ImageView imageViewRecordAudio;
    private ImageView imageViewPlay;
    private ImageView imageViewVideo;
    private ImageView imageViewRecordVideo;
    private ImageView imageViewCancel;
    private ImageView imageViewSave;

    private ImageView imageViewDot;
    private ImageView imageViewDotRed;
    private AnimationSet animSet;

    private MediaRecorder audioRecorder;
    private MediaPlayer m;
    private boolean isRecording = false;
    private int forCheckAudioRecorded = 0;
    private boolean forCheckVideoRecorded = false;
    private String filename;

    private Thread t;

    public CustomClipFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.otification_fragment_custom_clip, container, false);

        app = (OishiApplication) getActivity().getApplication();

        Bundle args = getArguments();
        final AlarmModel alarm = (AlarmModel) args.getSerializable("alarm");

        final int activity = args.getInt("activity");
        final int hour = args.getInt("hour");
        final int minute = args.getInt("minute");

        filename = (new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss")).format(new Date());

        animSet = new AnimationSet(true);
        Animation fadeOutLoopAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.fade_out_loop);
        animSet.addAnimation(fadeOutLoopAnimation);

        imageViewDot = (ImageView) rootView.findViewById(R.id.imageViewDot);
        imageViewDot.setImageResource(R.drawable.img_record_dot_gray);
        imageViewDotRed = (ImageView) rootView.findViewById(R.id.imageViewDotRed);


        if ((ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED)
                && (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                && (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) {

            prepareReccordAudio();
        }
        else {
            requestPermissions(new String[] {Manifest.permission.RECORD_AUDIO, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 125);
        }

        imageViewRecordAudio = (ImageView) rootView.findViewById(R.id.imageViewRecordAudio);
        imageViewRecordAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if ((ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED)
                        && (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                        && (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) {

                    reccordAudio();
                }
                else {
                    requestPermissions(new String[] {Manifest.permission.RECORD_AUDIO, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 124);
                }

            }
        });

        imageViewPlay = (ImageView) rootView.findViewById(R.id.imageViewPlay);
        imageViewPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                app.sendButtonStat("bnt_sound_play");
                if (forCheckAudioRecorded >= 2) {

                    try {
                        if (m == null) {
                            m = new MediaPlayer();
                            m.setDataSource(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + "/oishi/" + filename + ".mp3");
                            m.prepare();
                        }

                        if (!m.isPlaying()) {
                            m.start();
                        }
                        else {
                            m.stop();
                            m = null;
                            imageViewPlay.callOnClick();
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        imageViewVideo = (ImageView) rootView.findViewById(R.id.imageViewVideo);
        imageViewVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (forCheckVideoRecorded) {
                    Intent intent = new Intent(getContext(), PreviewVDOActivity.class);
                    intent.putExtra("previewCustom_filename", filename);
                    startActivity(intent);
                }

            }
        });

        imageViewRecordVideo = (ImageView) rootView.findViewById(R.id.imageViewRecordVideo);
        imageViewRecordVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                app.sendButtonStat("bnt_camera");

                if ((ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED)
                        && (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED)
                        && (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                        && (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) {

                    reccordVideo();
                }
                else {
                    requestPermissions(new String[] {Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 123);
                }

            }
        });

        imageViewCancel = (ImageView) rootView.findViewById(R.id.imageViewCancel);
        imageViewCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                app.sendButtonStat("bnt_custom_edit");

                ((MainActivity) getActivity()).setFragment(0, null);
            }
        });

        imageViewSave = (ImageView) rootView.findViewById(R.id.imageViewSave);
        imageViewSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                app.sendButtonStat("bnt_custom_save");
                if (forCheckAudioRecorded >= 2 && forCheckVideoRecorded) {
                    // save alarm
                    int actor = app.getAlarmDB().addAlarmCustom(hour, minute);

                    app.getHttpService().saveGame(true, hour, minute, -1, activity + 1, new HTTPService.OnResponseCallback<JSONObject>() {
                        @Override
                        public void onResponse(boolean success, Throwable error, JSONObject data) {

                        }
                    });

                    // save media
                    app.getPlayListAlarmDB().addMedia(new PlayListAlarmModel(-1, actor, AlarmModel.activity_th[activity], AlarmModel.activity_th[activity], AlarmModel.activity_message[activity], filename + ".mp4", filename + ".mp3", "sUrl", "sTitle", "sDescription", "sImage"));

                    startActivity(new Intent(getActivity().getApplicationContext(), ThankYouDialog.class));

                    ((MainActivity) getActivity()).setFragment(1, null);
                } else {
                    Intent intent = new Intent(getActivity().getApplicationContext(), EmptyDialog.class);
                    intent.putExtra("text", 0);
                    startActivity(intent);
                }
            }
        });

        return rootView;
    }

    private void stopRecord() {
        audioRecorder.stop();
        audioRecorder.reset();
        audioRecorder.release();
        audioRecorder = null;
        t.interrupt();
        m = null;

        animSet.cancel();
        imageViewDotRed.clearAnimation();
        imageViewDotRed.setAnimation(null);
        imageViewDotRed.setVisibility(View.INVISIBLE);

        imageViewDot.setImageResource(R.drawable.img_record_dot_green);

        isRecording = false;
        forCheckAudioRecorded++;
    }

    public void setResultRecordVideo(boolean result) {
        Log.d("recVideo", String.valueOf(result));

        forCheckVideoRecorded = result;
        if (result) {
            Bitmap b = readBitmap(filename);
            imageViewVideo.setImageBitmap(b);
        }
    }

    public Bitmap readBitmap(String filename) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap b = BitmapFactory.decodeFile(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath() + "/oishi/" + filename + ".png", options);
        return b;
    }

    @Override
    public void onPause() {
        super.onPause();
        if (isRecording) {
            stopRecord();
            isRecording = false;
        }
    }

    private void reccordAudio() {

        app.sendButtonStat("bnt_sound_rec");

//                if (forCheckAudioRecorded < 2) {

        if (m != null && m.isPlaying()) {
            m.stop();
            m = null;
        }

        if (isRecording) {
            stopRecord();
        } else {
            try {
                if (audioRecorder == null) {
                    audioRecorder = new MediaRecorder();
                    audioRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                    audioRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
                    audioRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
                    audioRecorder.setMaxDuration(5200);
                    audioRecorder.setOutputFile(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + "/oishi/" + filename + ".mp3");
                }

                imageViewDot.setImageResource(R.drawable.img_record_dot_yellow);
                imageViewDotRed.setVisibility(View.VISIBLE);

                imageViewDotRed.startAnimation(animSet);

                audioRecorder.prepare();
                audioRecorder.start();
                isRecording = true;
                forCheckAudioRecorded++;

                t = new Thread() {
                    @Override
                    public void run() {
                        try {

                            int i = 0;
                            while (!isInterrupted() && i < 5) {

                                Thread.sleep(1000);
                                i++;

                                final int finalI = i;
                                try {
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Log.d("sound thread", finalI + "");
                                            if (finalI == 5) {
                                                if (isRecording) {
                                                    stopRecord();
                                                }
                                            }
                                        }
                                    });
                                } catch (NullPointerException e) {
                                    e.printStackTrace();
                                }
                            }

                        } catch (InterruptedException e) {

                        }
                    }
                };

                t.start();

            } catch (IllegalStateException e) {
                isRecording = false;
                forCheckAudioRecorded--;

                e.printStackTrace();
            } catch (IOException e) {
                isRecording = false;
                forCheckAudioRecorded--;

                e.printStackTrace();
            }
//                    }
        }
    }

    private void reccordVideo() {
        Intent intent = new Intent(getActivity().getApplicationContext(), CameraActivity.class);
        intent.putExtra("filename", filename);
        getActivity().startActivityForResult(intent, 1908);
    }

    private void prepareReccordAudio() {

        audioRecorder = new MediaRecorder();
        audioRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        audioRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        audioRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        audioRecorder.setMaxDuration(5200);
        audioRecorder.setOutputFile(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + "/oishi/" + filename + ".mp3");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean pass = true;
        for (int i = 0; i < grantResults.length; i++) {
            if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                pass = false;
            }
        }

        switch (requestCode) {
            case 123 :
                if (pass) {

                    reccordVideo();
                }
                else {
                    Toast.makeText(getContext(), "กรุณาอนุมัติ Permission ใน Settings", Toast.LENGTH_SHORT).show();
                }
                break;
            case 124:
                if (pass) {

                    reccordAudio();
                }
                else {
                    Toast.makeText(getContext(), "กรุณาอนุมัติ Permission ใน Settings", Toast.LENGTH_SHORT).show();
                }
                break;
            case 125:
                if (pass) {

                    prepareReccordAudio();
                }
                else {
                    Toast.makeText(getContext(), "กรุณาอนุมัติ Permission ใน Settings", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}
