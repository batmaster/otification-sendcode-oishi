package com.adapter.oishi.otification.activity;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.adapter.oishi.R;

/**
 * Created by batmaster on 8/25/16 AD.
 */
public class TutorialPagerAdapter extends PagerAdapter {

    private Context context;

    private int[] res = {
            R.drawable.bg_tutorial1,
            R.drawable.bg_tutorial3,
            R.drawable.bg_tutorial2,
            R.drawable.bg_tutorial5,
            R.drawable.bg_tutorial4
    };

    public TutorialPagerAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return res.length;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.otification_fragment_tutorial, container, false);

        final ImageView imageView = (ImageView) rootView.findViewById(R.id.imageView);
        imageView.setImageResource(res[position]);

        container.addView(rootView);
        return rootView;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
