package com.adapter.oishi.otification.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.adapter.oishi.R;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class EmptyDialog extends Activity {

    private ImageView imageViewClose;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otification_activity_empty_dialog);

        int text = getIntent().getIntExtra("text", -1);

        String[] s = {
                "กรุณาอัดเสียง และสร้าง\n\n\n\nวิดีโอ",
                "สามารถสร้างการตั้งปลุก\n\n\n\nได้สูงสุด 8 ครั้ง",
                "เครื่องนี้ไม่รองรับ",
                "ยังไม่ถึงเวลาเล่น",
                "หมดเวลากิจกรรม"
        };

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/DB HelvethaicaMon X.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());

        imageViewClose = (ImageView) findViewById(R.id.imageViewClose);
        imageViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("close", true);
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });

        textView = (TextView) findViewById(R.id.textView);
        textView.setText(s[text]);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(base));
    }
}
