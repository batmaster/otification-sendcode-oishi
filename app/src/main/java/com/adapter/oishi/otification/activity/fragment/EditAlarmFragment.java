package com.adapter.oishi.otification.activity.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ToggleButton;

import com.adapter.oishi.OishiApplication;
import com.adapter.oishi.otification.model.AlarmModel;
import com.adapter.oishi.R;

public class EditAlarmFragment extends Fragment {

    private OishiApplication app;

    public EditAlarmFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.otification_fragment_edit_alarm, container, false);

        app = (OishiApplication) getActivity().getApplication();

        Bundle args = getArguments();
        final AlarmModel alarm = (AlarmModel) args.getSerializable("alarm");

        final ToggleButton toggleButtonSu = (ToggleButton) rootView.findViewById(R.id.toggleButtonSu);
        toggleButtonSu.setChecked(alarm.getSu() == 1);

        final ToggleButton toggleButtonMo = (ToggleButton) rootView.findViewById(R.id.toggleButtonMo);
        toggleButtonMo.setChecked(alarm.getMo() == 1);

        final ToggleButton toggleButtonTu = (ToggleButton) rootView.findViewById(R.id.toggleButtonTu);
        toggleButtonTu.setChecked(alarm.getTu() == 1);

        final ToggleButton toggleButtonWe = (ToggleButton) rootView.findViewById(R.id.toggleButtonWe);
        toggleButtonWe.setChecked(alarm.getWe() == 1);

        final ToggleButton toggleButtonTh = (ToggleButton) rootView.findViewById(R.id.toggleButtonTh);
        toggleButtonTh.setChecked(alarm.getTh() == 1);

        final ToggleButton toggleButtonFr = (ToggleButton) rootView.findViewById(R.id.toggleButtonFr);
        toggleButtonFr.setChecked(alarm.getFr() == 1);

        final ToggleButton toggleButtonSa = (ToggleButton) rootView.findViewById(R.id.toggleButtonSa);
        toggleButtonSa.setChecked(alarm.getSa() == 1);

        final ToggleButton toggleButtonSound = (ToggleButton) rootView.findViewById(R.id.toggleButtonSound);
        toggleButtonSound.setChecked(alarm.getSound() == 1);

        final ToggleButton toggleButtonVibration = (ToggleButton) rootView.findViewById(R.id.toggleButtonVibration);
        toggleButtonVibration.setChecked(alarm.getVibration() == 1);

//        final ToggleButton toggleButtonSnooze = (ToggleButton) rootView.findViewById(R.id.toggleButtonSnooze);
//        toggleButtonSnooze.setChecked(alarm.getSnooze() == 1);

        ImageView imageViewSave = (ImageView) rootView.findViewById(R.id.imageViewSave);
        imageViewSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean[] days = {
                        toggleButtonSu.isChecked(),
                        toggleButtonMo.isChecked(),
                        toggleButtonTu.isChecked(),
                        toggleButtonWe.isChecked(),
                        toggleButtonTh.isChecked(),
                        toggleButtonFr.isChecked(),
                        toggleButtonSa.isChecked(),
                };
                app.getAlarmDB().setDay(alarm, days);

                app.getAlarmDB().setSound(alarm, toggleButtonSound.isChecked());
                app.getAlarmDB().setVibration(alarm, toggleButtonVibration.isChecked());
//                app.getAlarmDB().setSnooze(alarm, toggleButtonSnooze.isChecked());
                ((MainActivity) getActivity()).setFragment(1, null);
            }
        });

        ImageView imageViewCancel = (ImageView) rootView.findViewById(R.id.imageViewCancel);
        imageViewCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).setFragment(1, null);
            }
        });

        return rootView;
    }
}
