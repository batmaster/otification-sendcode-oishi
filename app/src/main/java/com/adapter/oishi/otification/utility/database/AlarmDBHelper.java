package com.adapter.oishi.otification.utility.database;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.util.Log;

import com.adapter.oishi.Receiver;
import com.adapter.oishi.otification.model.AlarmModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class AlarmDBHelper extends SQLiteOpenHelper {

    private Context context;

    public AlarmDBHelper(Context context) {
        super(context, "alarm.db", null, 1);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE alarms (id INTEGER PRIMARY KEY AUTOINCREMENT, activity INTEGER, actor INTEGER, hour INTEGER, minute INTEGER, state INTEGER," +
                "su INTEGER, mo INTEGER, tu INTEGER, we INTEGER, th INTEGER, fr INTEGER, sa INTEGER," +
                "sound INTEGER, vibration INTEGER, snooze INTEGER)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS alarms");
        onCreate(db);
    }

    public void addAlarm(int activity, int actor, int hour, int minute) {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        sqLiteDatabase.execSQL(String.format("INSERT INTO alarms (activity, actor, hour, minute, state, sound, vibration) VALUES (%d, %d, %d, %d, 1, 1, 1)", activity, actor, hour, minute));
        sqLiteDatabase.close();

        ArrayList<AlarmModel> alarms = getAlarms();
        AlarmModel addedAlarm = null;
        for (int i = 0; i < alarms.size(); i++) {
            if (alarms.get(i).getActivity() == activity && alarms.get(i).getActor() == actor && alarms.get(i).getHour() == hour && alarms.get(i).getMinute() == minute) {
                addedAlarm = alarms.get(i);
                break;
            }
        }

        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, addedAlarm.getHour());
        c.set(Calendar.MINUTE, addedAlarm.getMinute());
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        if (c.compareTo(Calendar.getInstance()) < 0) {
            c.add(Calendar.DATE, 1);
        }

        addRepeatAlarm(addedAlarm, c.get(Calendar.DAY_OF_WEEK) - 1);
    }

    public int addAlarmCustom(int hour, int minute) {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT COUNT(*) FROM alarms WHERE activity = -1", null);

        int count = -1;
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    count = cursor.getInt(0);
                } while (cursor.moveToNext());
            }
        }
        cursor.close();
        sqLiteDatabase.close();


        SQLiteDatabase sqLiteDatabase2 = getWritableDatabase();
        sqLiteDatabase2.execSQL(String.format("INSERT INTO alarms (activity, actor, hour, minute, state, sound, vibration) VALUES (-1, (%d), %d, %d, 1, 1, 1)", count, hour, minute));
        sqLiteDatabase2.close();

        ArrayList<AlarmModel> alarmModels = getAlarms();

        AlarmModel alarmModel = null;
        for (int i = 0; i < alarmModels.size(); i++) {
            if (alarmModels.get(i).getActivity() == -1 && alarmModels.get(i).getActor() == count) {
                alarmModel = alarmModels.get(i);
            }
        }

        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, alarmModel.getHour());
        c.set(Calendar.MINUTE, alarmModel.getMinute());
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        if (c.compareTo(Calendar.getInstance()) < 0) {
            c.add(Calendar.DATE, 1);
        }

        addRepeatAlarm(alarmModel, c.get(Calendar.DAY_OF_WEEK) - 1);


        return count;
    }

    private static final String[] ds = {
            "Su",
            "Mo",
            "Tu",
            "We",
            "Th",
            "Fr",
            "Sa"
    };

    public void setDay(AlarmModel alarm, boolean[] days) {

        int count = 0;
        for (int i = 0; i < days.length; i++) {
            if (days[i] == true) {
                count++;
                Log.d("dbb", "d " + alarm.getId() + " " + ds[i] + " " + 1);

                SQLiteDatabase sqLiteDatabase = getWritableDatabase();
                sqLiteDatabase.execSQL(String.format("UPDATE alarms SET %s = 1 WHERE id = %d", ds[i], alarm.getId()));
                sqLiteDatabase.close();

                if (alarm.getState() == 1) {
                    addRepeatAlarm(alarm, i);
                }
            } else {
                Log.d("dbb", "d " + alarm.getId() + " " + ds[i] + " " + 0);

                SQLiteDatabase sqLiteDatabase = getWritableDatabase();
                sqLiteDatabase.execSQL(String.format("UPDATE alarms SET %s = 0 WHERE id = %d", ds[i], alarm.getId()));
                sqLiteDatabase.close();

                removeRepeatAlarm(alarm, i);
            }
        }

        if (count == 0) {
            Calendar c = Calendar.getInstance();
            c.set(Calendar.HOUR_OF_DAY, alarm.getHour());
            c.set(Calendar.MINUTE, alarm.getMinute());
            c.set(Calendar.SECOND, 0);
            c.set(Calendar.MILLISECOND, 0);

            if (c.compareTo(Calendar.getInstance()) < 0) {
                c.add(Calendar.DATE, 1);
            }

            addRepeatAlarm(alarm, c.get(Calendar.DAY_OF_WEEK) - 1);
        }

    }

    public void addAlarmsAfterBoot() {
        ArrayList<AlarmModel> alarms = getAlarms();
        for (int i = 0; i < alarms.size(); i++) {
            if (alarms.get(i).getState() == 1) {
                if (alarms.get(i).getSu() == 1) {
                    addRepeatAlarm(alarms.get(i), 0);
                }
                if (alarms.get(i).getMo() == 1) {
                    addRepeatAlarm(alarms.get(i), 1);
                }
                if (alarms.get(i).getTu() == 1) {
                    addRepeatAlarm(alarms.get(i), 2);
                }
                if (alarms.get(i).getWe() == 1) {
                    addRepeatAlarm(alarms.get(i), 3);
                }
                if (alarms.get(i).getTh() == 1) {
                    addRepeatAlarm(alarms.get(i), 4);
                }
                if (alarms.get(i).getFr() == 1) {
                    addRepeatAlarm(alarms.get(i), 5);
                }
                if (alarms.get(i).getSa() == 1) {
                    addRepeatAlarm(alarms.get(i), 6);
                }
            }

        }
    }

    public void addRepeatAlarm(AlarmModel alarm, int day) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context.getApplicationContext(), Receiver.class);
        intent.setAction("com.adapter.oishi.ALARM");
        intent.putExtra("alarm", alarm);

        int requestCode = Integer.parseInt(alarm.getId() + "00" + day);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, requestCode, intent, 0);

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_WEEK, day + 1);
        calendar.set(Calendar.HOUR_OF_DAY, alarm.getHour());
        calendar.set(Calendar.MINUTE, alarm.getMinute());
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        Calendar now = Calendar.getInstance();

        if (calendar.compareTo(now) < 0) {
            calendar.add(Calendar.DATE, 7);
        }

        // add alarm
//        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY * 7, pendingIntent);

        final int SDK_INT = Build.VERSION.SDK_INT;
        if (SDK_INT < Build.VERSION_CODES.KITKAT) {
            alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
        }
        else if (Build.VERSION_CODES.KITKAT <= SDK_INT  && SDK_INT < Build.VERSION_CODES.M) {
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
        }
        else if (SDK_INT >= Build.VERSION_CODES.M) {
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
        }

//        final int SDK_INT = Build.VERSION.SDK_INT;
//        if (SDK_INT < Build.VERSION_CODES.KITKAT) {
//            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY * 7, pendingIntent);
//        }
//        else if (Build.VERSION_CODES.KITKAT <= SDK_INT  && SDK_INT < Build.VERSION_CODES.M) {
//            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY * 7, pendingIntent);
//        }
//        else if (SDK_INT >= Build.VERSION_CODES.M) {
//            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY * 7, pendingIntent);
//        }


        // check alarm
        boolean isWorking = (PendingIntent.getBroadcast(context, requestCode, intent, PendingIntent.FLAG_NO_CREATE) != null);
        Log.d("alarmss", "setRepeating " + requestCode + " " + new SimpleDateFormat("yyyy-MM-dd HH:mm").format(calendar.getTime()) + " " + alarm.toString() + " " + isWorking);
    }

    public void addSnoozeAlarm(AlarmModel alarm) {
        Log.d("addSnoozeAlarm", alarm.toString());
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context.getApplicationContext(), Receiver.class);
        intent.setAction("com.adapter.oishi.ALARM");
        intent.putExtra("alarm", alarm);

        int requestCode = Integer.parseInt(alarm.getId() + "01");
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, requestCode, intent, 0);

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, 10);
        calendar.set(Calendar.SECOND, 0);
//        calendar.add(Calendar.MINUTE, 1);

        // add alarm
//        alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);

        final int SDK_INT = Build.VERSION.SDK_INT;
        if (SDK_INT < Build.VERSION_CODES.KITKAT) {
            alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
        }
        else if (Build.VERSION_CODES.KITKAT <= SDK_INT  && SDK_INT < Build.VERSION_CODES.M) {
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
        }
        else if (SDK_INT >= Build.VERSION_CODES.M) {
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
        }

        // check alarm
        boolean isWorking = (PendingIntent.getBroadcast(context, requestCode, intent, PendingIntent.FLAG_NO_CREATE) != null);
        Log.d("alarmss", "set " + requestCode + " snooze " + new SimpleDateFormat("yyyy-MM-dd HH:mm").format(calendar.getTime()) + " " + alarm.toString() + " " + isWorking);
    }

    private void removeSnoozeAlarm(AlarmModel alarm) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context.getApplicationContext(), Receiver.class);
        intent.setAction("com.adapter.oishi.ALARM");
        intent.putExtra("alarm", alarm);

        int requestCode = Integer.parseInt(alarm.getId() + "01");
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, requestCode, intent, 0);

        // remove alarm
        alarmManager.cancel(pendingIntent);
        pendingIntent.cancel();

        // check alarm
        boolean isWorking = (PendingIntent.getBroadcast(context, requestCode, intent, PendingIntent.FLAG_NO_CREATE) != null);
        Log.d("alarmss", "removeSnoozeAlarm " + alarm.toString() + " " + isWorking);
    }

    private void removeRepeatAlarm(AlarmModel alarm, int day) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context.getApplicationContext(), Receiver.class);
        intent.setAction("com.adapter.oishi.ALARM");
        intent.putExtra("alarm", alarm);

        int requestCode = Integer.parseInt(alarm.getId() + "00" + day);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, requestCode, intent, 0);

        // remove alarm
        alarmManager.cancel(pendingIntent);
        pendingIntent.cancel();

        // check alarm
        boolean isWorking = (PendingIntent.getBroadcast(context, requestCode, intent, PendingIntent.FLAG_NO_CREATE) != null);
        Log.d("alarmss", "cancel " + alarm.toString() + " " + isWorking);
    }

    public void setSound(AlarmModel alarm, boolean state) {
        Log.d("dbb", "setSound " + alarm.getId() + " " + state);
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        sqLiteDatabase.execSQL(String.format("UPDATE alarms SET sound = %d WHERE id = %d", (state ? 1 : 0), alarm.getId()));
        sqLiteDatabase.close();
    }

    public void setVibration(AlarmModel alarm, boolean state) {
        Log.d("dbb", "setVibration " + String.format("UPDATE alarms SET vibration = %d WHERE id = %d", (state ? 1 : 0), alarm.getId()));
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        sqLiteDatabase.execSQL(String.format("UPDATE alarms SET vibration = %d WHERE id = %d", (state ? 1 : 0), alarm.getId()));
        sqLiteDatabase.close();
    }

    public void setSnooze(AlarmModel alarm, boolean state) {
        Log.d("dbb", "setSnooze " + String.format("UPDATE alarms SET snooze = %d WHERE id = %d", (state ? 1 : 0), alarm.getId()));
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        sqLiteDatabase.execSQL(String.format("UPDATE alarms SET snooze = %d WHERE id = %d", (state ? 1 : 0), alarm.getId()));
        sqLiteDatabase.close();
    }

    public void setState(AlarmModel alarm, boolean state) {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        sqLiteDatabase.execSQL(String.format("UPDATE alarms SET state = %d WHERE id = %d", (state ? 1 : 0), alarm.getId()));
        sqLiteDatabase.close();

        if (state) {
            int count = 0;
            if (alarm.getSu() == 1) {
                count++;
                addRepeatAlarm(alarm, 0);
            }
            if (alarm.getMo() == 1) {
                count++;
                addRepeatAlarm(alarm, 1);
            }
            if (alarm.getTu() == 1) {
                count++;
                addRepeatAlarm(alarm, 2);
            }
            if (alarm.getWe() == 1) {
                count++;
                addRepeatAlarm(alarm, 3);
            }
            if (alarm.getTh() == 1) {
                count++;
                addRepeatAlarm(alarm, 4);
            }
            if (alarm.getFr() == 1) {
                count++;
                addRepeatAlarm(alarm, 5);
            }
            if (alarm.getSa() == 1) {
                count++;
                addRepeatAlarm(alarm, 6);
            }

            if (count == 0) {
                Calendar c = Calendar.getInstance();
                c.set(Calendar.HOUR_OF_DAY, alarm.getHour());
                c.set(Calendar.MINUTE, alarm.getMinute());
                c.set(Calendar.SECOND, 0);
                c.set(Calendar.MILLISECOND, 0);

                if (c.compareTo(Calendar.getInstance()) < 0) {
                    c.add(Calendar.DATE, 1);
                }

                addRepeatAlarm(alarm, c.get(Calendar.DAY_OF_WEEK) - 1);
            }
        } else {
            for (int i = 0; i < 7; i++) {
                removeRepeatAlarm(alarm, i);
            }

            removeSnoozeAlarm(alarm);
        }
    }

    public void deleteAllAlarm() {
        ArrayList<AlarmModel> alarmModels = getAlarms();
        for (int i = 0; i < alarmModels.size(); i++) {
            deleteAlarm(alarmModels.get(i));
        }
    }

    public void deleteAlarm(AlarmModel alarm) {
        for (int i = 0; i < 7; i++) {
            removeRepeatAlarm(alarm, i);
        }

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        sqLiteDatabase.execSQL(String.format("DELETE FROM alarms WHERE id = %d", alarm.getId()));
        sqLiteDatabase.close();

        removeSnoozeAlarm(alarm);
    }

    public ArrayList<AlarmModel> getAlarms() {
        ArrayList<AlarmModel> alarms = new ArrayList<AlarmModel>();

        SQLiteDatabase sqLiteDatabase = getReadableDatabase();

        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM alarms ORDER BY id DESC", null);

        if (cursor != null) {
            cursor.moveToFirst();
        }

        while (!cursor.isAfterLast()) {

            alarms.add(new AlarmModel(
                    cursor.getInt(0),
                    cursor.getInt(1),
                    cursor.getInt(2),
                    cursor.getInt(3),
                    cursor.getInt(4),
                    cursor.getInt(5),
                    cursor.getInt(6),
                    cursor.getInt(7),
                    cursor.getInt(8),
                    cursor.getInt(9),
                    cursor.getInt(10),
                    cursor.getInt(11),
                    cursor.getInt(12),
                    cursor.getInt(13),
                    cursor.getInt(14),
                    cursor.getInt(15)
            ));

            cursor.moveToNext();
        }

        sqLiteDatabase.close();

        return alarms;
    }
}
