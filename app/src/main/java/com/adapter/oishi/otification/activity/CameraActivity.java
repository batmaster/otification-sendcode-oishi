package com.adapter.oishi.otification.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.adapter.oishi.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class CameraActivity extends Activity {

    private ImageView imageViewClose;
    private ImageView imageViewDot;
    private ImageView imageViewSwitch;
    private TextView textViewTime;
    private ImageView imageViewRecord;

    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 29;
    MediaRecorder recorder;
    SurfaceHolder holder;
    boolean recording = false;

    private int hasRecord = 0;

    private Camera camera;

    private Thread t;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otification_activity_camera);

        File folder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + "/oishi");
        if (!folder.exists()) {
            folder.mkdir();
        }

        imageViewClose = (ImageView) findViewById(R.id.imageViewClose);
        imageViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (recording) {
                    stopRecord(false);

                }
                Intent result = new Intent();
                result.putExtra("ok", false);
                setResult(Activity.RESULT_OK, result);
                finish();
            }
        });

        imageViewDot = (ImageView) findViewById(R.id.imageViewDot);
        imageViewDot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        imageViewSwitch = (ImageView) findViewById(R.id.imageViewSwitch);
        imageViewSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Camera.getNumberOfCameras() > 1) {
                    if (camNum == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                        camNum = 0;
                    }
                    else {
                        camNum = Camera.CameraInfo.CAMERA_FACING_FRONT;
                    }
                }

            }
        });

        if (Camera.getNumberOfCameras() < 2) {
            imageViewSwitch.setVisibility(View.INVISIBLE);
        }

        imageViewRecord = (ImageView) findViewById(R.id.imageViewRecord);
        imageViewRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (recording) {
                    hasRecord++;
                    stopRecord(true);

                } else {
                    hasRecord++;
                    recording = true;
                    recorder.start();

                    startAnimRedDot();
                    t = new Thread() {
                        @Override
                        public void run() {
                            try {

                                int i = 0;
                                while (!isInterrupted() && i < 5 && recording) {

                                    Thread.sleep(1000);
                                    i++;

                                    final int finalI = i;
                                    try {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                textViewTime.setText(String.format("00:0%d", finalI));

                                                if (finalI == 5) {
                                                    Log.d("vidflow", finalI + "");
                                                    if (recording) {
                                                        Log.d("vidflow", finalI + " 2");
                                                        stopRecord(true);
                                                    }
                                                }
                                            }
                                        });
                                    } catch (NullPointerException e) {
                                        e.printStackTrace();
                                    }
                                }

                            } catch (InterruptedException e) {

                            }
                        }
                    };

                    t.start();
                }
            }
        });

        textViewTime = (TextView) findViewById(R.id.textViewTime);


        recorder = new MediaRecorder();



        initRecorder();


        SurfaceView cameraView = (SurfaceView) findViewById(R.id.surfaceView);
        holder = cameraView.getHolder();
        holder.addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                prepareRecorder();
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
                //
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
//                if (recording) {
//                    stopRecord();
//                }
            }
        });
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }


    public void stopRecord(boolean save) {

        if (recording) {
            Log.d("vidflow", "stopRecord");
            recorder.stop();
            recording = false;

            recorder.reset();
            recorder.release();
            recorder = null;

            camera.stopPreview();

            camera.lock();
            camera.release();

            camera = null;

            stopAnimRedDot();

            if (save) {
                Uri uri = Uri.fromFile(new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + "/oishi/", videoFileName + ".mp4"));
                Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(uri.getPath(), MediaStore.Video.Thumbnails.MICRO_KIND);
                storeImage(bitmap);
            }

            Intent result = new Intent();
            result.putExtra("ok", true);
            setResult(Activity.RESULT_OK, result);
            finish();
        }
    }

    private AnimationSet animSet;
    private void startAnimRedDot() {
        imageViewDot.setVisibility(View.VISIBLE);

        animSet = new AnimationSet(true);
        Animation fadeOutLoopAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_out_loop);
        animSet.addAnimation(fadeOutLoopAnimation);

        imageViewDot.startAnimation(animSet);
    }

    private void stopAnimRedDot() {
        animSet.cancel();
        imageViewDot.clearAnimation();
        imageViewDot.setAnimation(null);
    }

    private int camNum = 0;
    private void initRecorder() {
        Log.d("vidd", "initRecorder");

        if (Camera.getNumberOfCameras() > 1) {
            camera = Camera.open(Camera.CameraInfo.CAMERA_FACING_FRONT);

            camNum = Camera.CameraInfo.CAMERA_FACING_FRONT;
        }
        else {
            camera = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);
        }

        camera.setDisplayOrientation(90);

        recorder.setCamera(camera);
        camera.unlock();

        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);


        if (camNum == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            recorder.setOrientationHint(270);
        }
        else {
            recorder.setOrientationHint(90);
        }


//        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
//
//        recorder.setVideoSize(320, 480);
//        recorder.setVideoFrameRate(25);
//        recorder.setVideoEncoder(MediaRecorder.VideoEncoder.DEFAULT);
//        recorder.setVideoEncodingBitRate(8000000);
//
//        recorder.setAudioChannels(2);
//        recorder.setAudioSamplingRate(8000);
//        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
//        recorder.setAudioEncodingBitRate(32000);


        CamcorderProfile cpHigh = CamcorderProfile.get(CamcorderProfile.QUALITY_LOW);
        recorder.setProfile(cpHigh);
//        recorder.setOutputFile("/sdcard/videocapture_example.mp4");
//        recorder.setOutputFile("/storage/emulated/0/Documents/oishi/videoFileName.mp4");
        videoFileName = getIntent().getStringExtra("filename");
        recorder.setOutputFile(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + "/oishi/" + videoFileName + ".mp4");
        recorder.setMaxDuration(5200); // 5.2 seconds
//        recorder.setMaxFileSize(5000000); // Approximately 5 megabytes
    }

    private String videoFileName;

    private void prepareRecorder() {
        Log.d("vidd", "prepareRecorder");
        recorder.setPreviewDisplay(holder.getSurface());

        try {
            recorder.prepare();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String TAG = "TAG";
    private void storeImage(Bitmap bitmap) {
        String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath();
        OutputStream fOutputStream = null;
        File file = new File(path + "/oishi/", videoFileName + ".png");

        try {
            fOutputStream = new FileOutputStream(file);

            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOutputStream);

            fOutputStream.flush();
            fOutputStream.close();

            MediaStore.Images.Media.insertImage(getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(this, "Save Failed", Toast.LENGTH_SHORT).show();
            return;
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, "Save Failed", Toast.LENGTH_SHORT).show();
            return;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            if (recording) {
                stopRecord(false);
            }
            else {
                if (camera != null) {
                    camera.lock();
                    camera.release();
                    camera = null;
                }
            }
        }
        catch (RuntimeException e) {
            e.printStackTrace();
        }
    }

}
