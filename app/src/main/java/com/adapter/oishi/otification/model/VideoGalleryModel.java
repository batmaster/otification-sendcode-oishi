package com.adapter.oishi.otification.model;

/**
 * Created by batmaster on 8/19/16 AD.
 */
public class VideoGalleryModel {
    private String imageUrl;
    private String text;

    public VideoGalleryModel(String imageUrl, String text) {
        this.imageUrl = imageUrl;
        this.text = text;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
