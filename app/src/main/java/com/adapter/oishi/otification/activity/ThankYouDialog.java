package com.adapter.oishi.otification.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.adapter.oishi.R;
import com.adapter.oishi.otification.model.PlayListAlarmModel;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

import java.util.Arrays;

public class ThankYouDialog extends Activity {

    private LinearLayout parent;
    private ImageView imageViewClose;
    private ImageView imageViewFacebook;
    private ImageView imageViewLine;

    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otification_activity_thank_you_dialog);

        final PlayListAlarmModel media = (PlayListAlarmModel) getIntent().getSerializableExtra("media");

        imageViewClose = (ImageView) findViewById(R.id.imageViewClose);
        imageViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        imageViewFacebook = (ImageView) findViewById(R.id.imageViewFacebook);
        imageViewFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (AccessToken.getCurrentAccessToken() == null) {
                    String[] permissions = {"public_profile", "email"};
                    LoginManager.getInstance().logInWithReadPermissions(ThankYouDialog.this, Arrays.asList(permissions));
                }
                else {
                    share(media);
                }

            }
        });

        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                        share(media);
            }

            @Override
            public void onCancel() {
                int a = 0;
            }

            @Override
            public void onError(FacebookException error) {
                int a = 0;
            }
        });

        imageViewLine = (ImageView) findViewById(R.id.imageViewLine);
        imageViewLine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent sharingIntent = getPackageManager().getLaunchIntentForPackage(AppConfig.LINE_PACKAGE_NAME);
//                sharingIntent.setAction(Intent.ACTION_SEND);
//                sharingIntent.setType("text/plain");
                String shareBody = "line://msg/text/" + media.getsUrl();
                Log.d("lineee", shareBody);
//                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, media.getNoti_title());
//                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
//                startActivity(Intent.createChooser(sharingIntent, "Share via"));

                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(shareBody)));
            }
        });

        parent = (LinearLayout) findViewById(R.id.parent);

        if (media == null) {
            parent.setBackgroundResource(R.drawable.bg_thankyou_empty);
            imageViewFacebook.setVisibility(View.INVISIBLE);
            imageViewLine.setVisibility(View.INVISIBLE);
        }

        boolean gotoList = getIntent().getBooleanExtra("gotoList", false);
        if (gotoList) {
            Intent intent = new Intent();
            intent.putExtra("gotoList", true);
            setResult(Activity.RESULT_OK, intent);
        }
    }

    private void share(PlayListAlarmModel media) {

        String url = media.getsUrl();
        String title = media.getsTitle();
        String description = media.getsDescription();
        String image = media.getsImage();

        ShareLinkContent content = new ShareLinkContent.Builder()
                .setImageUrl(Uri.parse(image))
                .setContentUrl(Uri.parse(url))
                .setContentTitle(title)
                .setContentDescription(description)
                .build();
        Log.d("facee", "dialog ok");

        ShareDialog dialog = new ShareDialog(ThankYouDialog.this);
        dialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                Log.d("facee", "onSuccess");
//                app.getHttpService().saveShare(gid, result.getPostId(), AccessToken.getCurrentAccessToken().getToken(), new HTTPService.OnResponseCallback<JSONObject>() {
//                    @Override
//                    public void onResponse(boolean success, Throwable error, JSONObject data) {
//                        Log.d("facee", "onSuccess saveShare");
//                        EstApplication app = (EstApplication) getApplication();
//                        app.getHttpService().sendEstColaStat(HTTPService.EST_STAT_SHARE_RESULT);
//                    }
//                });
            }

            @Override
            public void onCancel() {
                Log.d("facee", "onCancel()");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d("facee", "onError");
                Log.d("share 2", "" + error.getMessage());
            }
        });
        if (dialog.canShow(ShareLinkContent.class)) {
            dialog.show(content);
        } else {
            Log.d("facee", "you cannot share :(");
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
