package com.adapter.oishi.otification.utility.pager;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by batmaster on 8/26/16 AD.
 */
public class ActivityPageItem implements Parcelable {

    private int normalRid;
    private int activeRid;
    private String text;

    public ActivityPageItem(int normalRid, int activeRid, String text) {
        this.normalRid = normalRid;
        this.activeRid = activeRid;
        this.text = text;
    }

    protected ActivityPageItem(Parcel in) {
        normalRid = in.readInt();
        activeRid = in.readInt();
        text = in.readString();
    }

    public int getNormalRid() {
        return normalRid;
    }

    public void setNormalRid(int normalRid) {
        this.normalRid = normalRid;
    }

    public int getActiveRid() {
        return activeRid;
    }

    public void setActiveRid(int activeRid) {
        this.activeRid = activeRid;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }


    public static final Creator<ActivityPageItem> CREATOR = new Creator<ActivityPageItem>() {
        @Override
        public ActivityPageItem createFromParcel(Parcel in) {
            return new ActivityPageItem(in);
        }

        @Override
        public ActivityPageItem[] newArray(int size) {
            return new ActivityPageItem[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(text);
    }
}
