package com.adapter.oishi.otification.model;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Created by batmaster on 8/19/16 AD.
 */
public class AlarmModel implements Serializable {

    private int id;
    private int activity;
    private int actor;
    private int hour;
    private int minute;
    private int state;
    private int su;
    private int mo;
    private int tu;
    private int we;
    private int th;
    private int fr;
    private int sa;
    private int sound;
    private int vibration;
    private int snooze;


    public static String[] activity_th = {
            "ตื่นนอน",
            "ออกกำลังกาย",
            "อ่านหนังสือ",
            "ฝันดี",
            "นัดนู่นนี่นั่น"
    };

    public static String[] activity_message = {
            "ถึงเวลาตื่นแล้ว",
            "ถึงเวลาออกกำลังกายแล้ว",
            "ได้เวลาอ่านหนังสือแล้ว",
            "ได้เวลานอนแล้ว",
            "ถึงเวลานัดแล้ว"
    };

    private String[] actor_th = {
            "พุฒ พุฒิชัย",
            "ต่อ ธนภพ",
            "มาร์ช จุฑาวุฒิ",
            "กั้ง วรกร",
            "มุก วรนิษฐ์",
            "ออฟ จุมพล",
    };

    public AlarmModel(int id, int activity, int actor, int hour, int minute, int state, int su, int mo, int tu, int we, int th, int fr, int sa, int sound, int vibration, int snooze) {
        this.id = id;
        this.activity = activity;
        this.actor = actor;
        this.hour = hour;
        this.minute = minute;
        this.state = state;
        this.su = su;
        this.mo = mo;
        this.tu = tu;
        this.we = we;
        this.th = th;
        this.fr = fr;
        this.sa = sa;
        this.sound = sound;
        this.vibration = vibration;
        this.snooze = snooze;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getActivity() {
        return activity;
    }

    public void setActivity(int activity) {
        this.activity = activity;
    }

    public int getActor() {
        return actor;
    }

    public void setActor(int actor) {
        this.actor = actor;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getSu() {
        return su;
    }

    public void setSu(int su) {
        this.su = su;
    }

    public int getMo() {
        return mo;
    }

    public void setMo(int mo) {
        this.mo = mo;
    }

    public int getTu() {
        return tu;
    }

    public void setTu(int tu) {
        this.tu = tu;
    }

    public int getWe() {
        return we;
    }

    public void setWe(int we) {
        this.we = we;
    }

    public int getTh() {
        return th;
    }

    public void setTh(int th) {
        this.th = th;
    }

    public int getFr() {
        return fr;
    }

    public void setFr(int fr) {
        this.fr = fr;
    }

    public int getSa() {
        return sa;
    }

    public void setSa(int sa) {
        this.sa = sa;
    }

    public int getSound() {
        return sound;
    }

    public void setSound(int sound) {
        this.sound = sound;
    }

    public int getVibration() {
        return vibration;
    }

    public void setVibration(int vibration) {
        this.vibration = vibration;
    }

    public int getSnooze() {
        return snooze;
    }

    public void setSnooze(int snooze) {
        this.snooze = snooze;
    }

    public String getDays() {
        String d = "";
        if (su == 1) {
            d += "อา ";
        }
        if (mo == 1) {
            d += "จ ";
        }
        if (tu == 1) {
            d += "อ ";
        }
        if (we == 1) {
            d += "พ ";
        }
        if (th == 1) {
            d += "พฤ ";
        }
        if (fr == 1) {
            d += "ศ ";
        }
        if (sa == 1) {
            d += "ส ";
        }

        return d.trim();
    }

    public String getActivityTH() {
        return activity_th[activity];
    }

    public String getActorTH() {
        return actor_th[activity];
    }

    public String getTime() {
        return String.format("%02d:%02d", hour, minute);
    }

    public int[] getDaysSet() {
        return new int[] {
                su, mo, tu, we, th, fr, sa
        };
    };

    @Override
    public String toString() {
        return "AlarmModel{" +
                "id=" + id +
                ", activity=" + activity +
                ", actor=" + actor +
                ", hour=" + hour +
                ", minute=" + minute +
                ", state=" + state +
                ", su=" + su +
                ", mo=" + mo +
                ", tu=" + tu +
                ", we=" + we +
                ", th=" + th +
                ", fr=" + fr +
                ", sa=" + sa +
                ", sound=" + sound +
                ", vibration=" + vibration +
                ", snooze=" + snooze +
                ", activity_th=" + Arrays.toString(activity_th) +
                ", actor_th=" + Arrays.toString(actor_th) +
                '}';
    }
}
