package com.adapter.oishi.otification.activity.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;

import com.adapter.oishi.OishiApplication;
import com.adapter.oishi.R;
import com.adapter.oishi.otification.model.ShareModel;

import java.util.ArrayList;

public class MyListFriendFragment extends Fragment {

    private OishiApplication app;

    private ImageView imageViewCreate;
    private ListView listView;
    private ImageView imageViewSelf;

    public MyListFriendFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.otification_fragment_my_list_friend, container, false);

        app = (OishiApplication) getActivity().getApplication();

        imageViewCreate = (ImageView) rootView.findViewById(R.id.imageViewCreate);
        imageViewCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                app.sendButtonStat("bnt_list_crebnt_list_createate");

                ((MainActivity) getActivity()).setFragment(6, null);
            }
        });

        listView = (ListView) rootView.findViewById(R.id.listView);

        ArrayList<ShareModel> shares = app.getShareDB().getAlarms();
        ListViewAdapter adapter = new ListViewAdapter(getActivity(), null, shares, ListViewAdapter.MODE_FRIEND);
        listView.setAdapter(adapter);

        imageViewSelf = (ImageView) rootView.findViewById(R.id.imageViewSelf);
        imageViewSelf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).setFragment(1, null);
            }
        });

        return rootView;
    }
}
