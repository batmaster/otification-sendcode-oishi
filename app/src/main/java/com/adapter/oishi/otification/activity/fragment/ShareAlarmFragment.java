package com.adapter.oishi.otification.activity.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.acbelter.directionalcarousel.CarouselPagerAdapter;
import com.acbelter.directionalcarousel.CarouselViewPager;
import com.acbelter.directionalcarousel.page.OnPageClickListener;
import com.adapter.oishi.OishiApplication;
import com.adapter.oishi.R;
import com.adapter.oishi.otification.activity.LoadingActivity;
import com.adapter.oishi.otification.activity.ThankYouDialog;
import com.adapter.oishi.otification.model.PlayListFriendModel;
import com.adapter.oishi.otification.utility.CustomViewPager;
import com.adapter.oishi.otification.utility.PreferenceService;
import com.adapter.oishi.otification.utility.pager.ActivityPageItem;
import com.adapter.oishi.otification.utility.pager.ActorPagerAdapter;
import com.adapter.oishi.otification.utility.service.HTTPService;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

public class ShareAlarmFragment extends Fragment {

    public static ShareAlarmFragment getInstance() {
        return new ShareAlarmFragment();
    }

    private OishiApplication app;

    private int activitySelected = 0;
    private CarouselViewPager carouselActivity;
    private ArrayList<ActivityPageItem> itemsActivity;
    private CarouselPagerAdapter<ActivityPageItem> adapterActivity;

    private CallbackManager callbackManager;
    private ImageView imageViewWhite;

    private static final int[] resActivity = {
            R.drawable.btn_normal_7,
            R.drawable.btn_normal_8,
            R.drawable.btn_normal_1,
            R.drawable.btn_normal_6,
            R.drawable.btn_normal_9,
            R.drawable.btn_normal_10,
            R.drawable.btn_normal_11,
            R.drawable.btn_normal_12
    };

    private static final int[] res2Activity = {
            R.drawable.btn_active_7,
            R.drawable.btn_active_8,
            R.drawable.btn_active_1,
            R.drawable.btn_active_6,
            R.drawable.btn_active_9,
            R.drawable.btn_active_10,
            R.drawable.btn_active_11,
            R.drawable.btn_active_12
    };

    private int actorSelected = 0;
    private static final String[] textActivity = {
            "HBD",
            "คิดถึง",
            "ฝันดี",
            "ให้กำลังใจ",
            "ชวนเที่ยว",
            "ง้อ",
            "ขอบคุณ",
            "Take Care",
    };

    private static final String[] names = {
            "พุฒ พุฒิชัย",
            "ต่อ ธนภพ",
            "มาร์ช จุฑาวุฒิ",
            "กั้ง วรกร",
            "มุก วรนิษฐ์",
            "ออฟ จุมพล",
    };

    private ArrayList<PlayListFriendModel> playListFriendModels;

    private ImageView imageViewLeft;
    private ImageView imageViewRight;

    private CustomViewPager viewPagerActor;
    public final static int PAGES = 6;
    public final static int LOOPS = 1000;
    public final static int FIRST_PAGE = PAGES * LOOPS / 2;

    private TextView textViewName;

    private ImageView imageViewFacebook;
    private ImageView imageViewLine;

    private ImageView imageViewSelf;

    public ShareAlarmFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.otification_fragment_share_alarm, container, false);

        app = (OishiApplication) getActivity().getApplication();

        carouselActivity = (CarouselViewPager) rootView.findViewById(R.id.carouselActivity);
        itemsActivity = new ArrayList<ActivityPageItem>(resActivity.length);
        for (int i = 0; i < resActivity.length; i++) {
            itemsActivity.add(new ActivityPageItem(resActivity[i], res2Activity[i], textActivity[i]));
        }

        final String[] activityGA = {
                "bnt_f_hbd",
                "bnt_f_missu",
                "bnt_f_gn",
                "bnt_f_fight",
                "bnt_f_travel",
                "bnt_f_sorry",
                "bnt_f_thnx",
                "bnt_f_takecare",
        };

        adapterActivity = new CarouselPagerAdapter<ActivityPageItem>(getContext(), itemsActivity);
        adapterActivity.setOnPageClickListener(new OnPageClickListener<ActivityPageItem>() {
            @Override
            public void onSingleTap(View view, ActivityPageItem item) {

            }

            @Override
            public void onDoubleTap(View view, ActivityPageItem item) {

            }

            @Override
            public void onSelected(int position) {
                int i = position % resActivity.length;
                activitySelected = i;

                app.sendButtonStat(activityGA[activitySelected]);

                (new PreferenceService(getContext())).putString("activitySelected", String.valueOf(activitySelected));

                if (activityActor != null) {
                    setActor();
                }
            }
        });

        imageViewWhite = (ImageView) rootView.findViewById(R.id.imageViewWhite);

        carouselActivity.setAdapter(adapterActivity);
        carouselActivity.setOffscreenPageLimit(6);
        carouselActivity.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (state == 0) {
                    imageViewWhite.setVisibility(View.VISIBLE);
                } else {
                    imageViewWhite.setVisibility(View.INVISIBLE);
                }

                switch (state) {
                    case 2:
                        carouselActivity.setPagingEnabled(false);
                        break;
                    case 0:
                        carouselActivity.setPagingEnabled(true);
                        break;
                }
            }
        });

        viewPagerActor = (CustomViewPager) rootView.findViewById(R.id.viewPagerActor);

        textViewName = (TextView) rootView.findViewById(R.id.textViewName);

        imageViewFacebook = (ImageView) rootView.findViewById(R.id.imageViewFacebook);
        imageViewFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                app.sendButtonStat("bnt_f_facebook");

                if (AccessToken.getCurrentAccessToken() == null) {
                    String[] permissions = {"public_profile", "email"};

                    LoginManager.getInstance().logInWithReadPermissions(getActivity(), Arrays.asList(permissions));
                } else {
                    share();
                }


                callbackManager = ((MainActivity) getActivity()).setFacebookResponseWaitingFragmentToShare(ShareAlarmFragment.this);

            }
        });

        imageViewLine = (ImageView) rootView.findViewById(R.id.imageViewLine);
        imageViewLine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                app.sendButtonStat("bnt_f_line");

                app.getHttpService().getPlaylistFriend(new HTTPService.OnResponseCallback<JSONObject>() {
                    @Override
                    public void onResponse(boolean success, Throwable error, JSONObject data) {
                        if (!(data == null)) {
                            try {
                                JSONArray group = data.getJSONObject("playlist").getJSONArray("group");
                                for (int i = 0; i < group.length(); i++) {

                                    JSONObject g = group.getJSONObject(i);

                                    int no = g.getInt("no");
                                    int activity = g.getInt("no") - 1;

                                    JSONArray list = g.getJSONArray("list");
                                    for (int j = 0; j < list.length(); j++) {
                                        JSONObject l = list.getJSONObject(j);

                                        int actor = l.getInt("actor") - 1;

                                        if (activity == activitySelected && actor == actorSelected) {

                                            Intent intent = new Intent(getActivity().getApplicationContext(), ThankYouDialog.class);
                                            intent.putExtra("gotoList", true);
                                            getActivity().startActivityForResult(intent, 1678);

                                            try {
                                                String shareBody = "line://msg/text/" + l.getString("share_url");
                                                getActivity().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(shareBody)));
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }

                                            Calendar c = Calendar.getInstance();
                                            app.getShareDB().addAlarm(2, activitySelected, actorSelected, c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE));

                                            int videoId = Integer.parseInt(l.getString("share_url").split("/")[l.getString("share_url").split("/").length - 1].replace(".html", ""));

                                            app.sendButtonStat("send_line_" + videoId);

                                            app.getHttpService().saveGame(false, c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), videoId, -1, new HTTPService.OnResponseCallback<JSONObject>() {
                                                @Override
                                                public void onResponse(boolean success, Throwable error, JSONObject data) {

                                                }
                                            });

                                        }
                                    }
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        } else {
                            Toast.makeText(getContext(), "เชื่อมต่ออินเตอร์เน็ตไม่ได้", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });

        imageViewLeft = (ImageView) rootView.findViewById(R.id.imageViewLeft);
        imageViewLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPagerActor.setCurrentItem(viewPagerActor.getCurrentItem() - 1, true);
            }
        });

        imageViewRight = (ImageView) rootView.findViewById(R.id.imageViewRight);
        imageViewRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPagerActor.setCurrentItem(viewPagerActor.getCurrentItem() + 1, true);
            }
        });

        imageViewSelf = (ImageView) rootView.findViewById(R.id.imageViewSelf);
        imageViewSelf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                app.sendButtonStat("bnt_myself");

                ((MainActivity) getActivity()).setFragment(0, null);
            }
        });

        app.getHttpService().getPlaylistFriend(new HTTPService.OnResponseCallback<JSONObject>() {
            @Override
            public void onResponse(boolean success, Throwable error, JSONObject data) {
                if (!(data == null)) {
                    try {

                        playListFriendModels = new ArrayList<PlayListFriendModel>();

                        JSONArray group = data.getJSONObject("playlist").getJSONArray("group");
                        for (int i = 0; i < group.length(); i++) {

                            JSONObject g = group.getJSONObject(i);

                            int activity = g.getInt("no") - 1;

                            JSONArray list = g.getJSONArray("list");
                            for (int j = 0; j < list.length(); j++) {
                                JSONObject l = list.getJSONObject(j);

                                int actor = l.getInt("actor") - 1;

                                playListFriendModels.add(new PlayListFriendModel(activity, actor, l.getInt("active"), l.getString("name"), l.getString("video"), l.getString("gallery_image"), l.getString("share_url"), l.getString("share_title"), l.getString("share_description"), l.getString("share_image")));
                            }
                        }

                        activityActor = (ArrayList<Integer>[]) new ArrayList[8];
                        for (int i = 0; i < 8; i++) {
                            activityActor[i] = new ArrayList<Integer>();
                        }
                        for (int i = 0; i < playListFriendModels.size(); i++) {
                            activityActor[playListFriendModels.get(i).getActivity()].add(playListFriendModels.get(i).getActor());
                        }

                        activityActorActive = (ArrayList<Integer>[]) new ArrayList[8];
                        for (int i = 0; i < 8; i++) {
                            activityActorActive[i] = new ArrayList<Integer>();
                        }
                        for (int i = 0; i < playListFriendModels.size(); i++) {
                            activityActorActive[playListFriendModels.get(i).getActivity()].add(playListFriendModels.get(i).getActive());
                        }

                        setActor();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getContext(), "เชื่อมต่ออินเตอร์เน็ตไม่ได้", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return rootView;
    }

    private ArrayList<Integer>[] activityActor;
    private ArrayList<Integer>[] activityActorActive;
    private ActorPagerAdapter adapter;

    public void setActor() {
        actorSelected = activityActor[activitySelected].get(0);

        int[] actors = new int[activityActor[activitySelected].size()];
        for (int i = 0; i < activityActor[activitySelected].size(); i++) {
            actors[i] = activityActor[activitySelected].get(i);
        }

        int[] actorsActive = new int[activityActorActive[activitySelected].size()];
        for (int i = 0; i < activityActorActive[activitySelected].size(); i++) {
            actorsActive[i] = activityActorActive[activitySelected].get(i);
        }

        Log.d("setActor", "activity:" + activitySelected + " numActor:" + actors.length);

        if (adapter == null) {
            adapter = new ActorPagerAdapter(getActivity(), getContext(), ShareAlarmFragment.this, getFragmentManager(), actors, actorsActive, false);
            adapter.setModelsForFriend(PlayListFriendModel.searchByActivity(playListFriendModels, activitySelected));
            viewPagerActor.setAdapter(adapter);
            viewPagerActor.setPageTransformer(false, adapter);
            viewPagerActor.setCurrentItem(FIRST_PAGE);
            viewPagerActor.setOffscreenPageLimit(activityActor[activitySelected].size());
//            viewPagerActor.setPageMargin(-600);
            viewPagerActor.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    position %= activityActor[activitySelected].size();

                    actorSelected = activityActor[activitySelected].get(position);
                    textViewName.setText(names[actorSelected]);

                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
        } else {
            adapter.setActors(actors, actorsActive);
            viewPagerActor.setCurrentItem(3000);
            adapter.setModelsForFriend(PlayListFriendModel.searchByActivity(playListFriendModels, activitySelected));
            adapter.notifyDataSetChanged();
            viewPagerActor.invalidate();
        }

        textViewName.setText(names[actorSelected]);

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    public void share() {

        app.getHttpService().getPlaylistFriend(new HTTPService.OnResponseCallback<JSONObject>() {
            @Override
            public void onResponse(boolean success, Throwable error, JSONObject data) {
                if (!(data == null)) {
                    try {
                        JSONArray group = data.getJSONObject("playlist").getJSONArray("group");
                        for (int i = 0; i < group.length(); i++) {

                            JSONObject g = group.getJSONObject(i);

                            int activity = g.getInt("no") - 1;

                            JSONArray list = g.getJSONArray("list");
                            for (int j = 0; j < list.length(); j++) {
                                final JSONObject l = list.getJSONObject(j);

                                int actor = l.getInt("actor") - 1;

                                if (activity == activitySelected && actor == actorSelected) {
                                    PlayListFriendModel media = new PlayListFriendModel(activity, actor, l.getInt("active"), l.getString("name"), l.getString("video"), l.getString("gallery_image"), l.getString("share_url"), l.getString("share_title"), l.getString("share_description"), l.getString("share_image"));

                                    if (app.getPlayListFriendDB().checkHasToDownload(media)) {
                                        // TODO HEREEE
                                        Intent intent = new Intent(getContext(), LoadingActivity.class);
                                        intent.putExtra("media", media);
                                        intent.putExtra("shouldPreview", false);
                                        intent.putExtra("isFromAlarm", false);
                                        getActivity().startActivityForResult(intent, 1907);
                                    } else {
                                        final String url = media.getUrl();
                                        String title = media.getTitle();
                                        String description = media.getDescription();
                                        String image = media.getShareImage();

                                        ShareLinkContent content = new ShareLinkContent.Builder()
                                                .setImageUrl(Uri.parse(image))
                                                .setContentUrl(Uri.parse(url))
                                                .setContentTitle(title)
                                                .setContentDescription(description)
                                                .build();
                                        Log.d("facee", "dialog ok");

                                        ShareDialog dialog = new ShareDialog(getActivity());
                                        dialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                                            @Override
                                            public void onSuccess(Sharer.Result result) {
                                                Log.d("facee", "onSuccess");

                                                Calendar c = Calendar.getInstance();
                                                app.getShareDB().addAlarm(2, activitySelected, actorSelected, c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE));

                                                int videoId = Integer.parseInt(url.split("/")[url.split("/").length - 1].replace(".html", ""));
                                                app.sendButtonStat("send_fb_" + videoId);

                                                app.getHttpService().saveGame(false, c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), videoId, activitySelected + 1, new HTTPService.OnResponseCallback<JSONObject>() {
                                                    @Override
                                                    public void onResponse(boolean success, Throwable error, JSONObject data) {

                                                    }
                                                });

                                                app.getHttpService().saveShareToWall(videoId, result.getPostId(), new HTTPService.OnResponseCallback<JSONObject>() {
                                                    @Override
                                                    public void onResponse(boolean success, Throwable error, JSONObject data) {

                                                    }
                                                });

                                                startActivity(new Intent(getActivity().getApplicationContext(), ThankYouDialog.class));

                                                ((MainActivity) getActivity()).setFragment(7, null);

//                                                app.getShareDB().addAlarm();
                                                //                app.getHttpService().saveShare(gid, result.getPostId(), AccessToken.getCurrentAccessToken().getToken(), new HTTPService.OnResponseCallback<JSONObject>() {
                                                //                    @Override
                                                //                    public void onResponse(boolean success, Throwable error, JSONObject data) {
                                                //                        Log.d("facee", "onSuccess saveShare");
                                                //                        EstApplication app = (EstApplication) getApplication();
                                                //                        app.getHttpService().sendEstColaStat(HTTPService.EST_STAT_SHARE_RESULT);
                                                //                    }
                                                //                });
                                            }

                                            @Override
                                            public void onCancel() {
                                                Log.d("facee", "onCancel()");
                                            }

                                            @Override
                                            public void onError(FacebookException error) {
                                                Log.d("facee", "onError");
                                                Log.d("share 2", "" + error.getMessage());
                                            }
                                        });
                                        if (dialog.canShow(ShareLinkContent.class)) {
                                            dialog.show(content);
                                        } else {
                                            Log.d("facee", "you cannot share :(");
                                        }
                                    }

                                }
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getContext(), "เชื่อมต่ออินเตอร์เน็ตไม่ได้", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
