package com.adapter.oishi.sendcode.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bestiize on 2/2/2559.
 */
public class QuotaSendCodeResponse {
    @SerializedName("result")
    private String result;
    @SerializedName("detail")
    private String detail;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
