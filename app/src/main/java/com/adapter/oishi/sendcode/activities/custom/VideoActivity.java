package com.adapter.oishi.sendcode.activities.custom;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.adapter.oishi.Config;
import com.adapter.oishi.R;
import com.adapter.oishi.sendcode.activities.BaseActivity;

import java.lang.reflect.InvocationTargetException;

/**
 * Created by Bestiize on 5/2/2559.
 */
public class VideoActivity extends BaseActivity {
    private WebView webView;
    private ProgressBar progressBar;

    private ImageView imageViewClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
//        VideoView myVideoView = (VideoView)findViewById(R.id.videoview);
//        myVideoView.setVideoURI(Uri.parse("rtsp://r2---sn-a5m7zu76.c.youtube.com/Ck0LENy73wIaRAnTmlo5oUgpQhMYESARFEgGUg5yZWNvbW1lbmRhdGlvbnIhAWL2kyn64K6aQtkZVJdTxRoO88HsQjpE1a8d1GxQnGDmDA==/0/0/0/video.3gp"));
//        myVideoView.setMediaController(new MediaController(this));
//        myVideoView.requestFocus();
//        myVideoView.start();
//        String videoID = "Z-qjxO_ZJKM";
        webView = (WebView) findViewById(R.id.webView);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        //webView.setWebChromeClient(mWebChromeClient);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                progressBar.setVisibility(View.VISIBLE);
                VideoActivity.this.progressBar.setProgress(0);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                VideoActivity.this.progressBar.setProgress(100);
                progressBar.setVisibility(View.GONE);
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        String url = "https://www.youtube.com/embed/" + Config.getString(getApplicationContext(), Config.youtube_sendcode);
        Log.d("youu", url);
        webView.loadUrl(url);


        imageViewClose = (ImageView) findViewById(R.id.imageViewClose);
        imageViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onPause() {
        super.onPause();
        webView.onPause();
        try {
            Class.forName("android.webkit.WebView")
                    .getMethod("onPause", (Class[]) null)
                    .invoke(webView, (Object[]) null);

        } catch (ClassNotFoundException cnfe) {

        } catch (NoSuchMethodException nsme) {

        } catch (InvocationTargetException ite) {

        } catch (IllegalAccessException iae) {

        }
    }
}
