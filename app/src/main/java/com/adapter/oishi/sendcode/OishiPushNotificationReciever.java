package com.adapter.oishi.sendcode;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.adapter.oishi.sendcode.activities.custom.MainActivity;
import com.adapter.oishi.sendcode.activities.custom.SplashActivity;
import com.adapter.oishi.sendcode.activities.custom.VoiceActivity;
import com.parse.ParsePushBroadcastReceiver;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Bestiize on 8/2/2559.
 */
public class OishiPushNotificationReciever extends ParsePushBroadcastReceiver {
    private final String TAG = "OishiPush";
    private Intent parseIntent;
    private String data;
    private String event;

    @Override
    protected void onPushOpen(Context context, Intent intent) {
        //super.onPushOpen(context, intent);

        data = intent.getStringExtra("com.parse.Data");
        try {
            JSONObject json = new JSONObject(data);
            event = json.getString("event");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            Log.d("MainActivity" , "ok");
            if (data == null) {
                Log.d("EstNotification", "Data = null");
                Intent intent1 = new Intent(context, SplashActivity.class);
                intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent1);

            } else if (event.equals("winner")) {

                Log.d("EstNotification", "Open Schedule Fragment");
                Intent intent1 = new Intent(context, MainActivity.class);
                intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent1.putExtra("page", VoiceActivity.class.getName());
                context.startActivity(intent1);
            } else {
                Log.d("EstNotification", "Data = null");
                Intent intent1 = new Intent(context, SplashActivity.class);
                intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent1);
            }
        } catch (NullPointerException e) {
            Log.d("MainActivity" , "error");
            Intent intent1 = new Intent(context, SplashActivity.class);
            intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent1);
        }
    }

    private void parsePushJson(Context context, JSONObject json) {
        try {
            //boolean isBackground = json.getBoolean("is_background");
            //JSONObject data = json.getJSONObject("");
            String alert = json.getString("alert");
            String event = json.getString("event");
            String sound = json.getString("sound");
            //Log.d(TAG, event + event);
            //showNotificationMessage(context , alert )
//            Intent resultIntent = null;
//            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
//
//
//            resultIntent = new Intent(context, MainActivity.class);
//            stackBuilder.addParentStack(MainActivity.class);
//
//            stackBuilder.addNextIntent(resultIntent);
//            PendingIntent resultPendingIntent =
//                    stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
//
//
////Customize your notification - sample code
//            NotificationCompat.Builder builder =
//                    new NotificationCompat.Builder(context)
//                            .setSmallIcon(R.mipmap.ic_logo)
//                            .setContentTitle("OISHI")
//                            .setContentText(alert);
//
//            int mNotificationId = 001;
//            NotificationManager mNotifyMgr =
//                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//            mNotifyMgr.notify(mNotificationId, builder.build());


        } catch (JSONException e) {
            Log.e(TAG, "Push message json exception: " + e.getMessage());
        }
    }
//    private void showNotificationMessage(Context context, String alert, Intent intent) {
//
//        notificationUtils = new NotificationUtils(context);
//
//        intent.putExtras(parseIntent.getExtras());
//
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//
//        notificationUtils.showNotificationMessage(title, message, intent);
//    }


}
