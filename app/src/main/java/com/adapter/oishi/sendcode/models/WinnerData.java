package com.adapter.oishi.sendcode.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bestiize on 2/2/2559.
 */
public class WinnerData {
    @SerializedName("winner")
    private String winner;
    @SerializedName("data")
    private String data;
    @SerializedName("datetext")
    private String datetext;
    @SerializedName("prize")
    private String prize;
    @SerializedName("status")
    private String status;
    @SerializedName("url")
    private String url;

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getDatetext() {
        return datetext;
    }

    public void setDatetext(String datetext) {
        this.datetext = datetext;
    }

    public String getPrize() {
        return prize;
    }

    public void setPrize(String prize) {
        this.prize = prize;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}

