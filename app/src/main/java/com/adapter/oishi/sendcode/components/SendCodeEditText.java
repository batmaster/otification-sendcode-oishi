package com.adapter.oishi.sendcode.components;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.adapter.oishi.R;
import com.adapter.oishi.sendcode.activities.BaseActivity;

/**
 * Created by Bestiize on 3/2/2559.
 */
public class SendCodeEditText extends RelativeLayout {
    private Context context;
    private EditText editText;
    private ImageView imvIcon;
    private RelativeLayout rloEdit;
    private boolean enable;

    public SendCodeEditText(Context context) {
        super(context);
        this.context = context;
        setupView();
    }

    public SendCodeEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        setupView();
    }

    public SendCodeEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        setupView();
    }

    private void setupView() {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.view_layout_sendcode_edittext, this, true);
        imvIcon = (ImageView) view.findViewById(R.id.imv_icon);
        editText = (EditText) view.findViewById(R.id.edit);
        rloEdit = (RelativeLayout) view.findViewById(R.id.rlo_edit);
        enable = true;

    }

    public EditText getEditText() {
        return editText;
    }

    public void setIconNumber(int number) {
        int resID = getResources().getIdentifier("icon_number" + (number + 1),
                "drawable", ((BaseActivity) (context)).getPackageName());
        imvIcon.setImageResource(resID);


    }

    public void enableEditText() {
        rloEdit.setBackgroundResource(R.drawable.input_normal);
        editText.setEnabled(true);
        enable = true;
        editText.setText("");


    }

    public void disableEditText() {
        rloEdit.setBackgroundResource(R.drawable.input_disable);
        editText.setEnabled(false);
        editText.setText("");
        enable = false ;

    }

    public void errorEditText() {
        rloEdit.setBackgroundResource(R.drawable.input_error);
        editText.setEnabled(true);
        enable = true;

    }

    public boolean isEnable() {
        return enable;
    }
}
