package com.adapter.oishi.sendcode.components;


import android.content.Context;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

/**
 * Created by Bestiize on 14/11/2558.
 */
public class TintableImageView extends ImageView {

    public TintableImageView(Context context) {
        super(context);
        this.setFocusableInTouchMode(true);
        setupOnTouchListener();
    }

    public TintableImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setFocusableInTouchMode(true);
        setupOnTouchListener();
    }

    public TintableImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.setFocusableInTouchMode(true);
        setupOnTouchListener();
    }

    public void setupOnTouchListener() {
        super.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    Log.d("Arm", "Cancel");
                    ImageView view = (ImageView) v;
                    view.setColorFilter(0x32000000, PorterDuff.Mode.SRC_ATOP);
                    view.invalidate();
                    return true;
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    ImageView view = (ImageView) v;
                    view.clearColorFilter();
                    view.invalidate();
                    v.performClick();

                } else if (event.getAction() == MotionEvent.ACTION_CANCEL) {
                    ImageView view = (ImageView) v;
                    view.clearColorFilter();
                    view.invalidate();
                }

                return false;
            }
        });
    }


}
