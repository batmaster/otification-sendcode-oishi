package com.adapter.oishi.sendcode.views.main;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.adapter.oishi.R;

/**
 * Created by Bestiize on 31/1/2559.
 */
public class MainDrawerListItem extends RelativeLayout {
    private Context context;
    private ImageView imageView;
    private int position = 0;
    private boolean isSelected = false;
    private int[] normalImage = new int[]{
            R.drawable.item_01,
            R.drawable.item_02,
            R.drawable.item_03,
            R.drawable.item_04,
            R.drawable.item_05,
            R.drawable.item_06,
            R.drawable.item_07,
            R.drawable.item_08,


    };

    public MainDrawerListItem(Context context) {
        super(context);
        this.context = context;
        setupView();
    }

    public void setPosition(int position) {
        this.position = position;
        this.imageView.setImageResource(normalImage[position]);
        this.imageView.setSelected(isSelected);

    }

    @Override
    public void setOnClickListener(OnClickListener l) {
        imageView.setOnClickListener(l);
    }

    public void setSelected(boolean isSelected) {

        this.isSelected = isSelected;
        this.imageView.setSelected(isSelected);


    }

    public MainDrawerListItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        setupView();
    }

    private void setupView() {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.view_list_item_drawer, this, true);
        imageView = (ImageView) view.findViewById(R.id.imv_image);

    }
}
