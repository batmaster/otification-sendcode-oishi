package com.adapter.oishi.sendcode.views.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.adapter.oishi.Config;
import com.adapter.oishi.OishiApplication;
import com.adapter.oishi.R;
import com.adapter.oishi.otification.utility.StrokeTextView;
import com.adapter.oishi.sendcode.activities.custom.MainActivity;
import com.adapter.oishi.sendcode.components.NoticeCodeEditText;
import com.adapter.oishi.sendcode.components.TintableImageView;
import com.squareup.picasso.Picasso;

/**
 * Created by Bestiize on 1/2/2559.
 */
public class NoticeWinnerDialog extends Dialog {

    private OishiApplication application;
    private Context context;
    private ImageView imageViewText;
    private StrokeTextView textViewNumber;


    private RelativeLayout topspace;

    public NoticeWinnerDialog(Context context) {
        super(context);
        this.context = context;
    }

    public NoticeWinnerDialog(Context context, int themeResId) {
        super(context, themeResId);
        this.context = context;
    }

    protected NoticeWinnerDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        View decorView = getWindow().getDecorView();
        setContentView(R.layout.view_dialog_noticewinner);
        application = (OishiApplication) context.getApplicationContext();

        final DisplayMetrics metrics = new DisplayMetrics();
        getWindow().getWindowManager().getDefaultDisplay().getMetrics(metrics);

        final int h = metrics.heightPixels;
        final int w = metrics.widthPixels;

        this.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        this.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        topspace = (RelativeLayout) findViewById(R.id.topspace);
        topspace.post(new Runnable() {
            @Override
            public void run() {
                LinearLayout.LayoutParams l = (LinearLayout.LayoutParams) topspace.getLayoutParams();
                l.height = (int) (w * 0.694);;
                topspace.setLayoutParams(l);
            }
        });

        imageViewText = (ImageView) findViewById(R.id.imageViewText);
        imageViewText.post(new Runnable() {
            @Override
            public void run() {
                LinearLayout.LayoutParams l = (LinearLayout.LayoutParams) imageViewText.getLayoutParams();
                l.height = (int) (w * 0.303);
                imageViewText.setLayoutParams(l);
            }
        });
        textViewNumber = (StrokeTextView) findViewById(R.id.textViewNumber);

        ImageView imageViewClose = (ImageView) findViewById(R.id.imageViewClose);
        imageViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                application.sendButtonStat(OishiApplication.SENDCODE_UA, "Close_Win");
                dismiss();
            }
        });
    }

    public void setDetailDialog(String type, String round, String codeNumber) {
        int typeInteger = Integer.parseInt(type);
        int roundInteger = Integer.parseInt(round);
//        if (typeInteger == 1) {
            if (roundInteger == 1) {
                imageViewText.setImageResource(R.drawable.txt_win1);
            }
            if (roundInteger == 2) {
                imageViewText.setImageResource(R.drawable.txt_win2);
            }
            if (roundInteger == 3) {
                imageViewText.setImageResource(R.drawable.txt_win3);
            }

        textViewNumber.setText("จากรหัสหมายเลข " + codeNumber);
//        }
//
//        if (typeInteger == 2) {
//            if (roundInteger == 1) {
//                imvTime.setImageResource(R.drawable.text_notice_round1);
//
//            }
//            if (roundInteger == 2) {
//                imvTime.setImageResource(R.drawable.text_notice_round2);
//
//            }
//            if (roundInteger == 3) {
//                imvTime.setImageResource(R.drawable.text_notice_round3);
//
//            }
//            if (roundInteger == 4) {
//                imvTime.setImageResource(R.drawable.text_notice_round4);
//
//            }
//            imvBg.setImageResource(R.drawable.bg_dialog_winner_100t);
//
//        }


    }
}
