package com.adapter.oishi.sendcode.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adapter.oishi.Config;
import com.adapter.oishi.OishiApplication;
import com.adapter.oishi.R;
import com.adapter.oishi.otification.activity.EmptyDialog;
import com.adapter.oishi.otification.utility.service.HTTPService;
import com.adapter.oishi.sendcode.activities.BaseActivity;
import com.adapter.oishi.sendcode.activities.custom.MainActivity;
import com.adapter.oishi.sendcode.activities.custom.VoiceActivity;
import com.adapter.oishi.sendcode.components.MobileEditText;
import com.adapter.oishi.sendcode.components.SendCodeEditText;
import com.adapter.oishi.sendcode.components.TintableImageView;
import com.adapter.oishi.sendcode.http.OishiHTTPService;
import com.adapter.oishi.sendcode.models.Code;
import com.adapter.oishi.sendcode.models.QuotaSendCodeResponse;
import com.adapter.oishi.sendcode.models.SendCodeResponse;
import com.adapter.oishi.sendcode.views.dialog.ThankyouDialog;
import com.crashlytics.android.Crashlytics;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bestiize on 31/1/2559.
 */
public class SendcodeFragment extends Fragment {
    //  private Button button;
    private String outputVoice;
    private int currentButton;
    private SendCodeEditText[] sendCodeEditText = new SendCodeEditText[10];
    private TintableImageView[] btnMic = new TintableImageView[10];
    private Code[] code = new Code[10];
    private MobileEditText mobileEditText;
    private TintableImageView btnSendcode;
    private ImageView textError;
    private ImageView textStatus;
    private OishiApplication application;
    private LinearLayout lloTouch;
    private List<Code> codes = new ArrayList<Code>();
    private List<Code> mockCodes = new ArrayList<Code>();
    private boolean checkError;
    private ThankyouDialog thankyouDialog;

    private int countQuota;
    private boolean checkNumber;
    private String currentNumber = "";
    private boolean useMic;

    private ImageView imageViewBackground;
    private RelativeLayout topspace;
    private View view;

    public static SendcodeFragment newInstance() {
        Bundle args = new Bundle();
        SendcodeFragment fragment = new SendcodeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sendcode, container, false);
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {

        application = (OishiApplication) getActivity().getApplicationContext();
        application.sendPageStat(OishiApplication.SENDCODE_UA, "WinterPro2016SendCode-Submit");

        this.view = view;

        mobileEditText = (MobileEditText) view.findViewById(R.id.et_mobile);
        textError = (ImageView) view.findViewById(R.id.imv_texterror);
        textStatus = (ImageView) view.findViewById(R.id.imv_text_status);
        lloTouch = (LinearLayout) view.findViewById(R.id.llo_touch);
        btnSendcode = (TintableImageView) view.findViewById(R.id.btn_sendcode);


        topspace = (RelativeLayout) view.findViewById(R.id.topspace);
        topspace.post(new Runnable() {
            @Override
            public void run() {
                DisplayMetrics metrics = new DisplayMetrics();
                getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);

                final int w = metrics.widthPixels;

                if (w < 600) {
                    RelativeLayout.LayoutParams rl = (RelativeLayout.LayoutParams) btnSendcode.getLayoutParams();
                    rl.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 120, metrics);
                    btnSendcode.setLayoutParams(rl);
                }

                RelativeLayout.LayoutParams l1 = (RelativeLayout.LayoutParams) topspace.getLayoutParams();
                l1.height = (int) (w * 0.972);
                topspace.setLayoutParams(l1);
            }
        });

        imageViewBackground = (ImageView) view.findViewById(R.id.imageViewBackground);
        imageViewBackground.post(new Runnable() {
            @Override
            public void run() {
                topspace.post(new Runnable() {
                    @Override
                    public void run() {
                        RelativeLayout.LayoutParams l = (RelativeLayout.LayoutParams) imageViewBackground.getLayoutParams();
                        l.height = view.findViewById(R.id.topspace).getHeight() + view.findViewById(R.id.center).getHeight();

                        imageViewBackground.setLayoutParams(l);


                        if (Config.isPassed(getContext())) {
                            Picasso.with(getContext()).load(R.drawable.bg_new_home2).noFade().fit().into(imageViewBackground);
//                    imageViewBackground.setImageResource(R.drawable.bg_new_home2);
                        } else {
                            Picasso.with(getContext()).load(R.drawable.bg_new_home1).noFade().fit().into(imageViewBackground);
                        }
                    }
                });
            }
        });


        useMic = false;
        //  bgSendCode.setImageBitmap(application.getSystemManager().getBitmap());

        for (int i = 0; i < 10; i++) {
            int resID = getResources().getIdentifier("et_sendcode_" + i,
                    "id", getActivity().getPackageName());
            int btnMicResID = getResources().getIdentifier("btn_mic_" + i,
                    "id", getActivity().getPackageName());
            sendCodeEditText[i] = (SendCodeEditText) view.findViewById(resID);
            sendCodeEditText[i].setIconNumber(i);
            btnMic[i] = (TintableImageView) view.findViewById(btnMicResID);
            final int finalI = i;
            btnMic[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    currentButton = finalI;
                    useMic = true;
                    startActivityForResult(new Intent(getActivity(), VoiceActivity.class), 123);
                    application.sendButtonStat(OishiApplication.SENDCODE_UA, "Click_Sound");
                }
            });
            code[i] = new Code();
        }

        mobileEditText.getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    Log.d("mobile", "focus");
                }
                else {
                    String mobileNo = mobileEditText.getEditText().getText().toString();
                    if (mobileNo.length() == 10) {
                        String twoCharacter = mobileNo.substring(0, 2);
                        if (twoCharacter.equals("08") || twoCharacter.equals("09") || twoCharacter.equals("06")) {
                            mobileEditText.showIconCorrect();
                            if (!currentNumber.equals(mobileEditText.getEditText().getText().toString())) {
                                ((BaseActivity) getActivity()).showProgressDialog();
                                checkQuotaSendcode(mobileNo);
                                application.getSystemManager().setPhonenumber(mobileNo);
                            }


                        } else {
                            if (((MainActivity) getActivity()).getShouldToast()) {
                                if (mobileNo.length() != 0) {
                                    Toast.makeText(getActivity(), "เบอร์โทรศัพท์มือถือไม่ถูกต้อง", Toast.LENGTH_SHORT).show();
                                }
                            }

                            currentNumber = mobileNo;
                            mobileEditText.hideIconCorrect();
                            disableAllSendCode();
                        }

                    } else {
                        if (((MainActivity) getActivity()).getShouldToast()) {
                            if (mobileNo.length() != 0) {
                                Toast.makeText(getActivity(), "เบอร์โทรศัพท์มือถือไม่ถูกต้อง", Toast.LENGTH_SHORT).show();
                            }
                        }
                        currentNumber = mobileNo;
                        mobileEditText.hideIconCorrect();
                        disableAllSendCode();
                    }
                }
            }
        });

        mobileEditText.getEditText().setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    //Clear focus here from edittext
                    mobileEditText.clearFocus();
                }
                return false;
            }
        });


        lloTouch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((BaseActivity) getActivity()).hideKeyboard(v);
                Log.d("rloTouch", "s");
                mobileEditText.getEditText().clearFocus();
            }
        });


        btnSendcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                application.sendButtonStat(OishiApplication.SENDCODE_UA, "Click_SubmitCode");

                int active = Config.getInt(getContext(), Config.sendcode_active);

                if (active == 0) {
                    Intent intent = new Intent(getContext(), EmptyDialog.class);
                    intent.putExtra("text", 3);
                    startActivity(intent);
                } else if (active == 1) {
//                    application.getOishiHttpService().statSendcode(new OishiHTTPService.OnResponseCallBack<String>() {
//                        @Override
//                        public void onResponse(boolean success, Throwable error, String data, String errorMessage) {
//                            if (success) {
//                                Log.d("statsendcode", "success");
//                            } else {
//
//                                Log.d("statsendcode", "fail");
//                            }
//                        }
//                    });
                    codes.clear();
                    checkError = false;
                    for (int i = 0; i < 10; i++) {
                        if (sendCodeEditText[i].isEnable() && !sendCodeEditText[i].getEditText().getText().toString().trim().equals("")) {
                            code[i].setPosition(i);
                            code[i].setCodeNumber(sendCodeEditText[i].getEditText().getText().toString());
                            codes.add(code[i]);
                            Log.d("position", code[i].getPosition() + " " + code[i].getCodeNumber());

                        }

                    }
                    if (codes.size() == 0) {
                        Toast.makeText(getActivity(), "กรุณากรอกรหัสอย่างน้อย 1 ช่อง", Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        ((BaseActivity) getActivity()).showProgressDialog();
                        sendCode(mobileEditText.getEditText().getText().toString(), prepareForSendCode(codes));
                    }

                } else {
                    Intent intent = new Intent(getContext(), EmptyDialog.class);
                    intent.putExtra("text", 4);
                    startActivity(intent);
                }


            }
        });
//        if (mobileEditText.getEditText().getText().toString().equals("")) {
//            disableAllSendCode();
//        }
        thankyouDialog = new ThankyouDialog(getActivity());
        disableAllSendCode();
        if (!application.getSystemManager().getPhonenumber().equals("")) {
            currentNumber = application.getSystemManager().getPhonenumber();
            mobileEditText.getEditText().setText(application.getSystemManager().getPhonenumber());
            mobileEditText.showIconCorrect();
            checkQuotaSendcode(currentNumber);
            Log.d("Sendcode", "oncreateView " + currentNumber);


        } else {
            disableAllSendCode();

        }

        ((MainActivity) getActivity()).setShouldToast(true);

        Log.d("SendCodeFragment", "oncreateView ");
    }

    @Override
    public void onResume() {
        super.onResume();
        //bgSendCode.setImageBitmap(application.getSystemManager().getBitmap());
        Log.d("SendCodeFragment", "onResume ");
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!application.getSystemManager().getPhonenumber().equals("")) {
            currentNumber = application.getSystemManager().getPhonenumber();
            mobileEditText.getEditText().setText(application.getSystemManager().getPhonenumber());
            mobileEditText.showIconCorrect();
            //checkQuotaSendcode(currentNumber);

            Log.d("SendCodeFragment", "oncreateView " + currentNumber);


        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 123 && resultCode == getActivity().RESULT_OK) {
            outputVoice = data.getStringExtra("Number").replaceAll("[^0-9]", "");
            Log.d("SendCodeFragment", "onActivityResult " + outputVoice + currentButton);
            sendCodeEditText[currentButton].getEditText().setText(outputVoice);
            //stripCode(outputVoice);

        }
    }

    public void checkQuotaSendcode(String mobileNo) {
        currentNumber = mobileNo;
        application.getOishiHttpService().checkQuota(mobileNo, new OishiHTTPService.OnResponseCallBack<QuotaSendCodeResponse>() {
            @Override
            public void onResponse(boolean success, Throwable error, QuotaSendCodeResponse data, String errorMessage) {
                if (success) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                ((BaseActivity) getActivity()).hideProgressDialog();
                            }
                            catch (NullPointerException e) {
                                e.printStackTrace();
                                Crashlytics.logException(e);
                            }
                        }
                    }, 1000);
                    Log.d("quota", "success");
                    enableSendCode(Integer.parseInt(data.getDetail()));
                    countQuota = Integer.parseInt(data.getDetail());


                } else {


                }
            }
        });


    }

    public void sendCode(final String mobileNo, String code) {
        application.getOishiHttpService().sendCode(mobileNo, code, new OishiHTTPService.OnResponseCallBack<SendCodeResponse>() {
            @Override
            public void onResponse(boolean success, Throwable error, SendCodeResponse data, String errorMessage) {
                if (success) {

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ((BaseActivity) getActivity()).hideProgressDialog();
                        }
                    }, 1000);
                    if (data.getCodeLists().size() == 0) {
                        return;

                    }
                    Log.d("sendcode", "success");
                    if (data.getCodeLists().size() > 0) {
                        for (int i = 0; i < data.getCodeLists().size(); i++) {
                            if (data.getCodeLists().get(i).getText().equals("VALID")) {
                                sendCodeEditText[mockCodes.get(i).getPosition()].disableEditText();
                                disableMicButton(mockCodes.get(i).getPosition());
                                countQuota = countQuota - 1;

                            } else {
                                sendCodeEditText[mockCodes.get(i).getPosition()].errorEditText();
                                showTextError();
                                checkError = true;

                            }

                        }

                    }

                    if (checkError) {
                        showTextError();

                    } else {
                        hideTextError();
                        thankyouDialog.show();

                        if (countQuota == 0) {
                            changeStatus(countQuota);
                            disableAllSendCode();


                        }
                    }
                } else {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ((BaseActivity) getActivity()).hideProgressDialog();
                        }
                    }, 1000);
                    Log.d("sendcode", "fail");
                }
            }
        });

    }


    public void enableMicButton(int target) {
        btnMic[target].setEnabled(true);
        btnMic[target].setImageResource(R.drawable.icon_mic);

    }

    public void disableMicButton(int target) {
        btnMic[target].setEnabled(false);
        btnMic[target].setImageResource(R.drawable.icon_mic_dis);
    }

    public void enableButtonSendcode() {
        btnSendcode.setImageResource(R.drawable.btn_sendcode);
        btnSendcode.setEnabled(true);

    }

    public void disableButtonSendcode() {
        btnSendcode.setImageResource(R.drawable.btn_sendcode_dis);
        btnSendcode.setEnabled(false);


    }

    public void showTextError() {
        textError.setVisibility(View.VISIBLE);

    }

    public void hideTextError() {
        textError.setVisibility(View.INVISIBLE);

    }

    public void changeStatus(int quota) {
        if (quota == 0) {
            textStatus.setImageResource(R.drawable.text_sendcode_tmr);
        } else {
            textStatus.setImageResource(R.drawable.text_sendcode_normal);

        }

    }

    public void enableSendCode(int quota) {
        if (quota > 0) {
            for (int i = 0; i < 10; i++) {
                if (i < quota) {
                    sendCodeEditText[i].enableEditText();
                    enableMicButton(i);
                } else {
                    sendCodeEditText[i].disableEditText();
                    disableMicButton(i);

                }
            }

            enableButtonSendcode();

        } else {
            disableAllSendCode();


        }
        changeStatus(quota);

    }

    public void disableAllSendCode() {
        for (int i = 0; i < 10; i++) {
            disableMicButton(i);
            sendCodeEditText[i].disableEditText();


        }

        disableButtonSendcode();

    }

    public String prepareForSendCode(List<Code> code) {
        String codeForSend = "";
        mockCodes.clear();
        for (int i = 0; i < code.size(); i++) {
            if (!codeForSend.contains(code.get(i).getCodeNumber())) {

                if (i == 0) {

                    codeForSend = codeForSend + code.get(i).getCodeNumber();

                } else {
                    codeForSend = codeForSend + "|" + code.get(i).getCodeNumber();

                }
                mockCodes.add(code.get(i));

            } else {
                sendCodeEditText[code.get(i).getPosition()].errorEditText();
                showTextError();
                checkError = true;

            }
        }
        Log.d("prepare", codeForSend);


        return codeForSend;

    }

}
