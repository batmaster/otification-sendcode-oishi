package com.adapter.oishi.sendcode.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bestiize on 2/2/2559.
 */
public class WinnerMobileResponse {
    @SerializedName("result")
    private String result;
    @SerializedName("detail")
    private String detail;
    @SerializedName("type")
    private String type;
    @SerializedName("mobile")
    private String mobile;
    @SerializedName("code")
    private String code;
    @SerializedName("winnerdate")
    private String winnerDate;
    @SerializedName("winnerdatetext")
    private String winnerDateText;
    @SerializedName("round")
    private String round;
    @SerializedName("allround")
    private String allround;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getWinnerDate() {
        return winnerDate;
    }

    public void setWinnerDate(String winnerDate) {
        this.winnerDate = winnerDate;
    }

    public String getWinnerDateText() {
        return winnerDateText;
    }

    public void setWinnerDateText(String winnerDateText) {
        this.winnerDateText = winnerDateText;
    }

    public String getRound() {
        return round;
    }

    public void setRound(String round) {
        this.round = round;
    }

    public String getAllround() {
        return allround;
    }

    public void setAllround(String allround) {
        this.allround = allround;
    }
}

