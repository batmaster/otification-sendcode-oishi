package com.adapter.oishi.sendcode.models;

/**
 * Created by Bestiize on 4/2/2559.
 */
public class Code {
    private String codeNumber;
    private int position ;

    public String getCodeNumber() {
        return codeNumber;
    }

    public void setCodeNumber(String codeNumber) {
        this.codeNumber = codeNumber;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
