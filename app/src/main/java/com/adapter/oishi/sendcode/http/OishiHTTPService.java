package com.adapter.oishi.sendcode.http;

import android.content.Context;

import com.adapter.oishi.sendcode.http.api.OishiAPI;
import com.adapter.oishi.sendcode.models.AlertDataResponse;
import com.adapter.oishi.sendcode.models.QuotaSendCodeResponse;
import com.adapter.oishi.sendcode.models.SendCodeResponse;
import com.adapter.oishi.sendcode.models.Sync;
import com.adapter.oishi.sendcode.models.WinnerMobileResponse;
import com.adapter.oishi.sendcode.models.WinnerResponse;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by Bestiize on 30/1/2559.
 */
public class OishiHTTPService {
    public static final String OISHI_BASE_URL = "https://www.oishidrink.com";
    public static final String OISHI_NEW_URL = "http://oishiapiandroid.digitalcampaignsite.com";
    private Retrofit retrofit;
    private Retrofit retrofitNew;
    private Context context;
    private OishiAPI oishiAPI;
    private OishiAPI oishiAPInew;

    public interface OnResponseCallBack<T> {
        void onResponse(boolean success, Throwable error, T data, String errorMessage);
    }


    public OishiHTTPService(Context context) {
        this.context = context;

        retrofit = new Retrofit.Builder()
                .baseUrl(OISHI_BASE_URL)

                .addConverterFactory(GsonConverterFactory.create())
                .build();
        retrofitNew = new Retrofit.Builder()
                .baseUrl(OISHI_NEW_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        this.oishiAPI = retrofit.create(OishiAPI.class);
        this.oishiAPInew = retrofitNew.create(OishiAPI.class);
    }

    public void sendCode(String mobileNo, String code, final OnResponseCallBack<SendCodeResponse> callBack) {
        String realCode = "";
        try {
            realCode = URLEncoder.encode(code, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        Call<SendCodeResponse> sendCode = this.oishiAPI.sendCode(mobileNo, realCode);
        sendCode.enqueue(new Callback<SendCodeResponse>() {
            @Override
            public void onResponse(Response<SendCodeResponse> response, Retrofit retrofit) {
                callBack.onResponse(response.isSuccess(), null, response.body(), response.message());
            }

            @Override
            public void onFailure(Throwable t) {
                callBack.onResponse(false, t, null, "Not found");
            }
        });

    }

    public void checkQuota(String mobileNo, final OnResponseCallBack<QuotaSendCodeResponse> callBack) {
        Call<QuotaSendCodeResponse> checkQuota = this.oishiAPI.checkQuota(mobileNo);
        checkQuota.enqueue(new Callback<QuotaSendCodeResponse>() {
            @Override
            public void onResponse(Response<QuotaSendCodeResponse> response, Retrofit retrofit) {
                callBack.onResponse(response.isSuccess(), null, response.body(), response.message());
            }

            @Override
            public void onFailure(Throwable t) {
                callBack.onResponse(false, t, null, "Not found");
            }
        });

    }

    public void checkWinner(final OnResponseCallBack<WinnerResponse> callBack) {
        Call<WinnerResponse> checkWinner = this.oishiAPInew.checkWinner();
        checkWinner.enqueue(new Callback<WinnerResponse>() {
            @Override
            public void onResponse(Response<WinnerResponse> response, Retrofit retrofit) {
                callBack.onResponse(response.isSuccess(), null, response.body(), response.message());
            }

            @Override
            public void onFailure(Throwable t) {
                callBack.onResponse(false, t, null, "Not found");
            }
        });


    }

    public void checkWinnerMobile(String mobileNo, final OnResponseCallBack<WinnerMobileResponse> callBack) {
        Call<WinnerMobileResponse> checkWinnerMobile = this.oishiAPInew.checkWinnerMobile(mobileNo);
        checkWinnerMobile.enqueue(new Callback<WinnerMobileResponse>() {
            @Override
            public void onResponse(Response<WinnerMobileResponse> response, Retrofit retrofit) {
                callBack.onResponse(response.isSuccess(), null, response.body(), response.message());
            }

            @Override
            public void onFailure(Throwable t) {
                callBack.onResponse(false, t, null, "Not found");
            }
        });


    }

    public void alertData(final OnResponseCallBack<AlertDataResponse> callBack) {
        Call<AlertDataResponse> alertData = this.oishiAPInew.alertData();
        alertData.enqueue(new Callback<AlertDataResponse>() {
            @Override
            public void onResponse(Response<AlertDataResponse> response, Retrofit retrofit) {
                callBack.onResponse(response.isSuccess(), null, response.body(), response.message());
            }

            @Override
            public void onFailure(Throwable t) {
                callBack.onResponse(false, t, null, "Not found");
            }
        });


    }

    public void statStartApp(final OnResponseCallBack<String> callBack) {
        Call<String> statStartApp = this.oishiAPI.statLog("oishipromo", "android", "openapp");
        statStartApp.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Response<String> response, Retrofit retrofit) {
                callBack.onResponse(response.isSuccess(), null, response.body(), response.message());
            }

            @Override
            public void onFailure(Throwable t) {
                callBack.onResponse(false, t, null, "Not found");
            }
        });


    }

    public void statSendcode(final OnResponseCallBack<String> callBack) {
        Call<String> statStartApp = this.oishiAPI.statLog("oishipromo", "android", "startsendcode");
        statStartApp.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Response<String> response, Retrofit retrofit) {
                callBack.onResponse(response.isSuccess(), null, response.body(), response.message());
            }

            @Override
            public void onFailure(Throwable t) {
                callBack.onResponse(false, t, null, "Not found");
            }
        });


    }

    public void syncData(final OnResponseCallBack<Sync> callBack) {
        Call<Sync> sync = this.oishiAPI.syncData();
        sync.enqueue(new Callback<Sync>() {
            @Override
            public void onResponse(Response<Sync> response, Retrofit retrofit) {
                callBack.onResponse(response.isSuccess(), null, response.body(), response.message());
            }

            @Override
            public void onFailure(Throwable t) {
                callBack.onResponse(false, t, null, "Not found");
            }
        });


    }


}
