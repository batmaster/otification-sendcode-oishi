package com.adapter.oishi.sendcode.activities.custom;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.adapter.oishi.Config;
import com.adapter.oishi.OishiApplication;
import com.adapter.oishi.R;
import com.adapter.oishi.otification.utility.service.HTTPService;
import com.adapter.oishi.sendcode.activities.BaseActivity;
import com.adapter.oishi.sendcode.components.TintableImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;

import java.util.ArrayList;

/**
 * Created by Bestiize on 1/2/2559.
 */
public class VoiceActivity extends BaseActivity implements RecognitionListener {

    private OishiApplication application;

    private TintableImageView btnMic;
    private TintableImageView btnCancle;
    private ImageView imvWave;
    private TintableImageView btnSubmit;
    private RelativeLayout rloMic;
    private RelativeLayout rloRecord;
    private SpeechRecognizer speech = null;
    private Intent recognizerIntent;
    private boolean mIslistening;
    final private int REQUEST_CODE_ASK_PERMISSIONS = 321;

    private ImageView imageViewBackground;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voice);

        application = (OishiApplication) getApplication();
        application.sendPageStat(OishiApplication.SENDCODE_UA, "WinterPro2016SendCode-Sound");

        btnMic = (TintableImageView) findViewById(R.id.btn_mic);
        btnCancle = (TintableImageView) findViewById(R.id.btn_cancle);
        btnSubmit = (TintableImageView) findViewById(R.id.btn_submit);
        rloMic = (RelativeLayout) findViewById(R.id.rlo_mic);
        rloRecord = (RelativeLayout) findViewById(R.id.rlo_record);
        mIslistening = false;
        btnMic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                application.sendButtonStat(OishiApplication.SENDCODE_UA, "Click_StartSound");
                application.getHttpService().sendStat(HTTPService.VOICE_START);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    int hasRecordAudioPermission = checkSelfPermission(Manifest.permission.RECORD_AUDIO);
                    if (hasRecordAudioPermission != PackageManager.PERMISSION_GRANTED) {
                        if (!shouldShowRequestPermissionRationale(Manifest.permission.RECORD_AUDIO)) {
                            showMessageOKCancel("You need to allow access to Microphone",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO},
                                                        REQUEST_CODE_ASK_PERMISSIONS);
                                            }
                                        }
                                    });
                            return;
                        }
                        requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO},
                                REQUEST_CODE_ASK_PERMISSIONS);
                        return;
                    }
                }

                if (!mIslistening) {
                    rloMic.setVisibility(View.GONE);
                    rloRecord.setVisibility(View.VISIBLE);
                    speech.startListening(recognizerIntent);
                }

            }
        });

        btnCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                application.sendButtonStat(OishiApplication.SENDCODE_UA, "Click_CancelSound");
                if (rloRecord.getVisibility() == View.VISIBLE) {
                    application.getHttpService().sendStat(HTTPService.VOICE_CANCEL);
                }
                speech.stopListening();
                finish();

            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                application.sendButtonStat(OishiApplication.SENDCODE_UA, "Click_SubmitSound");
                speech.stopListening();


            }
        });
        imvWave = (ImageView) findViewById(R.id.wave);
        Glide.with(this)
                .load(R.drawable.wave_gif)
                .asGif()
                .crossFade()
                .into(imvWave);

        imageViewBackground = (ImageView) findViewById(R.id.imageViewBackground);
        if (Config.isPassed(getApplicationContext())) {
            imageViewBackground.setImageResource(R.drawable.bg_voice);
        }


        recognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "th");
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, getApplication().getPackageName());
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        recognizerIntent.putExtra("android.speech.extra.DICTATION_MODE", true);
        // recognizerIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1);
        // recognizerIntent.putExtra(RecognizerIntent.EXTRA_PARTIAL_RESULTS, true);
        // recognizerIntent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_COMPLETE_SILENCE_LENGTH_MILLIS , 20000);
//        Log.d("recc", SpeechRecognizer.isRecognitionAvailable(getApplicationContext()) + "");

        speech = SpeechRecognizer.createSpeechRecognizer(this);
        speech.setRecognitionListener(this);
        GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(imvWave);
        Glide.with(this).load(R.drawable.wave_gif).into(imageViewTarget);
        Log.d("VoiceActivity", "onCreate");
    }

    @Override
    protected void onDestroy() {

        if (speech != null) {
            try {
                speech.destroy();
            }
            catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
            //Log.i(LOG_TAG, "destroy");
        }
        super.onDestroy();
        Log.d("VoiceActivity", "onDestroy");
    }

    @Override
    public void onReadyForSpeech(Bundle params) {
        Log.d("VoiceActivity", "onReadyForSpeech");

    }

    @Override
    public void onBeginningOfSpeech() {
        Log.d("VoiceActivity", "onBeginningOfSpeech");
    }

    @Override
    public void onRmsChanged(float rmsdB) {
        // Log.d("VoiceActivity" , "onRmsChanged");
    }

    @Override
    public void onBufferReceived(byte[] buffer) {
        Log.d("VoiceActivity", "onBufferReceived");
    }

    @Override
    public void onEndOfSpeech() {

        Log.d("VoiceActivity", "endofSpeech");

    }

    @Override
    public void onError(int error) {
        String errorMessage = getErrorText(error);
        Log.d("VoiceActivity", errorMessage);
        if (SpeechRecognizer.ERROR_NO_MATCH == error) {
            speech.destroy();
            startListen();
            return;

        }

        rloMic.setVisibility(View.VISIBLE);
        rloRecord.setVisibility(View.GONE);
        Toast.makeText(this, "กรุณาพูดใหม่อีกครั้ง", Toast.LENGTH_SHORT).show();

        //speech.startListening(recognizerIntent);


    }

    @Override
    public void onResults(Bundle results) {
        ArrayList<String> matches = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
//        String text = "";
//        for (String result : matches)
//            text += result + "\n";

        Log.d("VoiceActivity", "Result ");
        // speech.stopListening();
        Intent intent = getIntent();
        intent.putExtra("Number", matches.get(0));
        setResult(RESULT_OK, intent);
        application.getHttpService().sendStat(HTTPService.VOICE_END);
        finish();


    }

    @Override
    public void onPartialResults(Bundle partialResults) {
        Log.d("VoiceActivity", "onPartialResults");

    }

    @Override
    public void onEvent(int eventType, Bundle params) {
        Log.d("VoiceActivity", "onEvent");
    }

    public static String getErrorText(int errorCode) {
        String message;
        switch (errorCode) {
            case SpeechRecognizer.ERROR_AUDIO:
                message = "Audio recording error";
                break;
            case SpeechRecognizer.ERROR_CLIENT:
                message = "Client side error";
                break;
            case SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS:
                message = "Insufficient permissions";
                break;
            case SpeechRecognizer.ERROR_NETWORK:
                message = "Network error";
                break;
            case SpeechRecognizer.ERROR_NETWORK_TIMEOUT:
                message = "Network timeout";
                break;
            case SpeechRecognizer.ERROR_NO_MATCH:
                message = "No match";
                break;
            case SpeechRecognizer.ERROR_RECOGNIZER_BUSY:
                message = "RecognitionService busy";
                break;
            case SpeechRecognizer.ERROR_SERVER:
                message = "error from server";
                break;
            case SpeechRecognizer.ERROR_SPEECH_TIMEOUT:
                message = "No speech input";
                break;
            default:
                message = "Didn't understand, please try again.";
                break;
        }
        return message;
    }

    public void startListen() {
        speech = SpeechRecognizer.createSpeechRecognizer(this);
        speech.setRecognitionListener(this);
        speech.startListening(recognizerIntent);

    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(VoiceActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }
}
