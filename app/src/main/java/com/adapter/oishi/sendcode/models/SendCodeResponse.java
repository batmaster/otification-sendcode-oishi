package com.adapter.oishi.sendcode.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Bestiize on 2/2/2559.
 */
public class SendCodeResponse {
    @SerializedName("result")
    private String result;

    @SerializedName("codelist")
    private List<CodeList> codeLists;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public List<CodeList> getCodeLists() {
        return codeLists;
    }

    public void setCodeLists(List<CodeList> codeLists) {
        this.codeLists = codeLists;
    }
}
