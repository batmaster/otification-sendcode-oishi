package com.adapter.oishi.sendcode.managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;

import com.adapter.oishi.OishiApplication;
import com.adapter.oishi.sendcode.models.AlertDataResponse;
import com.adapter.oishi.sendcode.models.Sync;

/**
 * Created by Bestiize on 3/2/2559.
 */
public class SystemManager {
    private Context context;
    private OishiApplication application;
    private AlertDataResponse alertDataResponse;
    private Bitmap bitmap;
    private SharedPreferences sharedPreferences;
    private Sync syncData;
    private String syncDataString;
    public SystemManager(Context context) {
        this.context = context;
        application = (OishiApplication) context;
        sharedPreferences = application.getSharedPreferences("system", Context.MODE_PRIVATE);
        syncData = new Sync();

    }

    public String getSyncDataString() {
        return syncDataString;
    }

    public void setSyncDataString(String syncDataString) {
        this.syncDataString = syncDataString;
    }

    public Sync getSyncData() {
        return syncData;
    }

    public void setSyncData(Sync syncData) {
        this.syncData = syncData;
    }

    public String getPhonenumber() {

        return sharedPreferences.getString("phonenumber", "");

    }

    public void setPhonenumber(String phonenumber) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("phonenumber", phonenumber);
        editor.commit();

    }

    public void setSeenAlert(boolean seenAlert) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("seenalert", seenAlert);
        editor.commit();


    }

    public boolean getSeenAlert() {

        return sharedPreferences.getBoolean("seenalert", false);
    }

    public void setCurrentAlert(int currentAlert) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("currentalert", currentAlert);
        editor.commit();

    }

    public int getCurrentAlert() {
        return sharedPreferences.getInt("currentalert", 0);

    }

    public AlertDataResponse getAlertDataResponse() {
        return alertDataResponse;
    }

    public void setAlertDataResponse(AlertDataResponse alertDataResponse) {
        this.alertDataResponse = alertDataResponse;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }
}
