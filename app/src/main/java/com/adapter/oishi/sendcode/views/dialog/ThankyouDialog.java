package com.adapter.oishi.sendcode.views.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.adapter.oishi.Config;
import com.adapter.oishi.OishiApplication;
import com.adapter.oishi.R;
import com.adapter.oishi.sendcode.activities.BaseActivity;
import com.adapter.oishi.sendcode.activities.custom.MainActivity;
import com.adapter.oishi.sendcode.components.TintableImageView;
import com.adapter.oishi.sendcode.fragments.VoteFragment;
import com.squareup.picasso.Picasso;

/**
 * Created by Bestiize on 1/2/2559.
 */
public class ThankyouDialog extends Dialog {

    private OishiApplication application;
    private Context context;

    private RelativeLayout topspace;
    private ImageView imageViewTxtThankyou;

    public ThankyouDialog(Context context) {
        super(context);
        this.context = context;
    }

    public ThankyouDialog(Context context, int themeResId) {
        super(context, themeResId);
        this.context = context;
    }

    protected ThankyouDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        View decorView = getWindow().getDecorView();
        setContentView(R.layout.view_dialog_thankyou);
        application = (OishiApplication) context.getApplicationContext();

        this.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        this.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        ImageView imageViewBackground = (ImageView) findViewById(R.id.imageViewBackground);
        if (Config.isPassed(getContext())) {
            imageViewBackground.setImageResource(R.drawable.bg_thankyou2);
        }

        final DisplayMetrics metrics = new DisplayMetrics();
        getWindow().getWindowManager().getDefaultDisplay().getMetrics(metrics);

        final int h = metrics.heightPixels;
        final int w = metrics.widthPixels;

        float widthDpi = metrics.xdpi;
        float heightDpi = metrics.ydpi;

        float widthInches = w / widthDpi;
        float heightInches = h / heightDpi;

        final double diagonalInches = Math.sqrt((widthInches * widthInches) + (heightInches * heightInches));

        topspace = (RelativeLayout) findViewById(R.id.topspace);
        topspace.post(new Runnable() {
            @Override
            public void run() {
                LinearLayout.LayoutParams l = (LinearLayout.LayoutParams) topspace.getLayoutParams();
                l.height = (int) (w * 0.676);
                topspace.setLayoutParams(l);
            }
        });

        imageViewTxtThankyou = (ImageView) findViewById(R.id.imageViewTxtThankyou);
        imageViewTxtThankyou.post(new Runnable() {
            @Override
            public void run() {
                LinearLayout.LayoutParams l = (LinearLayout.LayoutParams) imageViewTxtThankyou.getLayoutParams();
                l.height = (int) (w * 0.356);

                if (diagonalInches >= 7) {
                    l.height = (int) (0.7 * l.height);
                }
                imageViewTxtThankyou.setLayoutParams(l);
            }
        });


        final ImageView imageViewText = (ImageView) findViewById(R.id.imageViewText);
        imageViewText.post(new Runnable() {
            @Override
            public void run() {
                LinearLayout.LayoutParams l = (LinearLayout.LayoutParams) imageViewText.getLayoutParams();
                l.height = (int) (w * 0.356);

                if (diagonalInches >= 7) {
                    l.height = (int) (0.48 * l.height);
                }
                imageViewText.setLayoutParams(l);
            }
        });

        final ImageView imageViewButton = (ImageView) findViewById(R.id.imageViewButton);
        imageViewButton.post(new Runnable() {
            @Override
            public void run() {
                LinearLayout.LayoutParams l = (LinearLayout.LayoutParams) imageViewButton.getLayoutParams();
                l.height = (int) (w * 0.178);

                l.height = (int) (0.7 * l.height);
                imageViewButton.setLayoutParams(l);
            }
        });

        imageViewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                application.sendButtonStat(OishiApplication.SENDCODE_UA, "Close_Thank");

                ((MainActivity) context).startFragment(VoteFragment.class.getName());
                dismiss();
            }
        });
        if (!Config.isOpenVote(getContext())) {
            imageViewText.setVisibility(View.INVISIBLE);
            imageViewButton.setVisibility(View.INVISIBLE);
        }

        ImageView imageViewClose = (ImageView) findViewById(R.id.imageViewClose);
        imageViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                application.sendButtonStat(OishiApplication.SENDCODE_UA, "Btn_Vote");

                dismiss();
            }
        });

    }


}
