package com.adapter.oishi.sendcode.activities.custom;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.adapter.oishi.Config;
import com.adapter.oishi.OishiApplication;
import com.adapter.oishi.R;
import com.adapter.oishi.activity.NewHomeActivity;
import com.adapter.oishi.otification.activity.HomeActivity;
import com.adapter.oishi.otification.utility.SharePref;
import com.adapter.oishi.sendcode.activities.BaseActivity;
import com.adapter.oishi.sendcode.components.TintableImageView;
import com.adapter.oishi.sendcode.fragments.HowToSendFragment;
import com.adapter.oishi.sendcode.fragments.NoticeWinnerFragment;
import com.adapter.oishi.sendcode.fragments.SendcodeFragment;
import com.adapter.oishi.sendcode.fragments.SpecialRewardFragment;
import com.adapter.oishi.sendcode.fragments.TermConditionsFragment;
import com.adapter.oishi.sendcode.fragments.VoteFragment;
import com.adapter.oishi.sendcode.http.OishiHTTPService;
import com.adapter.oishi.sendcode.models.AlertDataResponse;
import com.adapter.oishi.sendcode.models.Sync;

import java.util.List;

/**
 * Created by Bestiize on 30/1/2559.
 */
public class MainActivity extends BaseActivity {
    private TintableImageView imvDrawerToggle;
    private String currentFragmentTag;
    private Fragment[] fragments;
    private TintableImageView iconNotification;

    private DrawerLayout drawerLayout;
    private ImageView imageViewClose;
    private ImageView imageViewMenu1;
    private ImageView imageViewMenu2;
    private ImageView imageViewMenu3;
    private ImageView imageViewMenu4;
    private ImageView imageViewMenu5;
    private ImageView imageViewMenu6;
    private ImageView imageViewMenu7;
    private ImageView imageViewMenuX;
    private ImageView imageViewMenu8;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent intent = getIntent();
        String page = intent.getStringExtra("page");

        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        drawerLayout.setDrawerListener(new DrawerLayout.SimpleDrawerListener() {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

                setShouldToast(false);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);

                setShouldToast(true);
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                super.onDrawerStateChanged(newState);

                imageViewMenu1.setImageResource(R.drawable.txt_sendcode_menu1);
                imageViewMenu2.setImageResource(R.drawable.txt_sendcode_menu2);
                imageViewMenu3.setImageResource(R.drawable.txt_sendcode_menu3);
                imageViewMenu4.setImageResource(R.drawable.txt_sendcode_menu4);
                imageViewMenu5.setImageResource(R.drawable.txt_sendcode_menu5);
                imageViewMenu6.setImageResource(R.drawable.txt_sendcode_menu6);
                imageViewMenu7.setImageResource(R.drawable.txt_sendcode_menu7);
                imageViewMenuX.setImageResource(R.drawable.txt_sendcode_menux);
                imageViewMenu8.setImageResource(R.drawable.txt_sendcode_menu8);
                String f = getSupportFragmentManager().findFragmentById(R.id.rlo_container).getClass().getName();

                if (f.equals(fragments[0].getClass().getName())) {
                    imageViewMenu1.setImageResource(R.drawable.txt_sendcode_menu1_active);
                }
                else if (f.equals(fragments[1].getClass().getName())) {
                    imageViewMenu3.setImageResource(R.drawable.txt_sendcode_menu3_active);
                }
                else if (f.equals(fragments[2].getClass().getName())) {
                    imageViewMenu6.setImageResource(R.drawable.txt_sendcode_menu6_active);
                }
                else if (f.equals(fragments[3].getClass().getName())) {
                    imageViewMenu4.setImageResource(R.drawable.txt_sendcode_menu4_active);
                }
                else if (f.equals(fragments[4].getClass().getName())) {
                    imageViewMenu2.setImageResource(R.drawable.txt_sendcode_menu2_active);
                }
                else if (f.equals(fragments[5].getClass().getName())) {
                    imageViewMenuX.setImageResource(R.drawable.txt_sendcode_menux_active);
                }
            }

        });

        View drawer = (View) findViewById(R.id.drawer);
        drawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        imvDrawerToggle = (TintableImageView) findViewById(R.id.imv_toggle_drawer);
        imvDrawerToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                application.sendButtonStat(OishiApplication.SENDCODE_UA, "BT_menu");
                if (!drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.openDrawer(GravityCompat.START);
                    setShouldToast(false);
                } else {
                    drawerLayout.closeDrawers();
                    setShouldToast(true);
                }

                if (getCurrentFocus() != null) {
                    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    getCurrentFocus().clearFocus();
                }
            }
        });

        imageViewMenu1 = (ImageView) drawer.findViewById(R.id.imageViewMenu1);
        imageViewMenu1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                application.sendButtonStat(OishiApplication.SENDCODE_UA, "Menu_sendcode");
                drawerLayout.closeDrawers();

                if (currentFragmentTag.equals(SendcodeFragment.class.getName())) {
                    return;
                }
                startFragment(fragments[0].getClass().getName());
            }
        });

        imageViewMenu2 = (ImageView) drawer.findViewById(R.id.imageViewMenu2);
        imageViewMenu2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                application.sendButtonStat(OishiApplication.SENDCODE_UA, "Menu_Prize");
                drawerLayout.closeDrawers();

                startFragment(fragments[4].getClass().getName());
            }
        });

        imageViewMenu3 = (ImageView) drawer.findViewById(R.id.imageViewMenu3);
        imageViewMenu3.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                application.sendButtonStat(OishiApplication.SENDCODE_UA, "Menu_How");
                drawerLayout.closeDrawers();

                startFragment(fragments[1].getClass().getName());
            }
        });

        imageViewMenu4 = (ImageView) drawer.findViewById(R.id.imageViewMenu4);
        imageViewMenu4.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                application.sendButtonStat(OishiApplication.SENDCODE_UA, "Menu_Rule");
                drawerLayout.closeDrawers();

                startFragment(fragments[3].getClass().getName());
            }
        });

        imageViewMenu5 = (ImageView) drawer.findViewById(R.id.imageViewMenu5);
        imageViewMenu5.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                application.sendButtonStat(OishiApplication.SENDCODE_UA, "Menu_TVC");
                drawerLayout.closeDrawers();

                startActivity(new Intent(MainActivity.this, VideoActivity.class));
            }
        });

        imageViewMenu6 = (ImageView) drawer.findViewById(R.id.imageViewMenu6);
        imageViewMenu6.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawers();
                application.sendButtonStat(OishiApplication.SENDCODE_UA, "Menu_Announce");
                startFragment(fragments[2].getClass().getName());
                application.getSystemManager().setSeenAlert(true);

                SharePref.setSeenNoti(getApplicationContext(), application.getSystemManager().getCurrentAlert());

                checkIconNotification();
            }
        });

        imageViewMenu7 = (ImageView) drawer.findViewById(R.id.imageViewMenu7);
        imageViewMenu7.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                application.sendButtonStat(OishiApplication.SENDCODE_UA, "Menu_Share");
                drawerLayout.closeDrawers();

                String urlToShare = Config.getString(getApplicationContext(), Config.sendcode_share_url); //application.getSystemManager().getSyncData().getShrareUrl();
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
//                intent.putExtra(Intent.EXTRA_SUBJECT, application.getSystemManager().getSyncData().getShareTitle());
                intent.putExtra(Intent.EXTRA_SUBJECT, Config.getString(getApplicationContext(), Config.sendcode_share_title));
                intent.putExtra(Intent.EXTRA_TEXT, urlToShare);
                boolean facebookAppFound = false;
                List<ResolveInfo> matches = getPackageManager().queryIntentActivities(intent, 0);
                for (ResolveInfo info : matches) {
                    if (info.activityInfo.packageName.toLowerCase().startsWith("com.facebook.katana")) {
                        intent.setPackage(info.activityInfo.packageName);
                        facebookAppFound = true;
                        break;
                    }
                }

                if (!facebookAppFound) {
                    String sharerUrl = "https://www.facebook.com/sharer/sharer.php?u=" + urlToShare;
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse(sharerUrl));
                }
                startActivity(intent);
            }
        });


        imageViewMenuX = (ImageView) findViewById(R.id.imageViewMenuX);
        imageViewMenuX.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                application.sendButtonStat(OishiApplication.SENDCODE_UA, "Menu_Vote");
                drawerLayout.closeDrawers();

                startFragment(fragments[5].getClass().getName());
            }
        });
        if (!Config.isOpenVote(getApplicationContext())) {
            imageViewMenuX.setVisibility(View.GONE);
            findViewById(R.id.emptyX).setVisibility(View.GONE);
        }

        imageViewMenu8 = (ImageView) drawer.findViewById(R.id.imageViewMenu8);
        imageViewMenu8.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                application.sendButtonStat(OishiApplication.SENDCODE_UA, "Menu_Home");
                drawerLayout.closeDrawers();

                if (Config.getInt(getApplicationContext(), Config.oti_show_at_home) == 0) {
//                    startFragment(fragments[0].getClass().getName());
//                    checkIconNotification();
                    startActivity(new Intent(getApplicationContext(), LandingActivity.class));
                    finish();
                }
                else {
                    startActivity(new Intent(getApplicationContext(), NewHomeActivity.class));
                    finish();
                }
            }
        });

        imageViewClose = (ImageView) drawer.findViewById(R.id.imageViewClose);
        imageViewClose.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawers();
            }
        });

        fragments = new Fragment[]{
                SendcodeFragment.newInstance(),
                HowToSendFragment.newInstance(),
                NoticeWinnerFragment.newInstance(),
                TermConditionsFragment.newInstance(),
                SpecialRewardFragment.newInstance(),
                VoteFragment.newInstance()
        };


        iconNotification = (TintableImageView) findViewById(R.id.icon_noti);
        iconNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                application.sendButtonStat(OishiApplication.SENDCODE_UA, "BT_Announce");

                startFragment(fragments[2].getClass().getName());
                application.getSystemManager().setSeenAlert(true);
                SharePref.setSeenNoti(getApplicationContext(), application.getSystemManager().getCurrentAlert());

                checkIconNotification();
            }
        });


        if (page != null) {
            //startFragment(fragments[0].getClass().getName());
            startFragment(fragments[2].getClass().getName());
            Log.d("MainActivity" , "page !=null");
            application.getOishiHttpService().alertData(new OishiHTTPService.OnResponseCallBack<AlertDataResponse>() {
                @Override
                public void onResponse(boolean success, Throwable error, AlertDataResponse data, String errorMessage) {
                    if(success){
                        application.getSystemManager().setAlertDataResponse(data);
                        application.getSystemManager().setCurrentAlert(Integer.parseInt(data.getDetail()));
                        application.getSystemManager().setSeenAlert(true);
                        checkIconNotification();

                        Log.d("MainActivity" , "alert success");
                    }else{

                        Log.d("MainActivity" , "alert fail");
                    }
                }
            });

        } else {
            startFragment(fragments[0].getClass().getName());

            application.getOishiHttpService().alertData(new OishiHTTPService.OnResponseCallBack<AlertDataResponse>() {
                @Override
                public void onResponse(boolean success, Throwable error, AlertDataResponse data, String errorMessage) {
                    if (success) {
                        application.getSystemManager().setCurrentAlert(Integer.parseInt(data.getDetail()));
                        checkIconNotification();
                    }
                }
            });

        }

        application.getOishiHttpService().syncData(new OishiHTTPService.OnResponseCallBack<Sync>() {
            @Override
            public void onResponse(boolean success, Throwable error, Sync data, String errorMessage) {
                if(success){
                    application.getSystemManager().setSyncData(data);
                    Log.d("MainActivity", "sync success");
                }else{
                    Log.d("MainActivity" , "sync fail");

                }
            }
        });

    }

    protected boolean isNavDrawerOpen() {
        return drawerLayout != null && drawerLayout.isDrawerOpen(GravityCompat.START);
    }

    protected void closeNavDrawer() {
        if (drawerLayout != null) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    public void checkIconNotification() {
//        int detailNoti = Integer.parseInt(application.getSystemManager().getAlertDataResponse().getDetail());
        int detailNoti = application.getSystemManager().getCurrentAlert();
        int seenNoti = SharePref.getSeenNoti(getApplicationContext());

        if (detailNoti == 0) {
            iconNotification.setImageResource(R.drawable.icon_notification);

        }
        else if (detailNoti == 1) {
            if (seenNoti >= 1) {
                iconNotification.setImageResource(R.drawable.icon_notification1_seen);
            } else {
                iconNotification.setImageResource(R.drawable.icon_notification1);

            }

        }
        else if (detailNoti == 2) {
            if (seenNoti >= 2) {
                iconNotification.setImageResource(R.drawable.icon_notification2_seen);
            } else {
                iconNotification.setImageResource(R.drawable.icon_notification2);

            }

        }
        else if (detailNoti == 3) {
            if (seenNoti >= 3) {
                iconNotification.setImageResource(R.drawable.icon_notification3_seen);
            } else {
                iconNotification.setImageResource(R.drawable.icon_notification3);

            }

        }


    }

    @Override
    public void onBackPressed() {
        if (isNavDrawerOpen()) {
            closeNavDrawer();
        } else {
            int count = getSupportFragmentManager().getBackStackEntryCount();
            if (count == 1) {
                if (Config.getInt(getApplicationContext(), Config.oti_show_at_home) == 0) {
//                    finish();
                    startActivity(new Intent(getApplicationContext(), LandingActivity.class));
                    finish();
                }
                else {
                    startActivity(new Intent(getApplicationContext(), NewHomeActivity.class));
                    finish();
                }
            } else {
                getSupportFragmentManager().popBackStack();
                FragmentManager.BackStackEntry backEntry = getSupportFragmentManager().getBackStackEntryAt(this.getSupportFragmentManager().getBackStackEntryCount() - 2);
                String str = backEntry.getName();
                Fragment fragment = getSupportFragmentManager().findFragmentByTag(str);
                currentFragmentTag = fragment.getClass().getName();
                Log.d("MainActivity", "getBackStackEntryCount() " + currentFragmentTag);
            }

        }

    }

    private void beginFragmentTransaction(Fragment fragment) {
        String tag = fragment.getClass().getName();
        for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); i++) {
            if (getSupportFragmentManager().getBackStackEntryAt(i).getName().equals(tag)) {
                try {
                    getSupportFragmentManager().popBackStackImmediate(tag, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                } catch (RuntimeException e) {

                }
            }
        }

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.rlo_container, fragment, tag)
                .addToBackStack(tag)
                .commit();

    }

    public void startFragment(String className) {
        setShouldToast(false);

        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            getCurrentFocus().clearFocus();
        }

        for (Fragment fragment : fragments) {
            if (className.equals(fragment.getClass().getName())) {
                beginFragmentTransaction(fragment);
                currentFragmentTag = className;

            }
        }


    }

    public void startTermsConditionFragment() {
        startFragment(fragments[3].getClass().getName());
    }

    private boolean shouldToast = true;
    public boolean getShouldToast() {
        return shouldToast;
    }
    public void setShouldToast(boolean should) {
        shouldToast = should;
    }
}
