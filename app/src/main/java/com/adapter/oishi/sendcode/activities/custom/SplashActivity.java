package com.adapter.oishi.sendcode.activities.custom;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;

import com.adapter.oishi.OishiApplication;
import com.adapter.oishi.R;
import com.adapter.oishi.sendcode.activities.BaseActivity;
import com.adapter.oishi.sendcode.http.OishiHTTPService;

/**
 * Created by Bestiize on 25/10/2558.
 */
public class SplashActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        application.sendPageStat(OishiApplication.SENDCODE_UA, "openapp");
        application.getOishiHttpService().statStartApp(new OishiHTTPService.OnResponseCallBack<String>() {
            @Override
            public void onResponse(boolean success, Throwable error, String data, String errorMessage) {
                if (success) {
                    Log.d("startapp", "sucess");
                } else {
                    Log.d("startapp", "fail");
                }
            }
        });


        new CountDownTimer(1000, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                startActivity(new Intent(SplashActivity.this, LandingActivity.class));
                finish();
            }
        }.start();


    }
}
