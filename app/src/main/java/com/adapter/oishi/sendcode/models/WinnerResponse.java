package com.adapter.oishi.sendcode.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Bestiize on 2/2/2559.
 */
public class WinnerResponse {
    @SerializedName("data")
    private List<WinnerData> data;

    public List<WinnerData> getData() {
        return data;
    }

    public void setData(List<WinnerData> data) {
        this.data = data;
    }
}
