package com.adapter.oishi.sendcode.fragments;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adapter.oishi.Config;
import com.adapter.oishi.OishiApplication;
import com.adapter.oishi.R;
import com.adapter.oishi.sendcode.activities.BaseActivity;
import com.adapter.oishi.sendcode.activities.custom.MainActivity;
import com.adapter.oishi.sendcode.components.MobileEditText;
import com.adapter.oishi.sendcode.components.TintableImageView;
import com.adapter.oishi.sendcode.http.OishiHTTPService;
import com.adapter.oishi.sendcode.models.AlertDataResponse;
import com.adapter.oishi.sendcode.models.WinnerMobileResponse;
import com.adapter.oishi.sendcode.models.WinnerResponse;
import com.adapter.oishi.sendcode.views.dialog.NoticeWinnerDialog;
import com.adapter.oishi.sendcode.views.dialog.SorryDialog;
import com.squareup.picasso.Picasso;

/**
 * Created by Bestiize on 2/2/2559.
 */
public class NoticeWinnerFragment extends Fragment {

    private OishiApplication application;

    private SorryDialog sorryDialog;
    private NoticeWinnerDialog noticewinnerDialog;
    private TintableImageView btnCheckSubmit;
    private String mockPhoneNumber;
    private LinearLayout rloListWinner;
    private TintableImageView[] imvNotice = new TintableImageView[4];
    private MobileEditText editMobile;
    private RelativeLayout rloTouch;
    private String currentNumber = "";
    private boolean correctFormatMobile;

    private ImageView imageViewBackground;


    public static NoticeWinnerFragment newInstance() {

        Bundle args = new Bundle();
        NoticeWinnerFragment fragment = new NoticeWinnerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_noticewinner, container, false);
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        application = (OishiApplication) getActivity().getApplication();
        application.sendPageStat(OishiApplication.SENDCODE_UA, "WinterPro2016SendCode-Announce");

        final DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);

        final int w = metrics.widthPixels;
        final RelativeLayout topspace = (RelativeLayout) view.findViewById(R.id.topspace);
        topspace.post(new Runnable() {
            @Override
            public void run() {
                LinearLayout.LayoutParams l = (LinearLayout.LayoutParams) topspace.getLayoutParams();
                l.height = (int) (w * 0.787);
                topspace.setLayoutParams(l);

                if (w < 600) {
                    RelativeLayout.LayoutParams rl = (RelativeLayout.LayoutParams) btnCheckSubmit.getLayoutParams();
                    rl.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 120, metrics);
                    btnCheckSubmit.setLayoutParams(rl);
                }

            }
        });
        topspace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((BaseActivity) getActivity()).hideKeyboard(v);
                editMobile.getEditText().clearFocus();
            }
        });

        RelativeLayout belowspace = (RelativeLayout) view.findViewById(R.id.belowspace);
        belowspace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((BaseActivity) getActivity()).hideKeyboard(v);
                editMobile.getEditText().clearFocus();
            }
        });


        imageViewBackground = (ImageView) view.findViewById(R.id.imageViewBackground);
        imageViewBackground.post(new Runnable() {
            @Override
            public void run() {
                topspace.post(new Runnable() {
                    @Override
                    public void run() {
                        RelativeLayout.LayoutParams l = (RelativeLayout.LayoutParams) imageViewBackground.getLayoutParams();
                        l.height = view.findViewById(R.id.topspace).getHeight() + view.findViewById(R.id.belowspace).getHeight() + view.findViewById(R.id.rlo_listnoticewinner).getHeight();

                        imageViewBackground.setLayoutParams(l);


                        if (Config.isPassed(getContext())) {
                            Picasso.with(getContext()).load(R.drawable.bg_noticewinner2).noFade().fit().into(imageViewBackground);
                        } else {
                            Picasso.with(getContext()).load(R.drawable.bg_noticewinner1).noFade().fit().into(imageViewBackground);
                        }
                    }
                });
            }
        });




        sorryDialog = new SorryDialog(getActivity());
        noticewinnerDialog = new NoticeWinnerDialog(getActivity());
        btnCheckSubmit = (TintableImageView) view.findViewById(R.id.btn_checksubmit);
        mockPhoneNumber = "9999";
        application = (OishiApplication) getActivity().getApplicationContext();
        application.getOishiHttpService().alertData(new OishiHTTPService.OnResponseCallBack<AlertDataResponse>() {
            @Override
            public void onResponse(boolean success, Throwable error, AlertDataResponse data, String errorMessage) {
                if (success) {
                    application.getSystemManager().setAlertDataResponse(data);
                    application.getSystemManager().setCurrentAlert(Integer.parseInt(data.getDetail()));
                    ((MainActivity) getActivity()).checkIconNotification();

                } else {


                }
            }
        });
        rloListWinner = (LinearLayout) view.findViewById(R.id.rlo_listnoticewinner);
        editMobile = (MobileEditText) view.findViewById(R.id.edit_mobile);
        rloTouch = (RelativeLayout) view.findViewById(R.id.rlo_touch);
        rloTouch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((BaseActivity) getActivity()).hideKeyboard(v);
                editMobile.getEditText().clearFocus();
            }
        });

        view.findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((BaseActivity) getActivity()).hideKeyboard(v);
                editMobile.getEditText().clearFocus();
            }
        });

        for (int i = 0; i < 3; i++) {
            int resID = getResources().getIdentifier("imv_notice" + (i + 1),
                    "id", getActivity().getPackageName());
            Log.d("NoticeWinner", "pp" + resID);
            Log.d("NoticeWinner", "mm" + R.id.imv_notice1);
            imvNotice[i] = (TintableImageView) view.findViewById(resID);

        }
        ((BaseActivity) getActivity()).showProgressDialog();
        application.getOishiHttpService().checkWinner(new OishiHTTPService.OnResponseCallBack<WinnerResponse>() {
            @Override
            public void onResponse(boolean success, Throwable error, WinnerResponse data, String errorMessage) {
                if (success) {
                    Log.d("NoticeWinnerFragment", "sucess");
                    setImageNoticeWinnerList(data);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            rloListWinner.setVisibility(View.VISIBLE);
                            ((BaseActivity) getActivity()).hideProgressDialog();
                        }
                    }, 1000);

                } else {
                    Log.d("NoticeWinnerFragment", "fail");

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            rloListWinner.setVisibility(View.VISIBLE);
                            ((BaseActivity) getActivity()).hideProgressDialog();
                        }
                    }, 1000);


                }
            }
        });
        btnCheckSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                application.sendButtonStat(OishiApplication.SENDCODE_UA, "Check_Announce");

                String mobileNo = editMobile.getEditText().getText().toString();
                if (mobileNo.length() == 10) {
                    String twoCharacter = mobileNo.substring(0, 2);
                    if (twoCharacter.equals("08") || twoCharacter.equals("09") || twoCharacter.equals("06")) {
                        editMobile.showIconCorrect();
                        correctFormatMobile = true;


                    } else {
                        editMobile.hideIconCorrect();
                        correctFormatMobile = false;

                    }

                } else {
                    editMobile.hideIconCorrect();
                    correctFormatMobile = false;

                }

                if (correctFormatMobile) {
                    ((BaseActivity) getActivity()).showProgressDialog();
                    checkWinnerMobile(editMobile.getEditText().getText().toString());
                } else {
                    if (mobileNo.length() != 0) {
                        Toast.makeText(getActivity(), "เบอร์โทรศัพท์มือถือไม่ถูกต้อง", Toast.LENGTH_SHORT).show();
                    }
                    disableButton();

                }
//                if (mockPhoneNumber.equals("9999")) {
//                    noticewinnerDialog.show();
//                } else {
//                    sorryDialog.show();
//                }

            }
        });

        editMobile.getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    Log.d("mobile", "focus");

                } else {
                    Log.d("focus", "true");
                    String mobileNo = editMobile.getEditText().getText().toString();
                    if (mobileNo.length() == 10) {
                        String twoCharacter = mobileNo.substring(0, 2);
                        if (twoCharacter.equals("08") || twoCharacter.equals("09") || twoCharacter.equals("06")) {
                            application.getSystemManager().setPhonenumber(mobileNo);
                            editMobile.showIconCorrect();
                            correctFormatMobile = true;
                            if (application.getSystemManager().getCurrentAlert() == 0) {
                                disableButton();
                                return;
                            }
                            enableButton();


                        } else {
                            if (((MainActivity) getActivity()).getShouldToast()) {
                                if (mobileNo.length() != 0) {
                                    Toast.makeText(getActivity(), "เบอร์โทรศัพท์มือถือไม่ถูกต้อง", Toast.LENGTH_SHORT).show();
                                }
                            }

                            editMobile.hideIconCorrect();
                            correctFormatMobile = false;
                            if (application.getSystemManager().getCurrentAlert() == 0) {
                                disableButton();
                                return;
                            }
                            disableButton();

                        }

                    } else {
                        if (((MainActivity) getActivity()).getShouldToast()) {
                            if (mobileNo.length() != 0) {
                                Toast.makeText(getActivity(), "เบอร์โทรศัพท์มือถือไม่ถูกต้อง", Toast.LENGTH_SHORT).show();
                            }
                        }
                        editMobile.hideIconCorrect();
                        correctFormatMobile = false;
                        disableButton();
                    }
                }
            }
        });

        editMobile.getEditText().setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    //Clear focus here from edittext
                    Log.d("focus", "clear");
                    editMobile.clearFocus();
                }
                return false;
            }
        });

        if (!application.getSystemManager().getPhonenumber().equals("")) {
            editMobile.getEditText().setText(application.getSystemManager().getPhonenumber());
            editMobile.showIconCorrect();

        } else {
            editMobile.getEditText().setText("");
            editMobile.hideIconCorrect();
        }

        if (application.getSystemManager().getCurrentAlert() == 0) {
            disableButton();
        } else {
            if (editMobile.getEditText().getText().toString().equals("")) {
                disableButton();

            } else {
                enableButton();
            }
        }

        ((MainActivity) getActivity()).setShouldToast(true);

    }

    @Override
    public void onStart() {
        super.onStart();
        if (!application.getSystemManager().getPhonenumber().equals("")) {
            currentNumber = application.getSystemManager().getPhonenumber();
            editMobile.getEditText().setText(application.getSystemManager().getPhonenumber());
            editMobile.showIconCorrect();

            //checkQuotaSendcode(currentNumber);
            Log.d("SendCodeFragment", "oncreateView " + currentNumber);


        }

    }

    @Override
    public void onResume() {
        super.onResume();
        application.getOishiHttpService().alertData(new OishiHTTPService.OnResponseCallBack<AlertDataResponse>() {
            @Override
            public void onResponse(boolean success, Throwable error, AlertDataResponse data, String errorMessage) {
                if (success) {
                    application.getSystemManager().setAlertDataResponse(data);
                    application.getSystemManager().setCurrentAlert(Integer.parseInt(data.getDetail()));
                    application.getSystemManager().setSeenAlert(true);
                    ((MainActivity) getActivity()).checkIconNotification();

                } else {


                }
            }
        });

    }

    public void setImageNoticeWinnerList(final WinnerResponse data) {
        for (int i = 0; i < 3; i++) {
            if (data.getData().get(i).getStatus().equals("true")) {
                int resID = getResources().getIdentifier("imv_notice" + (i + 1),
                        "drawable", getActivity().getPackageName());
                imvNotice[i].setImageResource(resID);
                imvNotice[i].setEnabled(true);
                final int finalI = i;
                imvNotice[i].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        application.sendButtonStat(OishiApplication.SENDCODE_UA, "Click_Announce" + (finalI + 1));
                        String url = data.getData().get(finalI).getUrl();
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        startActivity(intent);

                    }
                });

            } else {
                int resIDdisable = getResources().getIdentifier("imv_notice" + (i + 1) + "_dis",
                        "drawable", getActivity().getPackageName());
                imvNotice[i].setImageResource(resIDdisable);
                imvNotice[i].setEnabled(false);


            }


        }


    }

    public void checkWinnerMobile(String mobileNo) {

        application.getOishiHttpService().checkWinnerMobile(mobileNo, new OishiHTTPService.OnResponseCallBack<WinnerMobileResponse>() {
            @Override
            public void onResponse(boolean success, Throwable error, final WinnerMobileResponse data, String errorMessage) {
                if (success) {

                    Log.d("NoticeWinnerFragment", "sucess");
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ((BaseActivity) getActivity()).hideProgressDialog();
                            checkShowDialog(data);

                        }
                    }, 1000);

                } else {
                    Log.d("NoticeWinnerFragment", "fail");
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ((BaseActivity) getActivity()).hideProgressDialog();

                        }
                    }, 1000);


                }
            }
        });

    }

    public void checkShowDialog(WinnerMobileResponse data) {
        Log.d("NoticeWinner", data.getResult());
        if (data.getResult().equals("winner")) {
            noticewinnerDialog.show();
            noticewinnerDialog.setDetailDialog(data.getType(), data.getRound(), data.getCode());
            Log.d("NoticeWinner", data.getResult() + 2);
        }
        else if (data.getResult().equals("notwinner")) {
            sorryDialog.show();
            Log.d("NoticeWinner", data.getResult() + 1);

        }
//        if (data.getResult().equals("notwinner")) {
//            sorryDialog.show();
//            sorryDialog.setImageDialogBytime();
//            Log.d("NoticeWinner", data.getResult() + 1);
//
//        }
//        if (data.getResult().equals("winner")) {
//            noticewinnerDialog.show();
//            noticewinnerDialog.setDetailDialog(data.getType(), data.getRound(), data.getCode());
//            Log.d("NoticeWinner", data.getResult() + 2);
//        }
//
//        if (data.getResult().equals("waitingtime")) {
//            Toast.makeText(getActivity(), "ยังไม่ถึงเวลาร่วมกิจกรรม", Toast.LENGTH_SHORT).show();
//            Log.d("NoticeWinner", data.getResult() + 3);
//        }


    }

    public void enableButton() {
        btnCheckSubmit.setImageResource(R.drawable.btn_check);
        btnCheckSubmit.setEnabled(true);
    }

    public void disableButton() {
        btnCheckSubmit.setImageResource(R.drawable.btn_check_dis);
        btnCheckSubmit.setEnabled(false);
    }
}
