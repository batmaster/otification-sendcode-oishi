package com.adapter.oishi.sendcode.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bestiize on 11/2/2559.
 */
public class Sync {
    @SerializedName("youtube")
    private String youtubeLink;
    @SerializedName("share_url")
    private String shrareUrl;
    @SerializedName("share_title")
    private String shareTitle;
    @SerializedName("share_description")
    private String shareDescription;
    @SerializedName("share_image")
    private String shareImage;
    @SerializedName("page_url")
    private String pageUrl;
    @SerializedName("page_reward")
    private String pageReward;
    @SerializedName("page_rule")
    private String pageRule;

    public String getYoutubeLink() {
        return youtubeLink;
    }

    public void setYoutubeLink(String youtubeLink) {
        this.youtubeLink = youtubeLink;
    }

    public String getShrareUrl() {
        return shrareUrl;
    }

    public void setShrareUrl(String shrareUrl) {
        this.shrareUrl = shrareUrl;
    }

    public String getShareTitle() {
        return shareTitle;
    }

    public void setShareTitle(String shareTitle) {
        this.shareTitle = shareTitle;
    }

    public String getShareDescription() {
        return shareDescription;
    }

    public void setShareDescription(String shareDescription) {
        this.shareDescription = shareDescription;
    }

    public String getShareImage() {
        return shareImage;
    }

    public void setShareImage(String shareImage) {
        this.shareImage = shareImage;
    }

    public String getPageUrl() {
        return pageUrl;
    }

    public void setPageUrl(String pageUrl) {
        this.pageUrl = pageUrl;
    }

    public String getPageReward() {
        return pageReward;
    }

    public void setPageReward(String pageReward) {
        this.pageReward = pageReward;
    }

    public String getPageRule() {
        return pageRule;
    }

    public void setPageRule(String pageRule) {
        this.pageRule = pageRule;
    }
}
