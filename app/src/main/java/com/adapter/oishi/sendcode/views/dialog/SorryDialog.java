package com.adapter.oishi.sendcode.views.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.adapter.oishi.OishiApplication;
import com.adapter.oishi.R;
import com.adapter.oishi.sendcode.activities.BaseActivity;
import com.adapter.oishi.sendcode.components.TintableImageView;

/**
 * Created by Bestiize on 1/2/2559.
 */
public class SorryDialog extends Dialog {

    private OishiApplication application;
    private Context context;

    private RelativeLayout topspace;
    private ImageView imageViewText;

    public SorryDialog(Context context) {
        super(context);
        this.context = context;
    }

    public SorryDialog(Context context, int themeResId) {
        super(context, themeResId);
        this.context = context;
    }

    protected SorryDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        View decorView = getWindow().getDecorView();
        setContentView(R.layout.view_dialog_sorry);
        application = (OishiApplication) context.getApplicationContext();

        this.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        this.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        imageViewText = (ImageView) findViewById(R.id.imageViewText);
        int currentNoticeNo = application.getSystemManager().getCurrentAlert();
        if (currentNoticeNo == 3) {
            imageViewText.setImageResource(R.drawable.txt_sorry2);
        }

        ImageView imageViewClose = (ImageView) findViewById(R.id.imageViewClose);
        imageViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                application.sendButtonStat(OishiApplication.SENDCODE_UA, "Close_Notwin");

                dismiss();
            }
        });

        final DisplayMetrics metrics = new DisplayMetrics();
        getWindow().getWindowManager().getDefaultDisplay().getMetrics(metrics);

        final int h = metrics.heightPixels;
        final int w = metrics.widthPixels;

        topspace = (RelativeLayout) findViewById(R.id.topspace);
        topspace.post(new Runnable() {
            @Override
            public void run() {
                LinearLayout.LayoutParams l = (LinearLayout.LayoutParams) topspace.getLayoutParams();
                l.height = (int) (w * 0.676);;
                topspace.setLayoutParams(l);
            }
        });

        imageViewText.post(new Runnable() {
            @Override
            public void run() {
                LinearLayout.LayoutParams l = (LinearLayout.LayoutParams) imageViewText.getLayoutParams();
                l.height = (int) (w * 0.356);;
                imageViewText.setLayoutParams(l);
            }
        });
    }

}
