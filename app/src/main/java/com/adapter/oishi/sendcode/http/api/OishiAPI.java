package com.adapter.oishi.sendcode.http.api;

import android.os.Build;

import com.adapter.oishi.OishiApplication;
import com.adapter.oishi.otification.utility.service.HTTPService;
import com.adapter.oishi.sendcode.models.AlertDataResponse;
import com.adapter.oishi.sendcode.models.QuotaSendCodeResponse;
import com.adapter.oishi.sendcode.models.SendCodeResponse;
import com.adapter.oishi.sendcode.models.Sync;
import com.adapter.oishi.sendcode.models.WinnerMobileResponse;
import com.adapter.oishi.sendcode.models.WinnerResponse;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Created by Bestiize on 30/1/2559.
 */
public interface OishiAPI {

    @POST("/promotion/api/mobile/mobileapp_sendcode.aspx?")
    Call<SendCodeResponse> sendCode(@Query("mobileno") String mobileNo, @Query(value = "code", encoded = true) String code);

    @POST("/promotion/api/mobile/mobileapp_checkdailyquota.aspx?")
    Call<QuotaSendCodeResponse> checkQuota(@Query("mobileno") String mobileNo);

    @GET("/promotion/api/mobile/mobileapp_getWinnerAnnounce.aspx")
    Call<WinnerResponse> checkWinner();

    @POST("/promotion/api/mobile/mobileapp_checkwinner.aspx?")
    Call<WinnerMobileResponse> checkWinnerMobile(@Query("mobileno") String mobileNo);

    @GET("/promotion/api/mobile/mobileapp_alertData.aspx")
    Call<AlertDataResponse> alertData();

    @GET("/promotion/api/mobile/applicationstatlog.aspx")
    Call<String> statLog(@Query("stat") String app, @Query("param1") String platform, @Query("param2") String action);

    @GET("/promotion/api/mobile/mobileapp_getDataInfoAndroid.aspx")
    Call<Sync> syncData();




}
