package com.adapter.oishi.sendcode.components;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.adapter.oishi.R;

/**
 * Created by Bestiize on 3/2/2559.
 */
public class NoticeCodeEditText extends RelativeLayout {
    private Context context;
    private EditText editText;
    public NoticeCodeEditText(Context context) {
        super(context);
        this.context = context;
        setupView();
    }

    public NoticeCodeEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        setupView();
    }

    public NoticeCodeEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        setupView();
    }

    private void setupView(){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.view_layout_notice_edittext, this, true);
        editText = (EditText) view.findViewById(R.id.edit);


    }

    public EditText getEditText() {
        return editText;
    }
}
