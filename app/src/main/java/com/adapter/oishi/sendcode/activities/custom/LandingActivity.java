package com.adapter.oishi.sendcode.activities.custom;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.adapter.oishi.Config;
import com.adapter.oishi.OishiApplication;
import com.adapter.oishi.R;
import com.adapter.oishi.activity.NewHomeActivity;
import com.adapter.oishi.otification.activity.HomeActivity;
import com.adapter.oishi.otification.utility.service.HTTPService;
import com.adapter.oishi.sendcode.activities.BaseActivity;
import com.adapter.oishi.sendcode.components.TintableImageView;
import com.adapter.oishi.sendcode.http.OishiHTTPService;
import com.adapter.oishi.sendcode.models.AlertDataResponse;
import com.adapter.oishi.sendcode.models.Sync;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

/**
 * Created by Bestiize on 30/1/2559.
 */
public class LandingActivity extends BaseActivity {
    TintableImageView btnEnter;
    private Target target;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);


        btnEnter = (TintableImageView) findViewById(R.id.btn_enter);
        btnEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                application.sendButtonStat(OishiApplication.OTI_UA, "Main_Sendcode");
                application.getHttpService().sendOpenAppStat(HTTPService.STAT_SENDCODE);

                loadBitmap();
                showProgressDialog();
            }
        });


    }



    public void alertData() {
        application.getOishiHttpService().alertData(new OishiHTTPService.OnResponseCallBack<AlertDataResponse>() {
            @Override
            public void onResponse(boolean success, Throwable error, AlertDataResponse data, String errorMessage) {
                if (success) {
                    if (Integer.parseInt(data.getDetail()) != application.getSystemManager().getCurrentAlert()) {
                        application.getSystemManager().setAlertDataResponse(data);
                        application.getSystemManager().setCurrentAlert(Integer.parseInt(data.getDetail()));
                        application.getSystemManager().setSeenAlert(false);
                    }
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            hideProgressDialog();
                            startActivity(new Intent(LandingActivity.this, MainActivity.class));
                            finish();

                        }
                    }, 1000);


                    Log.d("LandingActiivty", "alertData success");
                } else {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            hideProgressDialog();
                            Toast.makeText(LandingActivity.this, "ระบบขัดข้องกรุณาลองใหม่อีกครั้ง", Toast.LENGTH_SHORT).show();

                        }
                    }, 1000);

                    Log.d("LandingActiivty", "alertData failed");

                }
            }
        });


    }

    public void loadBitmap() {
        target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                application.getSystemManager().setBitmap(bitmap);
                syncData();
                //alertData();

            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };
        Picasso.with(getApplicationContext()).load(R.drawable.bg_sendcode).into(target);


    }

    public void syncData(){
        application.getOishiHttpService().syncData(new OishiHTTPService.OnResponseCallBack<Sync>() {
            @Override
            public void onResponse(boolean success, Throwable error, Sync data, String errorMessage) {
                if (success) {
                    application.getSystemManager().setSyncData(data);
                   // Log.d("LandingActivity", "success" +data);
                    Log.d("LandingActivity", "success"+data.getPageRule());
                    alertData();
                } else {

                    Log.d("LandingActivity", "fail sync");
                }
            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (Config.isPassed(getApplicationContext())) {
            finish();
        }
        else {
            startActivity(new Intent(getApplicationContext(), NewHomeActivity.class));
            finish();
        }
    }
}
