package com.adapter.oishi.sendcode.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bestiize on 2/2/2559.
 */
public class CodeList {

    @SerializedName("code")
    private String code;
    @SerializedName("text")
    private String text;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
